﻿<cfcomponent output="false">

	<cfset this.dsn = application.eonDsn>
	<cfset this.procDsn = application.eonProcData>
	
	<cffunction name="getAccountCode" returntype="String" output="false" access="public">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="lookupCode" required="true" type="string">		
		
		<cfset local.returnStr = "">
		
		<cfquery name="getCode" datasource="#this.dsn#" cachedwithin="#CreateTimeSpan(90,0,0,0)#">
			SELECT id FROM glAccountCodes 
			WHERE businessUnitId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.businessUnitId#">
			AND code LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.lookupCode#">
		</cfquery>
		
		<cfif getCode.RecordCount>
			<cfset local.returnStr = getCode.id>
		</cfif>
		
		<cfreturn local.returnStr />
		
	</cffunction>
	
	<cffunction name="InsertSalesData" returntype="void" output="false" access="public">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="dateOfSale" required="true" type="date">
		<cfargument name="codeId" required="true" type="string">			
		<cfargument name="debit" required="true" type="string">
		<cfargument name="credit" required="true" type="string">
		<cfargument name="note" required="true" type="string">
		
		<cfquery name="InsertData" datasource="#this.dsn#">
			IF NOT EXISTS (SELECT id FROM dailySales WHERE businessUnitId = '#arguments.businessUnitId#' AND dateOfSale = '#arguments.dateOfSale#' AND glAccountCodeId = '#arguments.codeId#' AND note = '#arguments.note#')
			BEGIN			
			INSERT INTO [dailySales]
			   ([businessUnitId]
			   ,[dateOfSale]
			   ,[glAccountCodeId]
			   ,[debit]
			   ,[credit]
			   ,[note])			   
			VALUES
			   ('#arguments.businessUnitId#'
			   ,'#arguments.dateOfSale#'
			   ,'#arguments.codeId#'
			   ,'#arguments.debit#'
			   ,'#arguments.credit#'
			   ,'#arguments.note#')
			END			 
		</cfquery>
	
	</cffunction>
	
	<cffunction name="InsertDailyGeneralData" returntype="void" output="false" access="public">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="dateOccured" required="true" type="date">
		<cfargument name="couponsNum" required="true" type="string">			
		<cfargument name="promosNum" required="true" type="string">
		<cfargument name="deletesNum" required="true" type="string">
		<cfargument name="customerCount" required="true" type="string">
		<cfargument name="giftChecksSold" required="true" type="string">
		<cfargument name="laborDollar" required="true" type="string">
		<cfargument name="gcPaidOutDollar" required="true" type="string">
		
		<cfquery name="InsertGeneralData" datasource="#this.dsn#">
			IF NOT EXISTS (SELECT id FROM dailyGeneralStoreData WHERE businessUnitId = '#arguments.businessUnitId#' AND dateOccured = '#arguments.dateOccured#')
			BEGIN			
			INSERT INTO [dailyGeneralStoreData]
			([businessUnitId]
			,[dateOccured]
			,[couponsNum]
			,[promosNum]
			,[deletesNum]
			,[customerCount]
			,[giftChecksSold]
			,[laborDollar]
			, [giftChecksPaidOut])
			VALUES
			('#arguments.businessUnitId#'
			,'#arguments.dateOccured#'
			,'#arguments.couponsNum#'
			,'#arguments.promosNum#'
			,'#arguments.deletesNum#'
			,'#arguments.customerCount#'
			,'#arguments.giftChecksSold#'
			,'#arguments.laborDollar#'
			,'#arguments.gcPaidOutDollar#')
			END			 
		</cfquery>
	
	</cffunction>
	
	
	<cffunction name="InsertBankTransactions" returntype="void" output="false" access="public">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="bankCode" required="true" type="string">
		<cfargument name="dateOfPosting" required="true" type="date">			
		<cfargument name="entryTypeId" required="true" type="string">
		<cfargument name="amount" required="true" type="string">
		
		<cfquery name="InsertBankTransactions" datasource="#this.dsn#">
			INSERT INTO [bankTransactions]
			           ([businessUnitId]
			           ,[bankCode]
			           ,[dateOfPosting]
			           ,[entryTypeId]
			           ,[amount])
			     VALUES
			           ('#arguments.businessUnitId#'
			           ,'#arguments.bankCode#'
			           ,'#arguments.dateOfPosting#'
			           ,'#arguments.entryTypeId#'
			           ,'#arguments.amount#')
		</cfquery>
	
	</cffunction>
	
	
	<cffunction name="InsertpurchaseCategory" returntype="boolean" output="false" access="public">
		<cfargument name="categoryId" required="true" type="string">
		
		<cfset local.result = false>
		
		<cfquery name="getCategory" datasource="#this.dsn#">
			SELECT categoryId 
			FROM purchaseCategories 
			WHERE categoryId = '#arguments.categoryId#'
		</cfquery>	
			
		<cfif NOT getCategory.RecordCount>
			<cfquery name="InsertpurchaseCategory" datasource="#this.dsn#">
				INSERT INTO [purchaseCategories]
				   ([categoryId])
				VALUES
				   ('#arguments.categoryId#')
			</cfquery>
			<cfset local.result = true>
		</cfif>
		
		<cfreturn local.result>
	
	</cffunction>	
	
	
	<cffunction name="InsertpurchaseItem" returntype="void" output="false" access="public">
		<cfargument name="itemNumber" required="true" type="string">
		<cfargument name="itemCategoryId" required="true" type="string">
		<cfargument name="itemDescription" required="true" type="string">
		<cfargument name="itemBrand" required="true" type="string">
		
			<cfquery name="InsertpurchaseItem" datasource="#this.dsn#">
				IF NOT EXISTS (SELECT itemNumber FROM purchaseItems WHERE itemNumber = '#arguments.itemNumber#')
				BEGIN
				INSERT INTO [purchaseItems]
				           ([itemNumber]
				           ,[itemCategoryId]
						   ,[itemDescription]
						   ,[itemBrand]
						   )
				     VALUES
				           ('#arguments.itemNumber#'
				           ,'#arguments.itemCategoryId#'
						   ,'#arguments.itemDescription#'
						   ,'#arguments.itemBrand#'
						   )
				END
				ELSE
				BEGIN
				UPDATE [purchaseItems]
				SET dateLastInvoiced = getdate()
				WHERE itemNumber = '#arguments.itemNumber#'
				END
			</cfquery>

	</cffunction>
	
	
	<cffunction name="InsertInvoiceItem" returntype="void" output="false" access="public">
		<cfargument name="dcNumber" required="true" type="string">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="invoiceNumber" required="true" type="string">
		<cfargument name="invoiceDate" required="true" type="string">
		<cfargument name="invoiceType" required="true" type="string">
		<cfargument name="invoiceLineNumber" required="true" type="string">		
		<cfargument name="quantity" required="true" type="string">
		<cfargument name="itemNumber" required="true" type="string">
		<cfargument name="unitOfMeasure" required="true" type="string">
		<cfargument name="itemPack" required="true" type="string">
		<cfargument name="itemSize" required="true" type="string">
		<cfargument name="itemCube" required="true" type="string">
		<cfargument name="itemUPC" required="true" type="string">
		<cfargument name="itemPrice" required="true" type="numeric">
		<cfargument name="itemTaxCode" required="true" type="string">
		<cfargument name="itemTaxAmount" required="true" type="numeric">
		<cfargument name="itemExtendedPrice" required="true" type="numeric">
		
			<cfquery name="InsertInvoiceItem" datasource="#this.dsn#">
				IF NOT EXISTS (SELECT id FROM purchaseInvoices WHERE purchaseInvoices.invoiceNumber = '#arguments.invoiceNumber#' 
					AND purchaseInvoices.invoiceLineNumber = '#arguments.invoiceLineNumber#' AND purchaseInvoices.invoiceDate = '#arguments.invoiceDate#')
				BEGIN
				INSERT INTO [purchaseInvoices]
			           ([dcNumber]
					   ,[businessUnitId]
			           ,[invoiceNumber]
			           ,[invoiceDate]
			           ,[invoiceType]
			           ,[invoiceLineNumber]
			           ,[quantity]
			           ,[itemNumber]
			           ,[unitOfMeasure]
			           ,[itemPack]
			           ,[itemSize]
			           ,[itemCube]
			           ,[itemUPC]
			           ,[itemPrice]
			           ,[itemTaxCode]
			           ,[itemTaxAmount]
			           ,[itemExtendedPrice])
			     VALUES
			           ('#arguments.dcNumber#'
					   ,'#arguments.businessUnitId#'
			           ,'#arguments.invoiceNumber#'
			           ,'#arguments.invoiceDate#'
			           ,'#arguments.invoiceType#'
			           ,'#arguments.invoiceLineNumber#'
			           ,'#arguments.quantity#'
			           ,'#arguments.itemNumber#'
			           ,'#arguments.unitOfMeasure#'
			           ,'#arguments.itemPack#'
			           ,'#arguments.itemSize#'
			           ,'#arguments.itemCube#'
			           ,'#arguments.itemUPC#'
			           ,#arguments.itemPrice#
			           ,'#arguments.itemTaxCode#'
			           ,#arguments.itemTaxAmount#
			           ,#arguments.itemExtendedPrice#)
				END
			</cfquery>

	</cffunction>
	
	
	<cffunction name="Insertmas90Entry" returntype="void" output="false" access="public">
		<cfargument name="code" required="true" type="string">
		<cfargument name="note" required="true" type="string">
		<cfargument name="dateOfPosting" required="true" type="string">
		<cfargument name="amount" required="true" type="string">
		<cfargument name="transType" required="true" type="string">
		<cfargument name="journalType" required="true" type="string">
		<cfargument name="description" required="true" type="string">
		
			<cfquery name="Insertmas90Entry" datasource="#this.dsn#">
				INSERT INTO [mas90entries]
			           ([code]
			           ,[note]
			           ,[dateOfPosting]
			           ,[amount]
			           ,[transType]
			           ,[journalType]
					   ,[description])
			     VALUES
			           ('#arguments.code#'
			           ,'#arguments.note#'
			           ,'#arguments.dateOfPosting#'
			           ,'#arguments.amount#'
			           ,'#arguments.transType#'
			           ,'#arguments.journalType#'
					   ,'#arguments.description#')
			</cfquery>

	</cffunction>
	
	<cffunction name="InsertPayRollDaily" returntype="void" output="false" access="public">
		<cfargument name="businessUnit" required="true" type="string">
		<cfargument name="ppeDate" required="true" type="string">
		<cfargument name="payRollId" required="true" type="string">
		<cfargument name="empName" required="true" type="string">
		<cfargument name="jobId" required="true" type="string">
		<cfargument name="jobDate" required="true" type="string">
		<cfargument name="jobInTime" required="true" type="string">
		<cfargument name="jobOutTime" required="true" type="string">
		<cfargument name="hoursWorked" required="true" type="string">
		
			<cfquery name="InsertPayRollEntry" datasource="#this.procDsn#">
				INSERT INTO [payrollDaily]
				           ([businessUnit]
				           ,[ppeDate]
				           ,[payRollId]
				           ,[empName]
				           ,[jobId]
				           ,[jobDate]
				           ,[jobInTime]
				           ,[jobOutTime]
				           ,[hoursWorked])
				     VALUES
				           ('#arguments.businessUnit#'
				           ,'#arguments.ppeDate#'
				           ,'#arguments.payRollId#'
				           ,'#arguments.empName#'
				           ,'#arguments.jobId#'
				           ,'#arguments.jobDate#'
				           ,'#arguments.jobInTime#'
				           ,'#arguments.jobOutTime#'
				           ,'#arguments.hoursWorked#')
			</cfquery>

	</cffunction>
	
	<cffunction name="InsertPayrollPeriodTotals" returntype="void" output="false" access="public">
		<cfargument name="businessUnit" required="true" type="string">
		<cfargument name="ppeDate" required="true" type="string">
		<cfargument name="payRollId" required="true" type="string">
		<cfargument name="empName" required="true" type="string">
		<cfargument name="regularHours" required="true" type="string">
		<cfargument name="OtHours" required="true" type="string">
		<cfargument name="regularPay" required="true" type="string">
		<cfargument name="otPay" required="true" type="string">
		
			<cfquery name="InsertPayRollTotals" datasource="#this.procDsn#">
				INSERT INTO [payrollPeriod]
				           ([businessUnit]
				           ,[ppeDate]
				           ,[payrollId]
				           ,[empName]
				           ,[regularHours]
				           ,[OtHours]
				           ,[regularPay]
				           ,[otPay])
				     VALUES
				           ('#arguments.businessUnit#'
				           ,'#arguments.ppeDate#'
				           ,'#arguments.payRollId#'
				           ,'#arguments.empName#'
				           ,'#arguments.regularHours#'
				           ,'#arguments.OtHours#'
				           ,'#arguments.regularPay#'
				           ,'#arguments.otPay#')
			</cfquery>

	</cffunction>
	
	
	<cffunction name="InsertInventoryItem" returntype="void" output="false" access="public">
		<cfargument name="userId" required="true" type="string">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="dateCreated" required="true" type="string">
		<cfargument name="dateWeekEnding" required="true" type="string">
		<cfargument name="itemNumber" required="true" type="string">
		<cfargument name="quantity" required="true" type="string">
		<cfargument name="inProcess" required="true" type="numeric">
		
			<cfset local.price = "">
			<cfquery name="GetItemUnitPrice" datasource="#this.dsn#">
				SELECT dbo.GetUnitPriceByItemNumber('#arguments.itemNumber#','#arguments.inProcess#') AS unitPrice
			</cfquery>
			
			<cfset local.price = GetItemUnitPrice.unitPrice>
		
			<cfif isNumeric(local.price)>
				<cfquery name="UpdatePrevious" datasource="#this.dsn#">
					UPDATE dbo.storeInventory
					SET isLatest = 0
					WHERE
					itemNumber = '#arguments.itemNumber#'
					AND businessUnitId = '#arguments.businessUnitId#'
					AND dateWeekEnding = '#arguments.dateWeekEnding#'
					AND inProcess = '#arguments.inProcess#'
				</cfquery>
			
				<cfquery name="InsertPayRollTotals" datasource="#this.dsn#">
					INSERT INTO [storeInventory]
					           ([userId]
					           ,[businessUnitId]
					           ,[dateCreated]
					           ,[dateWeekEnding]
					           ,[itemNumber]
					           ,[quantity]
							   ,[price]
							   ,[inProcess]
							   )
					     VALUES
					           ('#arguments.userId#'
					           ,'#arguments.businessUnitId#'
					           ,'#arguments.dateCreated#'
					           ,'#arguments.dateWeekEnding#'
					           ,'#arguments.itemNumber#'
					           ,'#arguments.quantity#'
							   ,'#local.price#'
							   ,'#arguments.inProcess#'
							   )
				</cfquery>

			</cfif>

	</cffunction>
	
	<cffunction name="InsertInventoryTransfer" returntype="void" output="false" access="public">
		<cfargument name="dateOftransfer" required="true" type="string">
		<cfargument name="fromStoreId" required="true" type="string">
		<cfargument name="toStoreId" required="true" type="string">
		<cfargument name="itemNumber" required="true" type="string">
		<cfargument name="quantity" required="true" type="string">
		<cfargument name="enteredByUser" required="true" type="string">
		<cfargument name="inProcess" required="true" type="string">
		<cfargument name="priceType" required="true" type="string">
		
		<cfset local.price = "">
		
		<cfif arguments.priceType EQ 0>
			<cfquery name="getItem" datasource="#this.dsn#">
				SELECT dbo.GetItemPriceByItemNumber('#arguments.itemNumber#') AS itemPrice
			</cfquery>
		<cfelse>
			<cfquery name="getItem" datasource="#this.dsn#">
				SELECT dbo.GetUnitPriceByItemNumber('#arguments.itemNumber#','#arguments.inProcess#') AS itemPrice
			</cfquery>				
		</cfif>

			
			<cfset local.price = getItem.itemPrice>
			
			<cfif isNumeric(local.price)>
				<cfquery name="InsertPayRollTotals" datasource="#this.dsn#">
					INSERT INTO [storeInventoryTransfers]
					           ([dateOfTransfer]
					           ,[fromStoreId]
					           ,[toStoreId]
					           ,[itemNumber]
					           ,[unitCost]
					           ,[quantity]
					           ,[enteredByUser]
					           )
					     VALUES
					           ('#arguments.dateOftransfer#'
					           ,'#arguments.fromStoreId#'
					           ,'#arguments.toStoreId#'
					           ,'#arguments.itemNumber#'
					           ,'#local.price#'
					           ,'#arguments.quantity#'
					           ,'#arguments.enteredByUser#'
					          )
				</cfquery>
			</cfif>
	</cffunction>
	
	<cffunction name="InsertNighlyData" returntype="void" output="false" access="public">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="businessDate" required="true" type="string">
		<cfargument name="sosSeconds" required="true" type="string">
		<cfargument name="carCountTimer" required="true" type="string">
		<cfargument name="carCountMicros" required="true" type="string">
		<cfargument name="safeTotal" required="true" type="string">
		<cfargument name="gcTotal" required="true" type="string">
		<cfargument name="enteredByUser" required="true" type="string">
		
			<cfquery name="InsertSos" datasource="#this.dsn#">
				IF EXISTS (SELECT id FROM dbo.storeNightlyData WHERE businessUnitId = '#arguments.businessUnitId#' AND businessDate = '#arguments.businessDate#')
				BEGIN
					UPDATE dbo.storeNightlyData
					SET 
					sosSeconds = '#arguments.sosSeconds#',
					dateModified = GETDATE(),
					modifiedByUser = '#arguments.enteredByUser#',
					carCountTimer = '#arguments.carCountTimer#',
					carCountMicros = '#arguments.carCountMicros#',
					safeTotal = '#arguments.safeTotal#',
					gcTotal = '#arguments.gcTotal#'						
					WHERE businessUnitId = '#arguments.businessUnitId#' 
					AND 
					businessDate = '#arguments.businessDate#'
				END
				ELSE
				BEGIN
					INSERT INTO [storeNightlyData]
					           (
							   [businessUnitId]
					           ,[businessDate]
					           ,[sosSeconds]
							   ,[carCountTimer]
							   ,[carCountMicros]
					           ,[safeTotal]
					           ,[gcTotal]
					           ,[enteredByUser]
							   )
					     VALUES
					           (
							   '#arguments.businessUnitId#'
					           ,'#arguments.businessDate#'
					           ,'#arguments.sosSeconds#'
							   ,'#arguments.carCountTimer#'
							   ,'#arguments.carCountMicros#'
							   ,'#arguments.safeTotal#'
							   ,'#arguments.gcTotal#'
					           ,'#arguments.enteredByUser#'
							   )
				END
			
				
			</cfquery>

	</cffunction>
	
	<cffunction name="GetInventorySnapshot" returntype="guid" access="public">
		<cfargument name="createdByUser" required="true" type="string">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="dateWeekEnding" required="true" type="string">
		<cfquery name="SnapShot" datasource="#this.dsn#">
			DECLARE @GUID uniqueidentifier;
			SET @GUID = NEWID();
			INSERT INTO [storeInventorySnapshot]
            (
			   [id],
			   [createdByUser],
			   [dateWeekEnding],
			   [businessUnitId]
		    )
     		VALUES
            (
	           	@GUID,
				'#arguments.createdByUser#',
				'#arguments.dateWeekEnding#',
				'#arguments.businessUnitId#'
			);
			SELECT @GUID AS snapShotId;
		</cfquery>
		<cfreturn SnapShot.snapShotId>
	</cffunction>
	
	<cffunction name="getInventoryVal" returntype="Numeric" access="public">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="CategoryId" required="true" type="string">
		<cfargument name="dateWeekEnding" required="true" type="date">
		
		<cfset local.InventoryAmount = 0>
		
		<cfquery name="Getinventory" datasource="#this.dsn#">
			SELECT * FROM [GetInventoryByStoreDateCategoryId] (
			'#arguments.businessUnitId#'
			,#arguments.CategoryId#
			,'#arguments.dateWeekEnding#')
		</cfquery>
		
		<cfif Getinventory.RecordCount>
			<cfset local.InventoryAmount = Getinventory.InventoryAmount>
		</cfif>
		
		<cfreturn local.InventoryAmount>
		
	</cffunction>
	
	<cffunction name="InsertInventorySummary" returntype="void" access="public">
		<cfargument name="snapShotId" required="true" type="guid">
		<cfargument name="businessUnitId" required="true" type="string">
		<cfargument name="dateWeekEnding" required="true" type="date">
		
		<cfset local.getItemCats = "">
		<cfset local.inventoryVal = 0>
		
		<cfquery name="local.getItemCats" datasource="#this.dsn#">
			SELECT categoryId
			FROM dbo.purchaseCategories
			WHERE accountCode IN ('4600','4601','5720')
			ORDER BY accountCode
		</cfquery>
		
		<cfloop query="local.getItemCats">
			<cfset local.inventoryVal = this.getInventoryVal('#arguments.businessUnitId#','#local.getItemCats.categoryId#','#arguments.dateWeekEnding#')>
			
			<cfquery name="insertInvSummary" datasource="#this.dsn#">
				INSERT INTO [storeInventorySummary]
				   ([inventorySnapshotId]
				   ,[purchaseItemCategoryId]
				   ,[inventoryValue])
				VALUES
				   ('#arguments.snapShotId#'
				   ,#local.getItemCats.categoryId#
				   ,#local.inventoryVal#)
			</cfquery>
			
		</cfloop>
	
	</cffunction>
	
	<cffunction name="InsertDailyMenuMix" returntype="void" output="false" access="public">
		<cfargument name="businessUnit" required="true" type="string">
		<cfargument name="businessDate" required="true" type="string">
		<cfargument name="itemId" required="true" type="string">
		<cfargument name="itemDescr" required="true" type="string">
		<cfargument name="qtySold" required="true" type="string">
		<cfargument name="netSales" required="true" type="string">
		
		<cfset local.newItemStruct = {itemId = arguments.itemId,itemDescription = arguments.itemDescr}>
		<cfset local.newMenuMixStruct = {businessDate = arguments.businessDate
										,businessUnitId = arguments.businessUnit
										,itemId = arguments.itemId
										,qtySold = arguments.qtySold
										,netSales = arguments.netSales}>
				
		<!---CHECK IF ITEM EXISTS. IF NOT, INSERT INTO ITEMS TABLE--->

		<cfset local.foundItem = application.appDao.storeMenuMixItems.read({itemId = local.newItemStruct.itemId})>
		<cfset local.foundMmix = application.appDao.storeMenuMixSales.read({businessDate = arguments.businessDate
										,businessUnitId = arguments.businessUnit
										,itemId = arguments.itemId})>	
			
		<cfif NOT local.foundItem.recordcount>
			<cfset application.appDao.storeMenuMixItems.create(local.newItemStruct)>
		</cfif>
		
		<!---INSERT MENU MIX--->
		<cfif NOT local.foundMmix.recordcount>
			<cfset application.appDao.storeMenuMixSales.create(local.newMenuMixStruct)>
		</cfif>		

	</cffunction>
	
</cfcomponent>