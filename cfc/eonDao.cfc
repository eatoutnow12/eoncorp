<cfcomponent displayname="Dynamic Application Database Access Object" hint="EONDAO: Pass in a table name and use CRUD (Create, Read, Update, Delete)." output="false">
<!---
	File:		eondao.cfc
	Desc:		Dynamic DAO object.  Pass in a table and instantly have CRUD.
	Ver:		2.1.1
	Author: 	Wes Byrd
	Note: 		Visit http://dadao.riaforge.com for details or to contact the author
	Histor: 	Update history located in the dadao-readme.txt located in the download zip file
	License: 	http://www.apache.org/licenses/LICENSE-2.0
--->

	<!--- Initialize the object. --->
	<cffunction name="init" returntype="eondao" access="public">
		<cfargument name="tableName" required="true" type="string" />
		<cfargument name="cacheTableInfo" required="false" type="boolean" default="true" />
		<cfargument name="dsn" required="false" default="#application.eonDsn#" />
		
		<cfset variables.dsn = arguments.dsn />
		<cfset variables.tableName = arguments.tableName />
		
		<cfset variables.tableData = describe(arguments.cacheTableInfo) />
		<cfset variables.fieldList = valueList(variables.tableData.column_name) />
		<cfset variables.tableKeys = filter(variables.tableData,"is_primaryKey = 'YES'","*") />
		
		<cfset this.tableData = variables.tableData />
		
		<cfreturn this />
	</cffunction>
		

	<!--- ------------------------ --->
	<!--- // The CRUD functions \\ --->
	<!--- ------------------------ --->
	
	<!--- Create --->
	<cffunction name="create" returntype="numeric" access="public">
		<cfargument name="createStruct" type="struct" required="true" />
		<cfargument name="sequenceName" type="string" required="false" default="" /> 
		
		<!--- To support Oracle's sequence, pass in the sequenceName --->
		<cfif structKeyExists(arguments,"sequenceName") and arguments.sequenceName neq "">
			<cfquery name="qGetSeqVal" datasource="#variables.dsn#">
				SELECT #arguments.sequenceName#.nextval FROM dual
			</cfquery>
		</cfif>
		
		<!--- using "local" scope rather than VAR allows you to refer to the variable in this function with it's scope prefix --->
		<cfset local.insertResult = structNew() />
		<cfquery name="qCreate" datasource="#variables.dsn#" result="local.insertResult">
			INSERT INTO #variables.tableName#
				(
				<cfset local.fieldCount = 0 />
				<cfloop query="variables.tableData">
					<cfif structKeyExists(arguments.createStruct, variables.tableData.column_name)>
						<cfset local.fieldCount++ /><cfif local.fieldCount gt 1>,</cfif>
						#variables.tableData.column_name#
					</cfif>
				</cfloop>
				)
			VALUES
				(
				<cfset local.fieldCount = 0 />
				<cfloop query="variables.tableData">
					<cfif structKeyExists(arguments.createStruct, variables.tableData.column_name)>
						<cfset local.fieldValue = arguments.createStruct[variables.tableData.column_name] />
						<cfset local.fieldCount++ /><cfif local.fieldCount gt 1>,</cfif>
						<cfif local.fieldValue is "">
							NULL
						<cfelseif isDefined("qGetSeqVal.nextval") and local.fieldValue contains ".nextval">
							<cfqueryparam cfsqltype="cf_sql_integer" value="#qGetSeqVal.nextval#" />
						<cfelse>
							<cfqueryparam cfsqltype="cf_sql_#variables.tableData.type_name#" value="#local.fieldValue#" />
						</cfif>
					</cfif>
				</cfloop>
				)
		</cfquery>

		<cfif isDefined("qGetSeqVal.nextval")>
			<cfreturn qGetSeqVal.nextval />
		<cfelseif structKeyExists(local.insertResult,"generated_key")>
			<cfreturn local.insertResult.generated_key />
		<cfelseif structKeyExists(local.insertResult,"identitycol")>
			<cfreturn local.insertResult.identitycol />
		<cfelseif structKeyExists(local.insertResult,"rowid")>
			<cfreturn local.insertResult.rowid />
		<cfelseif structKeyExists(local.insertResult,"syb_identity")>
			<cfreturn local.insertResult.syb_identity />
		</cfif>
		
		<cfreturn 0>
	</cffunction>
	
	<!--- Read --->
	<cffunction name="read" returntype="query" access="public">
		<cfargument name="readStruct" type="struct" required="false" />
		<cfargument name="readForeignTables" type="boolean" required="false" default="false" />
		<cfargument name="tableJoinType" type="string" required="false" default="INNER" />
		
		<!--- if we need the foreign table field data, then lets do this query now and use it later --->
		<cfif arguments.readForeignTables><cfset local.fkfields = filter(variables.tableData,"is_foreignkey = 'YES'","*") /></cfif>
		
		<!--- if we do not want all the fields, lets just get the ones we asked for --->
		<cfset local.fieldList = variables.fieldList />
		<cfif structKeyExists(arguments, "readStruct") AND structKeyExists(arguments.readStruct,"fieldList")>
			<cfset local.fieldList = arguments.readStruct.fieldList />
		<cfelseif arguments.readForeignTables>
			<!--- we need to prevent ambiguous column names --->
			<cfset local.newFieldList = "" />
			<cfloop from="1" to="#listLen(local.fieldList)#" index="i">
				<cfset local.newFieldList = listAppend(local.newFieldList,"#variables.tableName#.#listGetAt(local.fieldList, i)#") />
			</cfloop>
			<cfloop query="local.fkfields">
				<cfset local.fkTblDAO = new eondao(local.fkfields.referenced_primarykey_table) />
				<cfloop query="local.fkTblDAO.tableData">
					<cfif listFindNoCase(local.fieldList,local.fkTblDAO.tableData.column_name) eq 0>
						<cfset local.newFieldList = listAppend(local.newFieldList, "#local.fkfields.referenced_primarykey_table#.#local.fkTblDAO.tableData.column_name#") />
					</cfif>
				</cfloop>
			</cfloop>
			<cfset local.fieldList = local.newFieldList />
		</cfif>

		<cfquery name="qGet" datasource="#variables.dsn#">
			SELECT 	#local.fieldList#
			FROM 	#variables.tableName#
			<!--- Join Foreign Key Tables if requested --->
			<cfif arguments.readForeignTables>
				<cfloop query="local.fkfields">
					#arguments.tableJoinType# JOIN #local.fkfields.referenced_primarykey_table# ON #variables.tableName#.#local.fkfields.column_name# = #local.fkfields.referenced_primarykey_table#.#local.fkfields.referenced_primarykey#
				</cfloop>
			</cfif>
			<cfif structKeyExists(arguments,"readStruct")>
				WHERE 1 = 1
				<cfloop query="variables.tableData">
					<cfif structKeyExists(arguments.readStruct, variables.tableData.column_name)>
						<cfset local.loopValue = arguments.readStruct[variables.tableData.column_name] />
						<cfif local.loopValue is not "">
							AND #variables.tableName#.#variables.tableData.column_name# = <cfqueryparam cfsqltype="cf_sql_#variables.tableData.type_name#" value="#local.loopValue#" />
						</cfif>
					</cfif>
				</cfloop>
				<cfif structKeyExists(arguments.readStruct, "queryFilter")>
					AND #preserveSingleQuotes(arguments.readStruct.queryFilter)#
				</cfif>
			</cfif>
			<cfif structKeyExists(arguments, "readStruct") AND structKeyExists(arguments.readStruct,"groupBy")>
				GROUP BY #arguments.readStruct.groupby#
			</cfif>
			<cfif structKeyExists(arguments, "readStruct") AND structKeyExists(arguments.readStruct,"orderBy")>
				ORDER BY #arguments.readStruct.orderby#
			</cfif>
		</cfquery>
		<cfreturn qGet />
	</cffunction>
	
	<!--- Update --->
	<cffunction name="update" returntype="boolean" access="public">
		<cfargument name="updateStruct" type="Struct" required="true" />
		
		<!--- exclude key fields from fieldList --->
		<cfset local.updateFields = filter(variables.tableData,"is_primaryKey = 'NO'","*") />
		
		<cfquery name="qUpdate" datasource="#variables.dsn#">
			UPDATE #variables.tableName#
			SET 	
			<cfset local.fieldCount = 0 />
			<cfloop query="local.updateFields">
				<cfset local.loopField = local.updateFields.column_name/>
				<cfif structKeyExists(arguments.updateStruct, local.loopField)>
					<cfset local.loopValue = arguments.updateStruct[local.loopField] />
					<cfif local.loopValue neq "">
						<cfset local.fieldCount++ /><cfif local.fieldCount gt 1>,</cfif>
						#local.loopField# = <cfqueryparam cfsqltype="cf_sql_#local.updateFields.type_name#" value="#local.loopValue#" />
					</cfif>
				</cfif>
			</cfloop>
			WHERE 1 = 1
			<cfloop query="variables.tableKeys">
				<cfset local.loopField = variables.tableKeys.column_name />
				<cfif structKeyExists(arguments.updateStruct, local.loopField)>
					AND #local.loopField# = <cfqueryparam cfsqltype="cf_sql_#variables.tableKeys.type_name#" value="#arguments.updateStruct[local.loopField]#" />
				</cfif>
			</cfloop>
		</cfquery>
		<cfreturn true />
			
	</cffunction>
	
	<!--- Delete --->
	<cffunction name="delete" returntype="boolean" access="public">
		<cfargument name="deleteStruct" type="struct" required="true" />
		
		<cfif !structIsEmpty(arguments.deleteStruct)>
			<cfquery name="qDelete" datasource="#variables.dsn#">
				DELETE FROM #variables.tableName#
				WHERE 1 = 1
				<cfloop collection="#arguments.deleteStruct#" item="key">
					AND #key# = <cfqueryparam value="#arguments.deleteStruct[key]#" />
				</cfloop>
			</cfquery>
		<cfelse>
			<cfreturn false />
		</cfif>
		
		<cfreturn true />
	</cffunction>

	<!--- ------------------------ --->
	<!--- // Non CRUD functions \\ --->
	<!--- ------------------------ --->
	
	<!--- Query of Queries Filter  --->
	<cffunction name="filter" returntype="query" access="public">
		<cfargument name="query" type="query" required="true" />
		<cfargument name="where" type="string" required="true" />
		<cfargument name="fields" type="string" required="false" default="#variables.fieldList#" />
	
		<cfquery name="qFilter" dbtype="query">
			SELECT #arguments.fields#
			FROM arguments.query
			WHERE #preserveSingleQuotes(arguments.where)#
		</cfquery>
		
		<cfreturn qFilter />
	</cffunction>
	
	<!--- Sort Query  --->
	<cffunction name="sort" returntype="query" access="public">
		<cfargument name="query" type="query" required="true" />
		<cfargument name="sortFields" type="string" required="true" /><!--- can contain multiple fields and sort order direction --->
	
		<cfquery name="qFilter" dbtype="query">
			SELECT *
			FROM arguments.query
			ORDER BY #arguments.sortFields#
		</cfquery>
		
		<cfreturn qFilter />
	</cffunction>
	
	<!--- Join two query recordsets together...  --->
	<cffunction name="unite" returntype="query" access="public">
		<cfargument name="query1" type="query" required="true" />
		<cfargument name="query2" type="query" required="true" />
		<cfargument name="fields" type="string" required="false" default="#variables.fieldList#" />
	
		<cfquery name="qFilter" dbtype="query">
			SELECT #arguments.fields#
			FROM arguments.query1
			
			UNION
			
			SELECT #arguments.fields#
			FROM arguments.query2
		</cfquery>
		
		<cfreturn qFilter />
	</cffunction>
	
	<!--- Describe Table  --->
	<cffunction name="describe" returntype="query" access="private">
		<cfargument name="cache" type="boolean" required="false" default="true" />
	
		<cfset local.tableInfo = structNew() />
		
		<!--- there are 100 ways to cache this information. I chose to cache it in the application scope. If you prefer another method, adjust this freely to your liking --->
		<cfif !structKeyExists(application,"tables")><cfset application.tables = structNew() /></cfif>
		<cfif arguments.cache and structKeyExists(application.tables,variables.tableName)>
			<cfset local.tableInfo = application.tables[variables.tableName] />
		<cfelse>
			<cfdbinfo datasource="#variables.dsn#" type="columns" table="#variables.tableName#" name="local.tableInfo" />
			<cfif arguments.cache><cfset application.tables[variables.tableName] = local.tableInfo /></cfif>
		</cfif>
		
		<cfreturn local.tableInfo />
	</cffunction>

</cfcomponent>