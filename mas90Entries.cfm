<cfif IsDefined("form.journalType")>
	<cfset session.journalType = Trim(form.journalType)>
</cfif>

<cfif isDefined("url.resetjtype")>
	<cfset StructDelete(session, "journalType")>
</cfif>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Journal Entries</title>
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" />
		<link type="text/css" href="css/eon.css" rel="stylesheet" />  
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script> 
		<script type="text/javascript" src="js/jquery-ui-1.8.custom.min.js"></script> 
		
		<script type="text/javascript"> 
		$(function() {
			$("#entryDate").datepicker();
		});
		</script>
		
		<script type="text/javascript"> 
			
			var diff = 0;
			
			function SetJournal() {
				document.forms[0].submit();	
			}
			
			function resetJournal() {
				self.location.href = "mas90Entries.cfm?resetjtype=true";
			}
			
			function RecalcTotals() {
				var deb1 = document.getElementById('deb1').value;
				var deb2 = document.getElementById('deb2').value;
				var deb3 = document.getElementById('deb3').value;
				var deb4 = document.getElementById('deb4').value;
				var deb5 = document.getElementById('deb5').value;
				var deb6 = document.getElementById('deb6').value;
				var deb7 = document.getElementById('deb7').value;
				var deb8 = document.getElementById('deb8').value;
				var deb9 = document.getElementById('deb9').value;
				var deb10 = document.getElementById('deb10').value;
				var deb11 = document.getElementById('deb11').value;
				var deb12 = document.getElementById('deb12').value;
				
				if (deb1 == '' || deb1 == NaN) {deb1 = 0}
				if (deb2 == '' || deb2 == NaN) {deb2 = 0}
				if (deb3 == '' || deb3 == NaN) {deb3 = 0}
				if (deb4 == '' || deb4 == NaN) {deb4 = 0}
				if (deb5 == '' || deb5 == NaN) {deb5 = 0}
				if (deb6 == '' || deb6 == NaN) {deb6 = 0}
				if (deb7 == '' || deb7 == NaN) {deb7 = 0}
				if (deb8 == '' || deb8 == NaN) {deb8 = 0}
				if (deb9 == '' || deb9 == NaN) {deb9 = 0}
				if (deb10 == '' || deb10 == NaN) {deb10 = 0}
				if (deb11 == '' || deb11 == NaN) {deb11 = 0}
				if (deb12 == '' || deb12 == NaN) {deb12 = 0}
				
				var cred1 = document.getElementById('cred1').value;
				var cred2 = document.getElementById('cred2').value;
				var cred3 = document.getElementById('cred3').value;
				var cred4 = document.getElementById('cred4').value;
				var cred5 = document.getElementById('cred5').value;
				var cred6 = document.getElementById('cred6').value;
				var cred7 = document.getElementById('cred7').value;
				var cred8 = document.getElementById('cred8').value;
				var cred9 = document.getElementById('cred9').value;
				var cred10 = document.getElementById('cred10').value;
				var cred11 = document.getElementById('cred11').value;
				var cred12 = document.getElementById('cred12').value;
				
				if (cred1 == '' || cred1 == NaN) {cred1 = 0}
				if (cred2 == '' || cred2 == NaN) {cred2 = 0}
				if (cred3 == '' || cred3 == NaN) {cred3 = 0}
				if (cred4 == '' || cred4 == NaN) {cred4 = 0}
				if (cred5 == '' || cred5 == NaN) {cred5 = 0}
				if (cred6 == '' || cred6 == NaN) {cred6 = 0}
				if (cred7 == '' || cred7 == NaN) {cred7 = 0}
				if (cred8 == '' || cred8 == NaN) {cred8 = 0}
				if (cred9 == '' || cred9 == NaN) {cred9 = 0}
				if (cred10 == '' || cred10 == NaN) {cred10 = 0}
				if (cred11 == '' || cred11 == NaN) {cred11 = 0}
				if (cred12 == '' || cred12 == NaN) {cred12 = 0}
				
				var debTotal = parseFloat(deb1) + parseFloat(deb2) + parseFloat(deb3) + parseFloat(deb4) + parseFloat(deb5) + parseFloat(deb6) + parseFloat(deb7) + parseFloat(deb8) + parseFloat(deb9) + parseFloat(deb10) + parseFloat(deb11) + parseFloat(deb12);
				
				var credTotal = parseFloat(cred1) + parseFloat(cred2) + parseFloat(cred3) + parseFloat(cred4) + parseFloat(cred5) + parseFloat(cred6) + parseFloat(cred7) + parseFloat(cred8) + parseFloat(cred9) + parseFloat(cred10) + parseFloat(cred11) + parseFloat(cred12);
				
				
				diff = (parseFloat(debTotal) - parseFloat(credTotal)).toFixed(2);
				
				document.getElementById('diff').value = parseFloat(diff);
				//console.log(diff);
			}
			
			function validateForm() {
				if (diff != 0) {
					alert('Please make sure the debits and credits are in balance.');
				}
				else {
					document.forms[0].submit();
				}
			}
					
		</script>
</head>

<body>
    <form id="form1" name="form1" method="post" action="mas90Entries.cfm">
	<cfif NOT IsDefined("session.journalType")>
	 <div style="margin:20px">   
		<label for="journalType">Select a Journal Type: </label>
        
         <select name="journalType" id="journalType">
              <option value="JE">JE</option>
              <option value="TJ">TJ</option>
          </select>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <input type="button" value="Continue" name="setJournal" onclick="SetJournal()" />
          
          </div>
    <cfelse>         
		<div style="margin:20px; width:670px">	     
			<table width="309" border="0">
			  <tr>
			    <th width="114" scope="row"><div align="left">Journal Type:</div></th>
			    <td width="119">
					<cfoutput>#session.journalType#</cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="resetJournal()" style="color:red">Reset</a>
			     </td>
			  </tr>
			  <tr>
			    <th scope="row"><div align="left">Posting Date:</div></th>
			    <td><input name="entryDate" id="entryDate" maxlength="12" size="10" type="text" value="" /></td>
			  </tr>
			 </table>
			
			<table width="250" border="1">
			  <tr>
			    <td><div align="center">Account</div></td>
			    <td><div align="center">Debits</div></td>
			    <td><div align="center">Credits</div></td>
			    <td>Comments</td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc1" id="acc1" autocomplete="off" /></td>
			    <td><input type="text" name="deb1" id="deb1" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred1" id="cred1" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment1" id="comment1" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc2" id="acc2" autocomplete="off" /></td>
			    <td><input type="text" name="deb2" id="deb2" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred2" id="cred2" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment2" id="comment2" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc3" id="acc3" autocomplete="off" /></td>
			    <td><input type="text" name="deb3" id="deb3" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred3" id="cred3" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment3" id="comment3" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc4" id="acc4" autocomplete="off" /></td>
			    <td><input type="text" name="deb4" id="deb4" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred4" id="cred4" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment4" id="comment4" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc5" id="acc5" autocomplete="off" /></td>
			    <td><input type="text" name="deb5" id="deb5" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred5" id="cred5" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment5" id="comment5" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc6" id="acc6" autocomplete="off" /></td>
			    <td><input type="text" name="deb6" id="deb6" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred6" id="cred6" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment6" id="comment6" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc7" id="acc7" autocomplete="off" /></td>
			    <td><input type="text" name="deb7" id="deb7" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred7" id="cred7" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment7" id="comment7" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc8" id="acc8" autocomplete="off" /></td>
			    <td><input type="text" name="deb8" id="deb8" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred8" id="cred8" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment8" id="comment8" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc9" id="acc9" autocomplete="off" /></td>
			    <td><input type="text" name="deb9" id="deb9" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred9" id="cred9" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment9" id="comment9" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc10" id="acc10" autocomplete="off" /></td>
			    <td><input type="text" name="deb10" id="deb10" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred10" id="cred10" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment10" id="comment10" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc11" id="acc11" autocomplete="off" /></td>
			    <td><input type="text" name="deb11" id="deb11" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred11" id="cred11" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment11" id="comment11" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td><input type="text" name="acc12" id="acc12" autocomplete="off" /></td>
			    <td><input type="text" name="deb12" id="deb12" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="cred12" id="cred12" autocomplete="off" onblur="RecalcTotals()" /></td>
			    <td><input type="text" name="comment12" id="comment12" autocomplete="off" /></td>
			  </tr>
			  <tr>
			  	<td>Difference:</td>
			  	<td colspan="3"> <input type="text" id="diff" name="diff" readonly="readonly" size="15" value="" /></td>
			  </tr>
			</table>
		<div style="padding:10px">
		<input type="button" id="submitEntries" name="submitEntries" value="Save" onclick="validateForm()" />
		</div>
		</div>
		
		<cfif isdefined("form.entryDate")>
			<cfset variables.postDate = DateFormat(form.entryDate,"mm/dd/yyyy")>
			
			<cfloop index="i" from="1" to="12">
				<cfset variables.code = form["acc" & i]> 
				<cfset variables.deb = form["deb" & i]>
				<cfset variables.cred = form["cred" & i]>
				<cfset variables.note = form["comment" & i]>				
				
				<cfset variables.amount = 0>
				
				<cfif variables.deb GT 0>
					<cfset variables.amount = variables.deb>
					<cfset variables.entryType = 1>
				<cfelseif variables.cred GT 0>
					<cfset variables.amount = variables.cred>
					<cfset variables.entryType = 2>
				</cfif>
				
				<cfif variables.amount GT 0 AND len(trim(variables.code)) eq 9 and len(trim(session.journalType))>
					<cfset application.daoObj.Insertmas90Entry(variables.code,variables.note,variables.postDate,variables.amount,variables.entryType,session.journalType,'misc')>
				</cfif>
			</cfloop>	
			
		</cfif>
</cfif>


</form>
</body>
</html>