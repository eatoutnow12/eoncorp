<cffile action = "read" file = "#ExpandPath('./')#\payroll\payroll.csv" variable = "Report" charset="ISO-8859-1">

<cfset myObj = createObject("component", "cfc.utils")>
<cfset daoObj = application.daoObj>

<!--- Convert the CSV to an array of arrays.--->
<cfset arrCSV = myObj.CSVToArray(
	CSVData = Report,
	Delimiter = ",",
	Qualifier = """"
	) />	
	 
<cftry>

	<cftransaction>

		<cfloop from="1" to="#arrayLen(arrCSV)#" index="i">
			<cfset dataArr = arrCSV[i]>
			<cfset amount = 0>
			
			<cfset dateOfPosting = dataArr[1]>
			<cfset dateOfPosting = dateFormat(dateOfPosting,"mm/dd/yyyy")>
			
			<cfset code = Trim(dataArr[2])>
			<cfset note = Trim(dataArr[3])>
			<cfset debit = dataArr[4]>
			<cfset credit = dataArr[5]>
			
			<cfif debit GT 0>
				<cfset amount = debit>
				<cfset transType = 1>
			<cfelseif credit GT 0>
				<cfset amount = credit>
				<cfset transType = 2>
			</cfif>
			
			<cfif amount gt 0 and len(code) eq 9>
				<cfset amount = DecimalFormat(amount)>
				<cfset amount = LSParseNumber(amount)>
				<cfset application.daoObj.Insertmas90Entry(code,note,dateOfPosting,amount,transType,'JE','cc')>
			</cfif>
		</cfloop>
		
	</cftransaction>
	
	<cfcatch type="any">
		Import Failed	
	</cfcatch>
</cftry>
		
