﻿<cfparam name="url.err" default="">
<cfset variables.error = Trim(url.err)>
<cfscript>
	StructClear(session);
</cfscript>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/eon.css">
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>EON Corp Reports</title>
		
		<script type="text/javascript">
			function ValidateMe() {
				document.forms[0].submit();
			}
		
		</script>
		
		
		<style>
			#passwordContainer {
				padding-top: 5px;
			}
			
			#masterContainer {
				margin-top:10%; 
				margin-left:30%;
			}
			
			#messageContainer {
				margin-left:450px;
				margin-top:50px;
				color: #DD4B39;
				height:20px;
				font-weight:bold;
			}
			
			#loginButtonContainer {
				padding-top: 5px;
			}
		</style>
		
	</head>	
	<body>
	
	<div id="messageContainer">
		<cfoutput>#variables.error#</cfoutput>
	</div>
	
	<div id="masterContainer">
		<h3 style="text-decoration:underline;">EON Corp Reports</h3>
		
		<form action="login_eng.cfm" method="post">
			<div id="loginContainer" style="text-align:left;width:450px; padding:70px; border:double 2px #c6c6c6">
			
				<div id="userNameContainer" align="right">
					<label for="userName">email address/user Id:</label>
					<input id="userName" name="userName" type="text" size="40" maxlength="100" autocomplete="off" /> 
				</div>
				
				<div id="passwordContainer" align="right">
					<label for="password">password:</label>
					<input id="password" name="password" type="password" size="40" maxlength="100" autocomplete="off" /> 
				</div>
				
				<div id="loginButtonContainer" align="right">
					<input type="submit" name="login" value="Sign In" />
				</div>
				
			</div>
		</form>
	</div>
</body>	

</html>
		