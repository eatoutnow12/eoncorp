<cfif application.dsrNoError EQ 1>
			<cfscript>
				objUtil = createObject("component", "cfc.Utils");	
			</cfscript>
			<cfset today = Now()>
			<cfset CurrentYear = Year(today)>
			<cfset PreviousYear = CurrentYear - 1>	
			<cfset dayNum = dayOfWeek(today)>
			<cfset daysInPast = 0>
			<cfset ctr = 1>
			<cfif dayNum LT 3>
				 <cfset daysInPast = dayNum + 5>
			<cfelse>
				 <cfset daysInPast = dayNum - 2>
			</cfif>	
			
			<!---For SOS--->
			<cfset sosDayNum = dayOfWeek(now())>
			<cfset sosDayNum = sosDayNum-2>
			
			<cfif sosDayNum EQ 0>
				<cfset sosDayNum = 7>
			<cfelseif sosDayNum EQ -1>
				<cfset sosDayNum = 6> 
			</cfif>
			
			<cfset startDate = dateFormat(DateAdd("d",today,(daysInpast * -1)),application.eonDateFormat)>
			<cfset endDate = dateFormat(DateAdd("d",today,-1),application.eonDateFormat)>
			
			<cfset reportTitle = "Key Performance Indicators: " & startDate  & " to " & endDate>
			
			<cfquery name="getStores" datasource="#application.eonDsn#">
					SELECT [businessUnit].[id], [businessUnit].[unitName]
					FROM [businessUnit]
					WHERE inactiveSinceDate IS NULL
					ORDER BY [unitName]
			</cfquery>
						
							<cfquery name="getCalendarCurrent" datasource="#application.eonDsn#">
								SELECT StartDate from businessCalendar
								where year = '#CurrentYear#' 
								AND businessEntityId = (SELECT entityId FROM businessUnit WHERE id= '#getStores.id#')
							</cfquery>
							
							<cfset currentDateAsString = DateFormat(getCalendarCurrent.StartDate, application.eonDateFormat)>
							
							<cfset periodForDate = objUtil.getPeriod(currentDateAsString, startDate)>
							<cfset weekForDate = objUtil.getWeek(currentDateAsString, startDate)>
							<cfset dayForDate = objUtil.getDay(startDate)>
							
							<cfquery name="getCalendarPrior" datasource="#application.eonDsn#">
								SELECT StartDate from businessCalendar
								where year = '#PreviousYear#' 
								AND businessEntityId = (SELECT entityId FROM businessUnit WHERE id= '#getStores.id#')
							</cfquery>
							<cfset priorDateAsString = DateFormat(getCalendarPrior.StartDate, application.eonDateFormat)>
							
						<cfif getCalendarPrior.RecordCount>
							
							<cfset priorStartDate = objUtil.getDateByPeriodWeekDay(priorDateAsString, periodForDate, weekForDate, dayForDate)>
							<cfset priorStartDate = DateFormat(priorStartDate, application.eonDateFormat)>
							<cfset priorEndDate = dateAdd("d", priorStartDate, (daysInPast-1))>
							<cfset priorEndDate = DateFormat(priorEndDate, application.eonDateFormat)>
						</cfif>
			
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
		<html>
			<head>
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" /> 
				<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
				
			</head>			
		
		<body>
		<cfsavecontent variable="miniDSR">
				<style>
					
					body {				
						font-family:monospace;
						font-size:14pt;
						font-weight:bold;
					}
					
					.headerRow {
				 		color: #FFFFFF;
						background-color:#000;
						padding-left:10px;
				 	}
								
					td {
						width:inherit;
						padding-left:10px;
					}
		
					
				</style>
		<cfoutput>
				<h3>#reportTitle#</h3>
				<table>
				
				<tr class="headerRow">
					<td align="center">
					##
					</td>
					<td>
					Net Sale
					</td>
					<td>
					Guests
					</td>					
					<td>
					Paid Outs
					</td>
					<td>
					Coupons $
					</td>
					<td>
					Promos $
					</td>
					<td>
					Cash +/-
					</td>
					<td>
					Deletes %
					</td>
					<td>
					Labor %
					</td>
					<td>
					YOY % Diff
					</td>					
					<td>
					Avg SOS
					</td>
					
				</tr>
			
				<cfloop query="getStores">
					<cfquery name="getStoreSalesData" datasource="#application.eonDsn#">
						SELECT TOP 1	
						NetSales = (SELECT SUM([credit]) FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#getStores.id#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#')
						,PaidOuts = (SELECT SUM([debit]) FROM [dailySales] 
						WHERE [note] = 'Paid outs' AND [businessUnitId] = '#getStores.id#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#')
						,Coupons = (SELECT SUM([debit]) FROM [dailySales] 
						WHERE [note] = 'Coupon $' AND [businessUnitId] = '#getStores.id#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#')
						,Promos = (SELECT SUM([debit]) FROM [dailySales] 
						WHERE [note] = 'Promos $' AND [businessUnitId] = '#getStores.id#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#')
						,CashOverShort = (SELECT dbo.GetCashOverShortByStoreAndDateRange('#getStores.id#','#startDate#','#endDate#'))
						,Deletes = (SELECT SUM(deletesNum) FROM dailyGeneralStoreData WHERE [businessUnitId] = '#getStores.id#'
						AND [dateOccured]  BETWEEN '#startDate#' and '#endDate#')
						,CustCount = (SELECT SUM(customerCount) FROM dailyGeneralStoreData WHERE [businessUnitId] = '#getStores.id#'
						AND [dateOccured]  BETWEEN '#startDate#' and '#endDate#')
						,LabourDollar = (SELECT SUM(laborDollar) FROM dailyGeneralStoreData WHERE [businessUnitId] = '#getStores.id#'
						AND [dateOccured]  BETWEEN '#startDate#' and '#endDate#')
						,sos = (SELECT [dbo].[GetWTDSosByStoreAndDateRange] ('#getStores.id#','#startDate#','#endDate#',#sosDayNum#))
						,PriorNetSales = (SELECT SUM([credit]) FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#getStores.id#'
						AND [dateOfSale] BETWEEN '#priorStartDate#' AND '#priorEndDate#')
						
						FROM [dailySales]
						WHERE [businessUnitId] = '#getStores.id#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#'
						GROUP BY [dailySales].[dateOfSale], [businessUnitId]
					</cfquery>
					<cfset labourPercent = DecimalFormat((getStoreSalesData.LabourDollar/getStoreSalesData.NetSales) * 100)>
					<cfset deletesPercent = DecimalFormat((getStoreSalesData.Deletes/getStoreSalesData.NetSales) * 100)>
					<cfset criticalCashOverShort = (getStoreSalesData.NetSales * 0.001)>
					<cfset absCashOverShort = abs(getStoreSalesData.CashOverShort)>
					<cfset yoyDiff = DecimalFormat(Evaluate((getStoreSalesData.NetSales - getStoreSalesData.PriorNetSales)/getStoreSalesData.PriorNetSales) * 100)>
			
				<tr bgcolor="###iif(ctr MOD 2,DE('efefef'),DE('ffffff'))#">
					<td align="center">
					<strong>#getStores.unitName#</strong>
					</td>
					<td>
					#DollarFormat(getStoreSalesData.NetSales)#
					</td>
					<td>
					#getStoreSalesData.CustCount#
					</td>					
					<td>
					#DollarFormat(getStoreSalesData.PaidOuts)#
					</td>
					<td>
					#DollarFormat(getStoreSalesData.Coupons)#
					</td>
					<td>
					#DollarFormat(getStoreSalesData.Promos)#
					</td>
					<td <cfif absCashOverShort GT criticalCashOverShort> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>			
					#DollarFormat(getStoreSalesData.CashOverShort)#
					</td>
					<td>
					#deletesPercent#
					</td>
					<td <cfif labourPercent GT 21.5> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
					#labourPercent#
					</td>

					<td <cfif Sgn(yoyDiff) EQ -1> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>			
					#yoyDiff#
					</td>
					
					<td <cfif getStoreSalesData.sos GTE 181 AND getStoreSalesData.sos LTE 210> bgcolor="##F88017" style="color:##FFFFFF"<cfelseif getStoreSalesData.sos GT 210> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
					#getStoreSalesData.sos#	
					</td>
				</tr>
				<cfset ctr++>
				</cfloop>
				
				</table>
		</cfoutput>
			
		</cfsavecontent>
		
				<cfset mailAttributes = {
					from="EON Reports <reports@eatoutnow.net>",
					to="management@eatoutnow.net",
					cc="allstores@eatoutnow.net",
					subject="#reportTitle#"
				}
				/>
				
				<cfmail attributeCollection="#mailAttributes#" type="html">
					#miniDSR#
				</cfmail>
		
		</body>
		
		</html>
</cfif>