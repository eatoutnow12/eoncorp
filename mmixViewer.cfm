﻿<cfinclude template="header.cfm">

<style type="text/css">
	tr.lightrow td {
	background-color: #FFFFFF;
	}
	tr.darkrow td {
	background-color: #EFEFEF;
	}
</style>
		
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" /> 
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script> 
		<script type="text/javascript" src="js/jquery-ui-1.8.custom.min.js"></script> 
		
		<script type="text/javascript"> 
		$(function() {
			$("#startDate").datepicker();
		});
		
		$(function() {
			$("#endDate").datepicker();
		});	
	

		</script>

		<script type="text/javascript">
			function ValidateSubmit()
			{
				document.forms[0].submit();
			}	
		</script>
<div style="margin:10px">

	<h3>Menu Mix Viewer</h3><br>
	
	<cfquery name="getItems" datasource="#application.eonDsn#">
		SELECT     itemId, itemDescription
		FROM         storeMenuMixItems
		ORDER BY itemDescription
	</cfquery>
	
	
	<cfoutput><form action="#getFileFromPath( cgi.script_name )#" method="post"></cfoutput>
		<cfif NOT IsDefined("cookie.storeId")>
					<label for="bUnit">Select Store:</label>	
					<select id="bUnit" name="bUnit">
						<cfloop query="application.getAllUnits">
							<cfoutput>
				            	<option id="#Trim(unitName)#" value="#Trim(id)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ id>selected</cfif>>
								#Trim(unitName)#
								</option>
				            </cfoutput>
						</cfloop>
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<cfelse>
					<cfoutput><input type="hidden" name="bUnit" value="#cookie.storeId#"></cfoutput>
		</cfif>
	 
						<label for="startDate">Date:</label>			
						<cfoutput><input name="startDate" id="startDate" maxlength="12" size="10" type="text"<cfif IsDefined("form.startDate")> value="#form.startDate#"<cfelse>value=""</cfif> /></cfoutput>
						
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<label for="itemId">Item:</label>
						<select id="itemId" name="itemId">
							<option id="0" value="0">All</option>
							<cfloop query="getItems">
								<cfoutput>
									
					            	<option id="#Trim(itemId)#" value="#Trim(itemId)#" <cfif IsDefined("form.itemId") AND form.itemId EQ itemId>selected</cfif>>
									#Trim(itemDescription)#
									</option>
					            </cfoutput>
							</cfloop>
						</select>
						
					<input type="button"  onclick="ValidateSubmit()" value="Show Menu Mix"></p>
	
	</form>	
	
<cfif IsDefined("form.bUnit")>
	<cfset startDate = Trim(form.startDate)>
	<cfset startDate = dateFormat(startDate,application.eonDateFormat)>
	<cfset variables.storeId = Trim(form.bUnit)>

	<cfif NOT isDate(startDate)>
		<cfset startDate = dateFormat(dateadd("d",Now(),-1),application.eonDateFormat)>
	</cfif>
	
	<cfquery name="getMmix" datasource="#application.eonDsn#">
		SELECT qtySold, netSales, storeMenuMixItems.itemDescription
		FROM storeMenuMixSales
		INNER JOIN storeMenuMixItems
		ON storeMenuMixItems.itemId = storeMenuMixSales.itemId
		WHERE businessUnitId = '#variables.storeId#'
		AND businessDate = '#startDate#'
		<cfif form.itemId GT 0>
		AND storeMenuMixSales.itemId = #form.itemId#
		</cfif>
		ORDER BY itemDescription
	</cfquery>

	<table width="600" style="border:green double 2px;margin:5px">
		<tr>
			<td><b>Item Name</b></td>
			<td><b>Quantity</b></td>
			<td><b>Sales</b></td>
		</tr>

		<cfset idx = 1>
		<cfloop query="getMmix">	
			<cfset idx++>
			<cfset rowclass = "lightrow"> 
			<cfif idx mod 2>
				<cfset rowclass = "darkrow">
			</cfif>

				<cfoutput>
		        	<tr class="#rowclass#">
						<td>
							#itemDescription#
						</td>
						
						<td>
							#qtySold#
						</td>
						
						<td>
							#dollarFormat(netSales)#
						</td>

					</tr>
		        </cfoutput>

		</cfloop>
		
	</table>
</cfif>


</div>

<cfinclude template="footer.cfm">