﻿	<cfif NOT isDefined("cookie.storeId") OR Len(Trim(cookie.storeId)) EQ 0>
		<cfset variables.error = "This store/computer is currently not setup for use at this location.<br>Please contact the EatOutNow IT department by calling 402-408-6351.">
		<cflocation url="../login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	<cfelseif NOT isDefined("session.userId")>
		<cfset variables.error = "Your session has timed out. Please log-in again.">
		<cflocation url="../login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	<cfelse>
		<cfset variables.storeId = cookie.storeId>
		<cfset variables.userId = session.userId>
	</cfif>


<cfquery name="getItems" datasource="#application.eonDsn#">
			SELECT
				   [itemNumber]      
			      ,[itemDescription]
			      ,[storeCategoryId]
				  ,unit = (SELECT itemUnitLabel FROM storeItemUnits WHERE [itemUnitId] = purchaseItems.[itemUnitId])
				  ,inProcess = 0
				  FROM dbo.purchaseItems
			WHERE 1 = 1
			AND itemStatus = 1
UNION
			SELECT itemNumber
			,itemDescription
			,storeCategoryId
			,unit = (SELECT itemUnitLabel FROM storeItemUnits WHERE [itemUnitId] = storeInProcessItems.[itemUnitId])
			,inProcess = id
			FROM dbo.storeInProcessItems
			WHERE 1 = 1
			AND itemStatus = 1
			ORDER BY itemDescription
</cfquery>

<cfquery name="getStores" dbtype="query">
	SELECT * FROM application.getAllUnits
	WHERE id != '#variables.storeId#'
</cfquery>

<cfinclude template="../header.cfm">
<cfmodule template="../checkaccess.cfm" reportId="7">

<style>

</style>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" /> 		
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script> 
<script type="text/javascript" src="../js/jquery-ui-1.8.custom.min.js"></script> 

<script type="text/javascript">	
	
	$(function() {
			$("#transferDate").datepicker();
	});
	
	function validateAndSend()
	{
		var storeId = document.getElementById('toStoreId');
		var itemNumber = document.getElementById('itemNumber');
		var itemQty = document.getElementById('itemQty');
		var transferDate = document.getElementById('transferDate');
		var inProcVal = itemNumber.value.split('^')[1];

		var priceType = unitSelectInp.value;

		var itemNum = itemNumber.value.split('^')[0];

		if (storeId.value != 0 && itemNumber.value != 0 && itemQty.value != 0 && checkdate(transferDate))
		{
			$.post("../dbProxy.cfm", { action: 'InsertInventoryTransfer', fromStoreId: <cfoutput>'#variables.storeId#'</cfoutput>,  toStoreId: storeId.value, item: itemNum, itemQty: itemQty.value,  transferDate: transferDate.value, userId: <cfoutput>'#variables.userId#'</cfoutput>, inProcess:inProcVal, priceType: priceType}, function(data){document.location.reload();} );	
		}
		else {
			alert('Please make sure that all fields are filled-in properly, including Store #, Item, Quantity and Date.');
		}
	}
	
	function getItemData(itemNumberAndStatus) {
		
		var i;
		for(i=unitSelectInp.options.length-1;i>=0;i--)
		{
			unitSelectInp.remove(i);
		}
		
		
		var elOptNew = document.createElement('option');
		elOptNew.text = 'Case';
		elOptNew.value = 0;
		
		try {
			unitSelectInp.add(elOptNew, null); // standards compliant; doesn't work in IE
		} 
		catch (ex) {
			unitSelectInp.add(elOptNew); // IE only
		}		
		
		var itemNumber = itemNumberAndStatus.split('^')[0];
		var inProcessStat = itemNumberAndStatus.split('^')[1];
		inProcVal = inProcessStat;
		var price = '0';
		var unit = 'None';
		priceContainer.innerHTML = '';
		$.post("../dbProxy.cfm", { action: 'getItemData', item: itemNumber, inProcess: inProcessStat  }, function(data){  
			var elOptNew = document.createElement('option');
			elOptNew.text = data.split(':')[1];
			elOptNew.value = 1;

			if (unitSelectInp.length > 1) {
					unitSelectInp.remove(unitSelectInp.length - 1);
			}
			if (elOptNew.text != 'Case') {				
				try {
					unitSelectInp.add(elOptNew, null); // standards compliant; doesn't work in IE
				} 
				catch (ex) {
					unitSelectInp.add(elOptNew); // IE only
				}
			}
			
			if (inProcVal == '1' || inProcVal == '2' || inProcVal == '3' || inProcVal == '4')
			{
				unitSelectInp.remove(0);
			}
			
			priceContainer.innerHTML = '$' + data.split(':')[0];	
			unitSelectInp.disabled = false;
		} );
		

	}
	
	function getItemPrice(priceType)
	{		
		var itemNumber = priceSelectInp.options[priceSelectInp.selectedIndex].value;
		priceContainer.innerHTML = '';
		$.post("../dbProxy.cfm", { action: 'getItemPrice', item: itemNumber, priceType: priceType}, function(data){
				priceContainer.innerHTML = '$' + data;			
			} );
	}
	
	function deleteTransfer(store,itemDesc,dated,totalAmount,transferId)
	{
		var msg = 'Are you sure you want to DELETE this transfer:\n' + 'Sent to store: ' + store + '\n' + 'On: ' + dated + '\n' + 'For Item: ' + itemDesc + '\n' + 'Total Amount: ' + totalAmount; 
		if (confirm(msg)) {
			$.post("../dbProxy.cfm", { action: 'DeleteInventoryTransfer', transferId: transferId, userId: <cfoutput>'#variables.userId#'</cfoutput>}, function(data){document.location.reload();} );
		}
	}
	
	function acceptTransfer(store,itemDesc,dated,totalAmount,transferId)
	{
		var msg = 'Are you sure you want to ACCEPT this transfer:\n' + 'Sent to store: ' + store + '\n' + 'On: ' + dated + '\n' + 'For Item: ' + itemDesc + '\n' + 'Total Amount: ' + totalAmount; 
		if (confirm(msg)) {
			$.post("../dbProxy.cfm", { action: 'AcceptInventoryTransfer', transferId: transferId, userId: <cfoutput>'#variables.userId#'</cfoutput> }, function(data){document.location.reload();} );
		}
	}

</script>

	<div style="margin-left:100px;">
		<h2>Inventory Transfers</h2>
	
		<form name="doTransferForm">
			<h3>Enter new transfer:</h3>
			<table width="900" style="border:green double 2px;margin:20px">
				<tr>
					<td>
					Transfer Date
					</td>
					<td>
					Transfer to store
					</td>
					<td>
					Inventory Item	
					</td>
					<td>
					Unit
					</td>
					<td>
					Price	
					</td>
					<td>
					Quantity
					</td>
				</tr>
				
				<tr>
					<td>
					<input name="transferDate" id="transferDate" maxlength="12" size="10" type="text" />
					</td>
					<td>
						<select name="toStoreId" id="toStoreId">
								<option value="0" selected="true">Select receiving store</option>
							<cfoutput>
								<cfloop query="getStores">							
									<option value="#id#">#unitName#</option>
								</cfloop>
							</cfoutput>
						</select>
					</td>
					<td>
						<select name="itemNumber" id="itemNumber" onchange="getItemData(this.options[this.selectedIndex].value)">
								<option value="0" selected="true">Select an Inventory Item</option>
							<cfoutput>
								<cfloop query="getItems">						
									<option value="#itemNumber#^#inProcess#">#itemDescription# &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #itemNumber# </option>
								</cfloop>
							</cfoutput>
						</select>
					</td>
					<td>
						<select id="unitSelect" name="unitSelect" disabled="true" onchange="getItemPrice(this.options[this.selectedIndex].value)">
							
						</select>
					</td>
					<td>
						<div id="priceContainer" />
					</td>
					<td>
						<input type="text" name="itemQty" id="itemQty" value="" size="5" onblur="IsNumeric(this)" class="inputNoErr" autocomplete="off">
					</td>
				</tr>	
				<tr>
					<td colspan="6" align="right">
						<input type="button" id="btnSubmit" value="Send" onclick="validateAndSend()">
					</td>
				</tr>
			</table>
			
			
		</form>
		<br>
	
	<cfquery name="getTransfersSent" datasource="#application.eonDsn#">
		SELECT storeInventoryTransfers.[id] AS transferId
		      ,[dateOfTransfer]
		      ,dbo.businessUnit.unitName
		      ,dbo.purchaseItems.itemDescription
		      ,[unitCost]
		      ,[quantity]
		      ,users.userFName + ' ' + users.userLName AS enteredByUser
 		      ,[dateEntered]
		      ,acceptedByUser = (SELECT users.userFName + ' ' + users.userLName FROM users WHERE id = storeInventoryTransfers.acceptedByUser)
		      ,[dateAccepted]
		  FROM [storeInventoryTransfers]
		  INNER JOIN dbo.businessUnit
		  ON dbo.businessUnit.id = storeInventoryTransfers.toStoreId
		  INNER JOIN dbo.purchaseItems
		  ON storeInventoryTransfers.itemNumber = purchaseItems.itemNumber
		  INNER JOIN dbo.users
		  ON storeInventoryTransfers.enteredByUser = users.id
		  WHERE fromStoreId = '#variables.storeId#'
		  AND dateAccepted IS NULL
		  AND dateDeleted IS NULL
		  ORDER BY dateOfTransfer DESC
	</cfquery>
	
	<cfquery name="getTransfersReceived" datasource="#application.eonDsn#">
		SELECT storeInventoryTransfers.[id] AS transferId
		      ,[dateOfTransfer]
		      ,dbo.businessUnit.unitName
		      ,dbo.purchaseItems.itemDescription
		      ,[unitCost]
		      ,[quantity]
		      ,users.userFName + ' ' + users.userLName AS enteredByUser
 		      ,[dateEntered]
		      ,acceptedByUser = (SELECT users.userFName + ' ' + users.userLName FROM users WHERE id = storeInventoryTransfers.acceptedByUser)
		      ,[dateAccepted]
		  FROM [storeInventoryTransfers]
		  INNER JOIN dbo.businessUnit
		  ON dbo.businessUnit.id = storeInventoryTransfers.fromStoreId
		  INNER JOIN dbo.purchaseItems
		  ON storeInventoryTransfers.itemNumber = purchaseItems.itemNumber
		  INNER JOIN dbo.users
		  ON storeInventoryTransfers.enteredByUser = users.id
		  WHERE toStoreId = '#variables.storeId#'
		  AND dateAccepted IS NULL
		  AND dateDeleted IS NULL
		  ORDER BY dateOfTransfer DESC
	</cfquery>
	
		<h3>Transfers Inbox:</h3>
		<table width="900" style="border:green double 2px;margin:20px">
			<tr>
				<td>
					<b>Date</b>
				</td>
				<td>
					<b>From Store #</b>
				</td>
				<td>
					<b>Created by</b>
				</td>
				<td>
					<b>Item</b>
				</td>
				<td>
					<b>Qty</b>
				</td>
				<td>
					<b>Cost</b>
				</td>
				<td>
					<b>Total</b>
				</td>
				<td>
					<b>Accept</b>
				</td>					
			</tr>
<cfoutput>
				<cfloop query="getTransfersReceived">
				<tr>
					<td>
						#dateFormat(dateofTransfer,application.eonDateFormat)#
					</td>
					<td>
						#unitName#
					</td>
					<td>
						#enteredByUser#
					</td>
					<td>
						#itemDescription#
					</td>
					<td>
						#quantity#
					</td>
					<td>
						#dollarFormat(unitCost)#
					</td>	
					<td>
						#dollarFormat(unitCost*quantity)#
					</td>
					<td>
						<input type="image" 
						src="../img/icons/accept.png" 
						width="16" 
						height="16" 
						alt="Click to accept this transfer" 
						title="Click to accept this transfer" 
						onclick="acceptTransfer('#JSStringFormat(unitName)#','#JSStringFormat(HTMLEditFormat(itemDescription))#','#JSStringFormat(dateFormat(dateofTransfer,application.eonDateFormat))#','#JSStringFormat(dollarFormat(unitCost*quantity))#','#transferId#');"  />
					</td>		
				</tr>
				</cfloop>
</cfoutput>
		</table>
		<br>
		<h3>Transfers Outbox:</h3>
		<table width="900" style="border:green double 2px;margin:20px">
			<tr>
				<td>
					<b>Date</b>
				</td>
				<td>
					<b>To Store #</b>
				</td>
				<td>
					<b>Created by</b>
				</td>
				<td>
					<b>Item</b>
				</td>
				<td>
					<b>Qty</b>
				</td>
				<td>
					<b>Cost</b>
				</td>
				<td>
					<b>Total</b>
				</td>
				<td>
					<b>Delete</b>
				</td>						
			</tr>
<cfoutput>
				<cfloop query="getTransfersSent">
				<tr>
					<td>
						#dateFormat(dateofTransfer,application.eonDateFormat)#
					</td>
					<td>
						#unitName#
					</td>
					<td>
						#enteredByUser#
					</td>
					<td>
						#itemDescription#
					</td>
					<td>
						#quantity#
					</td>
					<td>
						#dollarFormat(unitCost)#
					</td>	
					<td>
						#dollarFormat(unitCost*quantity)#
					</td>	
					<td>
						<input type="image" src="../img/icons/delete.png" 
						width="16" 
						height="16" 
						alt="Click to delete this transfer" 
						title="Click to delete this transfer"
						onclick="deleteTransfer('#JSStringFormat(unitName)#','#JSStringFormat(HTMLEditFormat(itemDescription))#','#JSStringFormat(dateFormat(dateofTransfer,application.eonDateFormat))#','#JSStringFormat(dollarFormat(unitCost*quantity))#','#transferId#');" />
					</td>	
				</tr>
				</cfloop>
</cfoutput>
		</table>
	
	
	</div>
	
	
	
<cfinclude template="../footer.cfm" />
<script type="text/javascript">
		var priceContainer = document.getElementById('priceContainer');
		var unitSelectInp = document.getElementById('unitSelect');
		var priceSelectInp = document.getElementById('itemNumber');
</script>