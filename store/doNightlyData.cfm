﻿	<cfif NOT isDefined("cookie.storeId") OR Len(Trim(cookie.storeId)) EQ 0>
		<cfset variables.error = "This store/computer is currently not setup for use at this location.<br>Please contact the EatOutNow IT department by calling 402-408-6351.">
		<cflocation url="../login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	<cfelseif NOT isDefined("session.userId")>
		<cfset variables.error = "Your session has timed out. Please log-in again.">
		<cflocation url="../login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	<cfelse>
		<cfset variables.storeId = cookie.storeId>
		<cfset variables.userId = session.userId>
		<cfset variables.today = DateFormat(now(),application.eonDateFormat)>
	</cfif>

	<cfinclude template="../header.cfm">
	<cfmodule template="../checkaccess.cfm" reportId="8">
	
	<cfset weekDay = dayofweek(now())>
	
	<cfif weekDay EQ 1>
		<cfset dayFactor = 6>
	<cfelseif weekDay EQ 2>
		<cfset dayFactor = 7>
	<cfelse>
		<cfset dayFactor = weekDay-2>
	</cfif>
	
	<cfset dayFactor = (dayFactor)*-1>
	
	<cfset startDate = dateAdd("d",dayFactor,now())>
	<cfset endDate = dateAdd("d",6,startDate)>
	
	<cfset startDate = dateFormat(startDate,application.eonDateFormat)>
	<cfset endDate = dateFormat(endDate,application.eonDateFormat)>
	
	<cfquery datasource="#application.eonDsn#" name="getSos">
		SELECT dateEntered, dateModified, businessDate, sosSeconds, carCountTimer, carCountMicros 
		FROM storeNightlyData
		WHERE businessDate BETWEEN '#startDate#' AND '#endDate#'
		AND businessUnitId = '#variables.storeId#'	
		ORDER BY businessDate DESC
	</cfquery>
	
		<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" /> 		
		<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script> 
		<script type="text/javascript" src="../js/jquery-ui-1.8.custom.min.js"></script> 
			
		<script type="text/javascript">
			$(function() {
					$("#businessDate").datepicker();
			});
		
			
			function validateAndSend()
			{
				var sosNum = document.getElementById('sosNum');
				var carTimerNum = document.getElementById('carTimerNum');
				var carMicrosNum = document.getElementById('carMicrosNum');
				var safeTotalNum = document.getElementById('safeTotalNum');
				var gcTotalNum = document.getElementById('gcTotalNum');
				
				var businessDate = document.getElementById('businessDate');
		
				if (sosNum.value == 0 || carTimerNum.value == 0 || carMicrosNum.value == 0 || safeTotalNum.value == 0 || gcTotalNum.value == 0 || !checkdate(businessDate) || sosNum.value == '' || carTimerNum.value == '' || carMicrosNum.value == '' || safeTotalNum.value == '' || gcTotalNum.value == ''  )
				{
					alert('Please make sure that all fields are filled-in properly, including Date, SOS and Number of cars on Timer and Micros and Safe and GC Totals.');
				}
				else if (sosNum.value >= 300) {
					var msg = "Are you sure you want to submit a SOS time of: " + sosNum.value + " seconds?";
					if (confirm(msg)) {
						document.forms[0].submit();	
					}
				}
				else {
					document.forms[0].submit();	
				}
			}
			
			function checkNum(input, msg)
			{
				if (input.value != '') {
					IsNumeric(input,'Please enter a whole number for ' + msg);
					Is_int(input);
				}
			}
			
			function checkNum2(input, msg)
			{
				if (input.value != '') {
					IsNumeric(input,'Please enter a number for ' + msg);
				}
			}
		</script>
	<div style="margin-left:100px;">

	
	<cfif IsDefined("form.sosNum") AND form.sosNum GT 0>
		<cfset application.daoObj.InsertNighlyData(variables.storeId,form.businessDate,Int(form.sosNum),Int(form.carTimerNum),Int(form.carMicrosNum),form.safeTotalNum,form.gcTotalNum,variables.userId)>
		<h3>Done! Thanks for your submission.<br><a href="<cfoutput>#application.baseURL#</cfoutput>">Click here</a> to return to main</h3>
	<cfelse>
		<h2>Nightly Data Submission</h2>		
		<form name="doTransferForm" action="doNightlyData.cfm" method="post">
			
			<table width="800" style="border:green double 2px;margin:20px">
				<tr>
					<td>
					<b>Business Date</b>
					</td>
					<td>
					<b>SOS in seconds</b>
					</td>
					<td>
					<b># Cars - Timer</b>
					</td>
					<td>
					<b># Cars - Micros</b>
					</td>
					<td>
					<b>Safe Total $</b>
					</td>
					<td>
					<b>GC Total $</b>
					</td>
				</tr>
				
				<tr>
					<td>
						<input name="businessDate" id="businessDate" maxlength="12" size="10" type="text" value="<cfoutput>#variables.today#</cfoutput>"  autocomplete="off" />
					</td>
					<td>
						<input type="text" name="sosNum" id="sosNum" value="" size="5" onblur="checkNum(this, 'SOS')" class="inputNoErr" autocomplete="off">
					</td>
					<td>
						<input type="text" name="carTimerNum" id="carTimerNum" value="" size="5" onblur="checkNum(this, 'Cars Timer')" class="inputNoErr" autocomplete="off">
					</td>
					<td>
						<input type="text" name="carMicrosNum" id="carMicrosNum" value="" size="5" onblur="checkNum(this, 'Cars - Micros')" class="inputNoErr" autocomplete="off">
					</td>
					<td>
						<input type="text" name="safeTotalNum" id="safeTotalNum" value="" size="5" onblur="checkNum2(this, 'Safe Total')" class="inputNoErr" autocomplete="off">
					</td>
					<td>
						<input type="text" name="gcTotalNum" id="gcTotalNum" value="" size="5" onblur="checkNum2(this, 'Gift Certificates Total')" class="inputNoErr" autocomplete="off">
					</td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>	
				<tr>
					<td colspan="5" align="right">
						<input type="button" id="btnSubmit" value="Submit" onclick="validateAndSend()">
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			
			
		</form>
		<p></p>
			<h3>SOS submitted so far this week:</h3>
				<table width="800" style="border:green double 2px;margin:20px">
					<tr>
						<td>
						<b>Business Date</b>
						</td>
						<td>
						<b>Date Entered</b>
						</td>
						<td>
						<b>Date Modified</b>
						</td>
						<td>
						<b>SOS</b>
						</td>
						<td>
						<b># Cars - Timer</b>
						</td>
						<td>
						<b># Cars - Micros</b>
						</td>
					</tr>
					<cfif getsos.RecordCount>
					<cfloop query="getSos">
					<tr>
						<td>
						<cfoutput>#dateFormat(BusinessDate, application.eonDateFormat)#</cfoutput>
						</td>
						<td>
						<cfoutput>#dateFormat(dateEntered, application.eonDateFormat)# #timeFormat(dateEntered, "short")#</cfoutput>
						</td>
						<td>
						<cfoutput>#dateFormat(dateModified, application.eonDateFormat)# #timeFormat(dateModified, "short")#</cfoutput>
						</td>
						<td>
						<cfoutput>#sosSeconds#</cfoutput>
						</td>
						<td>
						<cfoutput>#carCountTimer#</cfoutput>
						</td>
						<td>
						<cfoutput>#carCountMicros#</cfoutput>
						</td>
					</tr>
					</cfloop>
					<cfelse>
						<tr><td colspan="6">No SOS submitted this week</td></tr>
					</cfif>
					
					
				</table>	
			</cfif>

	</div>
	
	
	<cfinclude template="../footer.cfm" />