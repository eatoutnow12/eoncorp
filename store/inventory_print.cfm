<html>
	<head>
		<title>Inventory Workbook</title>
		 <link rel="stylesheet" type="text/css" href="../css/print.css">
	</head>
	<body>
	<cfset maxRecsPerPage = 37>
<cfoutput>
	<cfset variables.dateWeekEnding = application.utilObj.getWeekEndingDate()>
	
	
	<cfif NOT isDefined("cookie.storeId") OR Len(Trim(cookie.storeId)) EQ 0>
		<cfset variables.error = "This store/computer is currently not setup for use at this location.<br>Please contact the EatOutNow IT department by calling 402-408-6351.">
		<cflocation url="../login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	<cfelseif NOT isDefined("session.userId")>
		<cfset variables.error = "Your session has timed out. Please log-in again.">
		<cflocation url="../login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	<cfelse>
		<cfset variables.storeId = cookie.storeId>
		<cfset variables.userId = session.userId>
	</cfif>
	
	<cfquery name="getStoreCats" datasource="#application.eonDsn#">
		SELECT [itemCategoryId]
		      ,[itemCategoryLabel]
		FROM [storeItemCategories]
		WHERE itemCategoryId > 0
		ORDER BY itemCategoryId
	</cfquery>
	<cfdocument format="PDF" localurl="true" pagetype="letter" filename="InventoryWorkbook.pdf" overwrite="true">
		<cfdocumentitem type="header">
		    <table width="100%" border="0" cellpadding="0" cellspacing="0">
		        <tr>
		        	<td align="center" style="font-size:10px;">
		        	<b>Inventory Workbook - #cookie.storeName#</b>
		        	</td>
				</tr>
		    </table>
		</cfdocumentitem>
		<div style="margin-left:30px;">
				<cfset pageCount = 0>
			<cfloop query="getStoreCats">
				<cfset pageCount++>
				<cfquery name="getCatItems" datasource="#application.eonDsn#">
					SELECT
						   [itemNumber]      
					      ,[itemDescription]
					      ,[storeCategoryId]
						  ,unit = (SELECT itemUnitLabel FROM storeItemUnits WHERE [itemUnitId] = purchaseItems.[itemUnitId])
						  ,inProcess = 0
						  FROM dbo.purchaseItems
					WHERE 1 = 1
					AND itemStatus = 1
					AND storeCategoryId = #getStoreCats.itemCategoryId#
					UNION
					SELECT
						   [itemNumber]      
					      ,[itemDescription]
					      ,[storeCategoryId]
						  ,unit = (SELECT itemUnitLabel FROM storeItemUnits WHERE [itemUnitId] = storeInProcessItems.[itemUnitId])
						  ,id AS inProcess
						  FROM dbo.storeInProcessItems
					WHERE 1 = 1
					AND itemStatus = 1
					AND storeCategoryId = #getStoreCats.itemCategoryId#
					ORDER BY itemDescription
				</cfquery>
				
					<table width="550">
						<tr>
							<td colspan="4" align="center">
							<cfif getCatItems.RecordCount LTE maxRecsPerPage>
								<cfset emptyRows = maxRecsPerPage - getCatItems.RecordCount>
								<cfset pages = 1>
							<cfelse>
								<cfset pages = Ceiling(getCatItems.RecordCount/maxRecsPerPage)>
								<cfset emptyRows = ((maxRecsPerPage*pages) - getCatItems.RecordCount)>
							</cfif>
							<div style="font-size:14pt;color:green"><strong>#getStoreCats.itemCategoryLabel#</strong></div>
							</td>
						</tr>
						<tr>
							
							<td class="inventoryTdHeader" nowrap="nowrap">
								Item
							</td>
							
							<td width="30" class="inventoryTdHeader">
								Unit
							</td>
							
							
							<td width="20" class="inventoryTdHeader">
								Count
							</td>
							
							<td width="100" class="inventoryTdHeader">
								Notes
							</td>
		
						</tr>
					<cfloop query="getCatItems">
						<cfif getCatItems.CurrentRow EQ maxRecsPerPage>
								<tr>
									<td colspan="4"><div style="font-size:14pt;color:green"><strong>#getStoreCats.itemCategoryLabel# - Contd.</strong></div></td>
								</tr>
								<cfset pageCount++>
						</cfif>
						<tr>
							
							<td class="inventoryTd" nowrap="nowrap">
								<u>#itemDescription#</u>			
							</td>
							
							<td class="inventoryTd">
								<u>#unit#</u>			
							</td>
							
							<td class="inventoryTd">
								____
							</td>
											
							<td class="inventoryTd">
								______
							</td>
							
						</tr>			
					</cfloop>			
					
						<cfif emptyRows GT 0>
							<cfloop index="i" from="1" to="#emptyRows#">
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
							</cfloop>
						</cfif>
					
					</table>
					<br/>
			</cfloop>
		</div>
		<cfdocumentitem type="footer">
		    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				<cfoutput>
				<tr>
					<td align="left" style="font-size:8px;">Created: #DateFormat(Now(),"full")# #TimeFormat(Now(),"short")#</td>
					<td align="right" style="font-size:10px;">Page #cfdocument.currentpagenumber# of #pageCount#</td>
				</tr>
				</cfoutput>
		    </table>
		</cfdocumentitem>
	
	</cfdocument>		
</cfoutput>

	<!--- Get the temp file for streaming. --->
	<cfset strFilePath = GetTempFile(
		GetTempDirectory(),
		"pdf_"
		) />
		
	<cfpdf source="#ExpandPath('./')#InventoryWorkbook.pdf" 
	action="deletepages" overwrite="yes" pages="8"destination="#strFilePath#">
	
	<cfheader
	name="Content-Disposition"
	value="attachment; filename=InventoryWorkbook.pdf" />

	<cfcontent
		type="application/pdf"
		file="#strFilePath#"
		deletefile="true"
		/>


<cfinclude template="../footer.cfm" />
