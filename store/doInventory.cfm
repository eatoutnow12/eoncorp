﻿<cfinclude template="../header.cfm">
<cfmodule template="../checkaccess.cfm" reportId="4">
<script type="text/javascript">
	function validateAndSubmit(){
		var submitBtn = document.getElementById('submitBtn');
		submitBtn.value = 'Submitting, Wait...';
		submitBtn.disabled = true;
		document.forms[0].submit();
	}
</script>


<cfoutput>
	<cfset variables.dateWeekEnding = application.utilObj.getWeekEndingDate()>	
	
	<cfif NOT isDate(variables.dateWeekEnding)>
		<cfset variables.error = "The Inventory Submission app will not be available until Sunday.">
		<cflocation url="../index.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	</cfif>
	
	<cfif NOT isDefined("cookie.storeId") OR Len(Trim(cookie.storeId)) EQ 0>
		<cfset variables.error = "This store/computer is currently not setup for use at this location.<br>Please contact the EatOutNow IT department by calling 402-408-6351.">
		<cflocation url="../login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	<cfelseif NOT isDefined("session.userId")>
		<cfset variables.error = "Your session has timed out. Please log-in again.">
		<cflocation url="../login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">	
	<cfelse>
		<cfset variables.storeId = cookie.storeId>
		<cfset variables.userId = session.userId>
	</cfif>
	
	<cfquery name="getStoreCats" datasource="#application.eonDsn#">
		SELECT [itemCategoryId]
		      ,[itemCategoryLabel]
		FROM [storeItemCategories]
		WHERE itemCategoryId > 0
		ORDER BY itemCategoryId
	</cfquery>
	
	<cfset variables.itemIndex = 0>
<div style="margin-left:100px;width:700px;float:left;">	
	<cfif NOT IsDefined("form.itemCnt")>
		<cflog type="information" file="eonCorp" text="Inventory Submission app opened at IP: #cgi.REMOTE_ADDR# for store: #cookie.storeName#">
		
<h2>Weekly Inventory Submission</h2>			
		<form name="doInv" method="post" action="doInventory.cfm">
	<cfloop query="getStoreCats">
		<cfquery name="getCatItems" datasource="#application.eonDsn#">
			SELECT
				   [itemNumber]      
			      ,[itemDescription]
			      ,[storeCategoryId]
				  ,unit = (SELECT itemUnitLabel FROM storeItemUnits WHERE [itemUnitId] = purchaseItems.[itemUnitId])
				  ,inProcess = 0
				  ,unitPrice = (SELECT dbo.GetUnitPriceByItemNumber(dbo.purchaseItems.itemNumber,0))
				  FROM dbo.purchaseItems
			WHERE 1 = 1
			AND itemStatus = 1
			AND storeCategoryId = #getStoreCats.itemCategoryId#
			UNION
			SELECT
				   [itemNumber]      
			      ,[itemDescription]
			      ,[storeCategoryId]
				  ,unit = (SELECT itemUnitLabel FROM storeItemUnits WHERE [itemUnitId] = storeInProcessItems.[itemUnitId])
				  ,id AS inProcess
				  ,unitPrice = (SELECT dbo.GetUnitPriceByItemNumber(dbo.storeInProcessItems.itemNumber,storeInProcessItems.id))
				  FROM dbo.storeInProcessItems
			WHERE 1 = 1
			AND itemStatus = 1
			AND storeCategoryId = #getStoreCats.itemCategoryId#
			ORDER BY itemDescription
		</cfquery>	
		
			
			<table width="800" style="border:green double 2px;">
				<tr>
					<td colspan="6" align="center">
					<div style="font-size:14pt;color:green"><strong>#getStoreCats.itemCategoryLabel#</strong></div>
					</td>
				</tr>
				<tr>
					<td width="100" class="inventoryTdHeader">
						Number
					</td>
					
					<td width="300" class="inventoryTdHeader">
						Item
					</td>
					
					<td width="30" class="inventoryTdHeader">
						Unit
					</td>
					
					<td width="10" class="inventoryTdHeader">
						Unit Price
					</td>
					
					<td width="5" class="inventoryTdHeader">
						Count
					</td>
				</tr>
			<cfloop query="getCatItems">
				<cfset variables.itemIndex = variables.itemIndex+1>
				<tr>
					<td class="inventoryTd">
						
						#itemNumber#
						<input type="hidden" name="item#variables.itemIndex#" value="#itemNumber#">
						<input type="hidden" name="inProcess#variables.itemIndex#" value="#inProcess#">					
					
					</td>
					
					<td class="inventoryTd">
						#itemDescription#			
					</td>
					
					<td class="inventoryTd">
						#unit#			
					</td>
					
					<td class="inventoryTd">
						#dollarFormat(unitPrice)#			
					</td>
									
					<td class="inventoryTd">
						<input type="text" name="qty#variables.itemIndex#" id="qty#variables.itemIndex#" value="" 
						size="6" maxlength="6" onblur="IsNumeric(this)" class="inputNoErr" autocomplete="off">
					</td>
					
				</tr>			
			</cfloop>			

			</table>
			<br>
	</cfloop>
			<div style="width:800px;text-align:center;padding:10px">
				<input type="hidden" name="itemCnt" value="#variables.itemIndex#">
				<input type="button" id="submitBtn" value="Submit Inventory" onclick="validateAndSubmit()">
			</div>
			<br><br>
		</form>
		
	<cfelse>
	
		<cfset variables.dateCreated = dateformat(now(), application.eonDateFormat) & " " & timeformat(now(), "HH:MM:ss")>
		
		
		<cftry>
			<cftransaction>
				<cfloop from="1" to="#form.itemCnt#" index="i">
					<cfset keyItem = "item" & i>
					<cfset qtyItem = "qty" & i>
					<cfset inProcess = "inProcess" & i>
					
					<cfif isNumeric(evaluate("form.#qtyItem#")) AND evaluate("form.#qtyItem#") GT 0>
						<cfset application.daoObj.InsertInventoryItem('#variables.userId#'
						,'#variables.storeId#','#variables.dateCreated#','#variables.dateWeekEnding#','#evaluate("form.#keyItem#")#'
						,'#evaluate("form.#qtyItem#")#',evaluate("form.#inProcess#"))>
					</cfif>
				</cfloop>
				
				<cfset application.daoObj.InsertInventorySummary(application.daoObj.GetInventorySnapshot('#variables.userId#','#variables.storeId#','#variables.dateWeekEnding#'),'#variables.storeId#','#variables.dateWeekEnding#')>
						
			</cftransaction>
			<h3>Done! Thanks for your submission.<br>	<br>
			Now, you may: </h3>
			<ul>
				<li><h3><a href="<cfoutput>#application.baseURL#foodCostViewer.cfm</cfoutput>">Click here</a> to View Food Cost that you just submitted OR</h3></li>
				<li><h3><a href="<cfoutput>#application.baseURL#inventoryCounts.cfm</cfoutput>">Click here</a> to View the Inventory Counts that you just submitted OR</h3></li>
				<li><h3><a href="<cfoutput>#application.baseURL#store/doInventory.cfm</cfoutput>">Click here</a> to Re-enter Inventory Counts</h3></li>
			</ul>
			<cflog type="information" file="eonCorp" text="Inventory Submission Succeeded at IP: #cgi.REMOTE_ADDR# at store cookie: #cookie.storeName#, For store Id: #variables.storeId#">
			
			<cfset mailAttributes = {
			message="Inventory Submission Succeeded at store: #cookie.storeName# at #TimeFormat(Now(),"long")# on #DateFormat(Now(),application.eonDateFormat)#.<br/>Submitted from computer using IP: #cgi.REMOTE_ADDR#",
			sendTo="sales@eatoutnow.net",
			sendCc="#cookie.storeName#popeyes@eatoutnow.net",
			sentFrom="Inventory App",
			subject="Inventory for store: #cookie.storeName#",
			mailFormat="html"
		}>	
		
			<cfmodule template="..\sendMail.cfm" attributecollection="#mailAttributes#">			
				
			<cfcatch>
				<h3>An error has occured. Please stop entering any additional data and contact Kenny at 402-319-8740.</h3>
				<cfmodule template="..\errorReport.cfm" report="#cfcatch#" parsedData="#form#">
				<cflog type="error" file="eonCorp" text="Inventory Submission Failed at IP: #cgi.REMOTE_ADDR# for store: #cookie.storeName#. #cfcatch#">
			</cfcatch>

		</cftry>
		
	</cfif>

</div>
<div style="float:left;width:90px;margin-top:25px">
	<a href="inventory_print.cfm">
		<img src="..\img\icons\download.png" width="16" height="16" border="0" /> Print
	</a>
</div>

<div style="clear:both;"> </div>
</cfoutput>

<cfinclude template="../footer.cfm" />