<cfparam name="url.err" default="">
<cfset variables.error = Trim(url.err)>
	<cfoutput>
<cfif NOT structKeyExists(session, "userId") OR NOT Len(session.userId)>
	<cflocation url="#application.baseurl#login.cfm?err=#URLEncodedFormat('Please sign-in below to continue')#" addtoken="false">
</cfif>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>EON Corp Portal</title>		
		<script type="text/javascript" src="#application.baseurl#js/eon.js"></script>
		<link rel="stylesheet" type="text/css" href="#application.baseurl#css/eon.css">
	</head>
	<body>
		<div class="topBar">
			<div style="float:left;height:1px;width:35%"><a href="#application.baseurl#index.cfm"><img src="#application.baseurl#img/icons/home.png" width="15" height="15" border="0" style="padding:3px 0 0 20px" alt="Portal Home" title="Portal Home" /></a></div>
	 		<div style="float:left; width:30%;color:##DD4B39;font-weight:bold;background-color:##F5F5F5;text-align:center">
				#variables.error#
			</div>
			<div style="float:right; padding-right:20px">
				Logged in as: <strong>#session.firstName# #session.lastName#</strong><cfif IsDefined("cookie.storeName")> at <strong>Store##: </strong> #cookie.storeName#</cfif>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#application.baseurl#logout.cfm">Log-off</a>
			</div>
			<div style="clear:both"></div>
		</div>
</cfoutput>