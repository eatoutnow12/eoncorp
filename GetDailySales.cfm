﻿<cfloop query="application.getAllUnits">
	<cftry>
		<cfmodule template="GetDsrFromEM.cfm" microsStoreId="#application.getAllUnits.microsStoreId#">	
		<cftransaction>				
				<!--- Get Account Codes --->
					<cfset salesDataStruct.netSalesCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '4100-%')>
					<cfif salesDataStruct.businessUnitId EQ "85dbf6e1-7550-4116-8dbd-6b0af85d1037">
						<cfset salesDataStruct.salesTaxCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '2361-0000')>	
					<cfelse>
						<cfset salesDataStruct.salesTaxCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '2360-0000')>
					</cfif>
					<cfset salesDataStruct.paidOutsCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '5715-%')>			
					<cfset salesDataStruct.gcRedeemedCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '1101-%')>
					<cfset salesDataStruct.ArCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '1200-0000')>
					<cfset salesDataStruct.VisaCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '1101-%')>
					<cfset salesDataStruct.AmexCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '1101-%')>
					<cfset salesDataStruct.AmDepositCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '1101-%')>
					<cfset salesDataStruct.PmDepositCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '1101-%')>
					<cfset salesDataStruct.couponCreditCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '4100-%')>	
					<cfset salesDataStruct.promosCreditCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '4100-%')>
					<cfset salesDataStruct.couponDebitCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '4110-%')>	
					<cfset salesDataStruct.promosDebitCodeId = application.daoObj.getAccountCode(salesDataStruct.businessUnitId, '4110-%')>

					<!---Insert Data								
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.netSalesCodeId, '0', salesDataStruct.netSalesDollar, 'Net Food Sales')>
													
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.salesTaxCodeId, '0', salesDataStruct.salesTaxDollar, 'Sales Tax Payable')>
													
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.gcRedeemedCodeId, salesDataStruct.gcRedeemedDollar, '0', 'G/C Redeemed')>
													
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.ArCodeId, salesDataStruct.ArDollar, '0', 'A/R')>
													
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.VisaCodeId, salesDataStruct.VisaMcDollar, '0', 'Visa/MC/Discover')>
													
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.AmexCodeId, salesDataStruct.AmexDollar, '0', 'AMEX')>
													
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.AmDepositCodeId, salesDataStruct.AMdepositsDollar, '0', 'AM Deposit')>
													
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.PmDepositCodeId, salesDataStruct.PMdepositsDollar, '0', 'PM Deposit')>								
					
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.couponDebitCodeId, salesDataStruct.couponDollar, '0', 'Coupon $')>
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.couponCreditCodeId, '0', salesDataStruct.couponDollar, 'Coupon $')>
													
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.paidOutsCodeId, salesDataStruct.paidOutsDollar, '0', 'Paid outs')>
					
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.promosDebitCodeId, salesDataStruct.promosDollar, '0', 'Promos $')>
					<cfset application.daoObj.InsertSalesData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.promosCreditCodeId, '0', salesDataStruct.promosDollar, 'Promos $')>	

					<!---Insert General Data--->
					<cfset application.daoObj.InsertDailyGeneralData(salesDataStruct.businessUnitId, salesDataStruct.entryDate, salesDataStruct.CouponsNum, salesDataStruct.PromosNum, 
					salesDataStruct.DeletesNum, salesDataStruct.CustomerCount, salesDataStruct.GcSold, salesDataStruct.laborNum, salesDataStruct.GcPaidOut)>
				--->
		</cftransaction>
		<cfcatch type="any">
			<cfdump var="#cfcatch#">
		</cfcatch>
	</cftry>
	
	
	
</cfloop>



<!---
<cfdump var="#salesDataStruct#">--->