<cfinclude template="header.cfm">
			
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" /> 
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script> 
		<script type="text/javascript" src="js/jquery-ui-1.8.custom.min.js"></script> 
		
		<script type="text/javascript"> 
		$(function() {
			$("#startDate").datepicker();
		});
		
		$(function() {
			$("#endDate").datepicker();
		});	
	

		</script>

		<script type="text/javascript">
			function ValidateSubmit()
			{
				document.forms[0].submit();
			}	
		</script>
<div style="margin:10px">

<h3>KPI Report</h3><br>

<form action="kpiReport.cfm" method="post">
	<cfif NOT IsDefined("cookie.storeId")>
				<label for="bUnit">Select Store:</label>	
				<select id="bUnit" name="bUnit">
					<cfloop query="application.getAllUnits">
						<cfoutput>
			            	<option id="#Trim(unitName)#" value="#Trim(id)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ id>selected</cfif>>
							#Trim(unitName)#
							</option>
			            </cfoutput>
					</cfloop>
				</select>	&nbsp;&nbsp;&nbsp;&nbsp;
		

				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<cfelse>
				<cfoutput><input type="hidden" name="bUnit" value="#cookie.storeId#"></cfoutput>
	</cfif>
 
					<label for="startDate">From Date:</label>			
					<cfoutput><input name="startDate" id="startDate" maxlength="12" size="10" type="text"<cfif IsDefined("form.startDate")> value="#form.startDate#"<cfelse>value=""</cfif> /></cfoutput>
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					<label for="endDate">To Date:</label>			
					<cfoutput><input name="endDate" id="endDate" maxlength="12" size="10" type="text"<cfif IsDefined("form.endDate")> value="#form.endDate#"<cfelse>value=""</cfif> /></cfoutput>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type="button"  onclick="ValidateSubmit()" value="Show KPI"></p>
				

</form>		

<cfif IsDefined("form.bUnit")>
				<cfset ctr = 1>
				<cfset storeId = Trim(form.bUnit)>
				<cfset startDate = form.startDate>
				<cfset endDate = form.endDate>
				
				<cfif NOT IsDate(startDate)>
					<b>Error: Please choose a From Date to view</b>
					<cfabort>
				</cfif>
				
				<cfif NOT IsDefined("form.endDate") OR NOT IsDate(form.endDate)>
					<cfset endDate = form.startDate>
				<cfelse>
					<cfset endDate = form.endDate>
				</cfif>				
				
				<cfif DateDiff("d", Dateformat(startDate, application.eonDateFormat), DateFormat(endDate,application.eonDateFormat)) LT 0>			
					<b>Error: To Date should be after From Date</b>
					<cfabort>			
				</cfif>
				
				<cfif DateDiff("d", Dateformat(Now(), application.eonDateFormat), DateFormat(startDate,application.eonDateFormat)) GTE 0 OR DateDiff("d", Dateformat(Now(), application.eonDateFormat), DateFormat(endDate,application.eonDateFormat)) GTE 0>			
					<b>Error: Dates cannot be in the future or even today's date. Today's data will be available tomorrow.</b>
					<cfabort>			
				</cfif>
				
				<!---For SOS--->
				<cfset sosDayNum = DateDiff("d", Dateformat(startDate, application.eonDateFormat), DateFormat(endDate,application.eonDateFormat))>
				
				<cfset sosDayNum = sosDayNum+1>
				
				<!---<cfif sosDayNum EQ 0>
					<cfset sosDayNum = 7>
				<cfelseif sosDayNum EQ -1>
					<cfset sosDayNum = 6> 
				</cfif><cfdump var="#sosDayNum#">--->
				
				<cfset today = Now()>
				<cfset CurrentYear = Year(today)>
				<cfset PreviousYear = CurrentYear - 1>	
				
				<cfquery name="getCalendarCurrent" datasource="#application.eonDsn#">
					SELECT StartDate from businessCalendar
					where year = '#CurrentYear#' 
					AND businessEntityId = (SELECT entityId FROM businessUnit WHERE id= '#storeId#')
				</cfquery>
				
				<cfset currentDateAsString = DateFormat(getCalendarCurrent.StartDate, application.eonDateFormat)>
				
				<cfset periodForStartDate = application.utilObj.getPeriod(currentDateAsString, startDate)>
				<cfset weekForStartDate = application.utilObj.getWeek(currentDateAsString, startDate)>
				<cfset dayForStartDate = application.utilObj.getDay(startDate)>
				
				
				<cfset periodForEndDate = application.utilObj.getPeriod(currentDateAsString, endDate)>
				<cfset weekForEndDate = application.utilObj.getWeek(currentDateAsString, endDate)>
				<cfset dayForEndDate = application.utilObj.getDay(endDate)>
				
				<cfquery name="getCalendarPrior" datasource="#application.eonDsn#">
					SELECT StartDate from businessCalendar
					where year = '#PreviousYear#' 
					AND businessEntityId = (SELECT entityId FROM businessUnit WHERE id= '#storeId#')
				</cfquery>
				<cfset priorStartDateAsString = DateFormat(getCalendarPrior.StartDate, application.eonDateFormat)>
				
				
			<cfif getCalendarPrior.RecordCount>				
				<cfset priorStartDate = application.utilObj.getDateByPeriodWeekDay(priorStartDateAsString, periodForStartDate, weekForStartDate, dayForStartDate)>
				<cfset priorStartDate = DateFormat(priorStartDate, application.eonDateFormat)>
				<cfset priorEndDate = application.utilObj.getDateByPeriodWeekDay(priorStartDateAsString, periodForEndDate, weekForEndDate, dayForEndDate)>
				<cfset priorEndDate = DateFormat(priorEndDate, application.eonDateFormat)>
			</cfif>
				
				<style>
					
					
					.headerRow {
				 		color: #FFFFFF;
						background-color:#000;
						padding-left:10px;
				 	}
								
					td {
						width:inherit;
						padding-left:10px;
					}
		
					
				</style>
		<cfoutput>
				<table>
				
				<tr class="headerRow">
					<td>
					Net Sale
					</td>
					<td>
					Guests
					</td>					
					<td>
					Paid Outs
					</td>
					<td>
					Coupons $
					</td>
					<td>
					Promos $
					</td>
					<td>
					Cash +/-
					</td>
					<td>
					Deletes
					</td>
					<td>
					Labor %
					</td>
					<td>
					YOY % Diff
					</td>					
					<td>
					Avg SOS
					</td>
					
				</tr>
			
					<cfquery name="getStoreSalesData" datasource="#application.eonDsn#">
						SELECT TOP 1	
						NetSales = (SELECT SUM([credit]) FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#storeId#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#')
						,PaidOuts = (SELECT SUM([debit]) FROM [dailySales] 
						WHERE [note] = 'Paid outs' AND [businessUnitId] = '#storeId#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#')
						,Coupons = (SELECT SUM([debit]) FROM [dailySales] 
						WHERE [note] = 'Coupon $' AND [businessUnitId] = '#storeId#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#')
						,Promos = (SELECT SUM([debit]) FROM [dailySales] 
						WHERE [note] = 'Promos $' AND [businessUnitId] = '#storeId#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#')
						,CashOverShort = (SELECT dbo.GetCashOverShortByStoreAndDateRange('#storeId#','#startDate#','#endDate#'))
						,Deletes = (SELECT SUM(deletesNum) FROM dailyGeneralStoreData WHERE [businessUnitId] = '#storeId#'
						AND [dateOccured]  BETWEEN '#startDate#' and '#endDate#')
						,CustCount = (SELECT SUM(customerCount) FROM dailyGeneralStoreData WHERE [businessUnitId] = '#storeId#'
						AND [dateOccured]  BETWEEN '#startDate#' and '#endDate#')
						,LabourDollar = (SELECT SUM(laborDollar) FROM dailyGeneralStoreData WHERE [businessUnitId] = '#storeId#'
						AND [dateOccured]  BETWEEN '#startDate#' and '#endDate#')
						,sos = (SELECT [dbo].[GetWTDSosByStoreAndDateRange] ('#storeId#','#startDate#','#endDate#',#sosDayNum#))
						,PriorNetSales = (SELECT SUM([credit]) FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#storeId#'
						AND [dateOfSale] BETWEEN '#priorStartDate#' AND '#priorEndDate#')
						
						FROM [dailySales]
						WHERE [businessUnitId] = '#storeId#'
						AND [dateOfSale] BETWEEN '#startDate#' AND '#endDate#'
						GROUP BY [dailySales].[dateOfSale], [businessUnitId]
					</cfquery>
					<cfset labourPercent = DecimalFormat((getStoreSalesData.LabourDollar/getStoreSalesData.NetSales) * 100)>
					<cfset criticalCashOverShort = (getStoreSalesData.NetSales * 0.001)>
					<cfset absCashOverShort = abs(getStoreSalesData.CashOverShort)>
					<cfset yoyDiff = DecimalFormat(Evaluate((getStoreSalesData.NetSales - getStoreSalesData.PriorNetSales)/getStoreSalesData.PriorNetSales) * 100)>
			
				<tr bgcolor="###iif(ctr MOD 2,DE('efefef'),DE('ffffff'))#">
					<td>
					#DollarFormat(getStoreSalesData.NetSales)#
					</td>
					<td>
					#getStoreSalesData.CustCount#
					</td>					
					<td>
					#DollarFormat(getStoreSalesData.PaidOuts)#
					</td>
					<td>
					#DollarFormat(getStoreSalesData.Coupons)#
					</td>
					<td>
					#DollarFormat(getStoreSalesData.Promos)#
					</td>
					<td <cfif absCashOverShort GT criticalCashOverShort> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>			
					#DollarFormat(getStoreSalesData.CashOverShort)#
					</td>
					<td>
					#DollarFormat(getStoreSalesData.Deletes)#
					</td>
					<td <cfif labourPercent GT 16> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
					#labourPercent#
					</td>

					<td <cfif Sgn(yoyDiff) EQ -1> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>			
					#yoyDiff#
					</td>
					
					<td <cfif getStoreSalesData.sos GTE 181 AND getStoreSalesData.sos LTE 210> bgcolor="##F88017" style="color:##FFFFFF"<cfelseif getStoreSalesData.sos GT 210> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
					#getStoreSalesData.sos#	
					</td>
				</tr>
				<cfset ctr++>
				
				</table>
		</cfoutput>
			
</cfif>

</div>

<cfinclude template="footer.cfm">