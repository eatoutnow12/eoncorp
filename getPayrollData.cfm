﻿
<cfset folderPrefix = DateFormat(Now(),"yyyymmdd")>
<cfset save_path = ExpandPath('./') & "\Payroll\" & folderPrefix & "\">
<cfset storesImported = ArrayNew(1)>
<cfset payrollDelim = "$PayrollId$">
<cfset columnDelim = chr(32) & chr(32)>
			
<cfif directoryExists(#save_path#)>
	<cfdirectory action="delete" directory="#save_path#" recurse="true">
<cfelse>
	<cfdirectory action="create" directory="#save_path#">
</cfif>

<!---
<cftry>--->
	<cfset javaSystem = createObject("java", "java.lang.System") />
	<cfset jProps = javaSystem.getProperties() />
	<cfset jProps.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory") />
	<cfset jProps.setproperty("mail.pop3.port",995) />
	<cfset jProps.setProperty("mail.pop3.socketFactory.port", 995) />
	<cfset dsrFileCount = 0>


	<cfpop 
	action="GETALL" 
	name="payrollQry" 
	server="pop.gmail.com" 
	username="payroll@eatoutnow.net" 
	password="eonSystem" 
	port="995" 
	attachmentpath="#save_path#"
	generateuniquefilenames="true"
	timeout="360">
	
	<cfif FileExists("#save_path#CUSTOMPAYROLL.TXT")>	
		<cffile action="rename" source="#save_path#CUSTOMPAYROLL.TXT" destination="#save_path#CUSTOMPAYROLL0.TXT">
	</cfif>
	
	<cfdirectory action="list" directory="#save_path#" filter="CUSTOMPAYROLL*.txt" name="PayrollFiles">
	<cfset payrollFileCount = PayrollFiles.RecordCount-1>

<!---<cfcatch type="any">
		<cfmodule template="errorReport.cfm" report="#cfcatch#">
</cfcatch>
</cftry>--->



	<cfloop index="fileIdx" from="0" to="#payrollFileCount#">
		
		<!---<cftry>	--->		
			
			<cffile action = "read" file="#save_path#CUSTOMPAYROLL#fileIdx#.TXT" variable = "payrollFile">

			<cfscript>
				parsedData = ListToArray(payrollFile,payrollDelim, false, true);
			</cfscript>
			
			<cfset lineDelim = Chr(13) & Chr(10)>
			
			<cfset storeAndDateSet = parsedData[1]>
			
			<cfset storeId = Trim(ListGetAt(storeAndDateSet,2,"^",true))>
			
			<cfset storeId = Trim(listLast(storeId,"-"))><!---<cfdump var="#storeid#" abort="true">--->
			<cfset ppeDate = DateFormat(Trim(ListGetAt(storeAndDateSet,3,"^",true)),application.eonDateFormat)>
			
				<cfloop from="2" to="#arraylen(parsedData)#" index="emp">
					<cfset totalHours = 0>
					<cfset empData = parsedData[emp]>
					
					<cfscript>
						empData = ListToArray(empData,lineDelim, false);
					</cfscript>
						<cfset payrollIdAndName = empData[1]>
						<cfset payrollId = Trim(listGetAt(payrollIdAndName,2,"$",true))>
						<cfset empName = Trim(listGetAt(payrollIdAndName,3,"$",true))>	
						<cfloop from="2" to="#arraylen(empData)#" index="dayData">
							
							<cfif dayData neq arrayLen(empData)>
							
								<cfset payrollDailyData = StructNew()>
								<cfset dayData = empData[dayData]>
								<cfset dayData = ListToArray(dayData, " ", false)>
								
								<cfset payrollDailyData.storeId = storeId>
								<cfset payrollDailyData.ppeDate = ppeDate>
								<cfset payrollDailyData.payrollId = payrollId>
								<cfset payrollDailyData.empName = empName>		
											
								<cfset payrollDailyData.jobId = trim(dayData[1])>
								<cfset payrollDailyData.jobDate = trim(dayData[2])>
								<cfset payrollDailyData.inTime = trim(dayData[3])>
								<cfset payrollDailyData.outTime = trim(dayData[4])>
								<cfset payrollDailyData.hoursWorked = trim(dayData[5])>
								<cfset totalHours = totalHours + payrollDailyData.hoursWorked>
								
								<cfset application.daoObj.InsertPayRollDaily(payrollDailyData.storeId,
								payrollDailyData.ppeDate,payrollDailyData.payrollId,payrollDailyData.empName,
								payrollDailyData.jobId,payrollDailyData.jobDate,
								payrollDailyData.inTime,payrollDailyData.outTime,payrollDailyData.hoursWorked)>
								
							<cfelse>
							
								<cfset payrollPeriodData = StructNew()>		
								<cfset ppeData = empData[arrayLen(empData)]>
								<cfset ppeData = ListToArray(ppeData, " ", false)>
								
								<cfset payrollPeriodData.storeId = storeId>
								<cfset payrollPeriodData.ppeDate = ppeDate>
								<cfset payrollPeriodData.payrollId = payrollId>
								<cfset payrollPeriodData.empName = empName>	
														
								<cfset payrollPeriodData.ppeRegHours = trim(ppeData[3])>
								<cfset payrollPeriodData.ppeOThours = trim(ppeData[4])>
								<cfset payrollPeriodData.ppeRegPay = trim(ppeData[5])>
								<cfset payrollPeriodData.ppeOTPay = trim(ppeData[6])>
								
								<cfset payrollPeriodData.ppeRegHours = replace(payrollPeriodData.ppeRegHours,",","","all")>
								<cfset payrollPeriodData.ppeOThours = replace(payrollPeriodData.ppeOThours,",","","all")>
								<cfset payrollPeriodData.ppeRegPay = replace(payrollPeriodData.ppeRegPay,",","","all")>
								<cfset payrollPeriodData.ppeOTPay = replace(payrollPeriodData.ppeOTPay,",","","all")>
						
								
								<cfset application.daoObj.InsertPayrollPeriodTotals(payrollPeriodData.storeId,
								payrollPeriodData.ppeDate,payrollPeriodData.payrollId,payrollPeriodData.empName,payrollPeriodData.ppeRegHours,
								payrollPeriodData.ppeOThours,payrollPeriodData.ppeRegPay,payrollPeriodData.ppeOTPay)>
								
													
							</cfif>
						</cfloop>	
				</cfloop>
			
			<!---<cfcatch>
				
			</cfcatch>
			
			</cftry>--->
			
	</cfloop>
	
<!------>	<cfdump var="#payrollDailyData#">
	<cfdump var="#payrollPeriodData#">
	<cfdump var="#totalHours#">
