﻿<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<head>
</head>
<body>
Geo Tracker <br>
<script>
var BJ_ACCEPTABLE_ACCURACY = 100;
var BJ_MAX_POSITIONING_ATTEMPTS = 3;
var positioningAttempts = 0;
var watchId;
function initiate_geolocation() {
    if (navigator.geolocation) {
    // Request repeated updates.

        watchId = navigator.geolocation.watchPosition(handle_geolocation_query,handle_errors);

    } else {
        alert("I'm sorry, but geolocation services are not supported by your browser.");
    }
}

function handle_errors(error)
{
    switch(error.code)
    {
        case error.PERMISSION_DENIED: alert("user did not share geolocation data");
        break;

        case error.POSITION_UNAVAILABLE: alert("could not detect current position");
        break;

        case error.TIMEOUT: alert("retrieving position timed out");
        break;

        default: alert("unknown error");
        break;
    }
}


var watchcount=0;
document.write('<div id="watchdiv"><br></div>');
document.write('<div id="watchclr"><br></div>');

function handle_geolocation_query(position){

      document.getElementById("watchdiv").innerHTML= ('<br>Lat: ' + position.coords.latitude +
          '<br> Lon: ' + position.coords.longitude +
          '<br> Acc: ' + position.coords.accuracy + '<br> WatchCount: ' + watchcount);
    watchcount = watchcount + 1;

}
initiate_geolocation();
</script>
</body>
</html>