<cfinclude template="header.cfm">
<style type="text/css">
	tr.lightrow td {
	background-color: #FFFFFF;
	}
	tr.darkrow td {
	background-color: #EFEFEF;
	}
</style>

		<script type="text/javascript">
			function ValidateSubmit()
			{
				document.forms[0].submit();
			}	
		</script>
<form action="lpInvoiceTotals.cfm" method="post">
<div style="margin:10px"><h3>LP Invoice Totals</h3><br>

	<cfif NOT IsDefined("cookie.storeId")>
				<label for="bUnit">Select Store:</label>	
				<select id="bUnit" name="bUnit">
					<cfloop query="application.getAllUnits">
						<cfoutput>
			            	<option id="#Trim(unitName)#" value="#Trim(id)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ id>selected</cfif>>
							#Trim(unitName)#
							</option>
			            </cfoutput>
					</cfloop>
				</select>&nbsp;&nbsp;&nbsp;&nbsp;
			<cfelse>
				<cfoutput><input type="hidden" name="bUnit" value="#cookie.storeId#"></cfoutput>
			</cfif>
	
		<label for="fcYear">Year:</label>
		<select id="fcYear" name="fcYear">
			<cfloop index="i" from="#year(now())#" to="2011" step="-1">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcYear") AND form.fcYear EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>
		</select>	
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="fcPeriod">Period:</label>
		<select id="fcPeriod" name="fcPeriod">
			<cfloop index="i" from="1" to="13">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcPeriod") AND form.fcPeriod EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>
		</select>
		
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="fcWeek">Week:</label>
		<select id="fcWeek" name="fcWeek">					
			<cfloop index="i" from="1" to="4">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcWeek") AND form.fcWeek EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>			
		</select>

		<input type="button" onclick="ValidateSubmit()" value="View Invoices">

</form>

	<cfif IsDefined("form.bUnit")>
		
		<cfquery name="getCal" datasource="#application.eonDsn#">
			SELECT startDate 
			FROM businessCalendar
			WHERE YEAR = #form.fcYear#
		</cfquery>				
		

		<cfset currentDateEnd = application.utilObj.getDateByPeriodWeekDay(getCal.startDate,form.fcPeriod,form.fcWeek,7)>

		<cfset currentDateStart = dateformat(dateadd("d",-6,currentDateEnd), application.eonDateFormat)>
		<cfset currentDateEnd = dateformat(currentDateEnd, application.eonDateFormat)>
		
		<cfquery name="getInvoices" datasource="#application.eonDsn#">
			SELECT invoiceNumber, SUM(itemExtendedPrice) AS amount, purchaseCategories.categoryDescr, purchaseCategories.categoryId, invoiceDate
			FROM dbo.purchaseInvoices
			INNER JOIN dbo.businessUnit
			ON dbo.businessUnit.id = purchaseInvoices.businessunitid
			INNER JOIN dbo.purchaseItems
			ON purchaseItems.itemNumber = dbo.purchaseInvoices.itemNumber
			INNER JOIN dbo.purchaseCategories
			ON dbo.purchaseCategories.categoryId = purchaseItems.itemCategoryId
			WHERE
			invoiceDate BETWEEN '#currentDateStart#' AND '#currentDateEnd#'
			AND businessUnitId = '#form.bUnit#'
			GROUP BY invoiceNumber, purchaseCategories.categoryId, purchaseCategories.categoryDescr, invoiceDate
			ORDER BY invoiceNumber, purchaseCategories.categoryId, purchaseCategories.categoryDescr, invoiceDate
		</cfquery>
	
	<table width="500" style="border:green double 2px;margin:5px">
		<tr>
			<td><b>Invoice Number</b></td>
			<td><b>Invoice Date</b></td>
			<td><b>Category</b></td>
			<td><b>Amount</b></td>
		</tr>
		
		<cfset invNum = "">
		<cfset idx = 1>
		<cfloop query="getInvoices">	
			<cfset idx++>
			<cfset rowclass = "lightrow"> 
			<cfif idx mod 2>
				<cfset rowclass = "darkrow">
			</cfif>
					
				<cfif invNum NEQ invoiceNumber>	
					<tr>
						<td colspan="4">
							<hr style="border:dashed red; border-width:1px 0 0; height:0;">
						</td>
					</tr>
						<cfquery name="getFuel" datasource="#application.eonDsn#">
							SELECT [dbo].[GetTotalLPfuelSurchargeForInvoice] ('#invoiceNumber#') AS fuelCharge
						</cfquery>
					<tr>
						<td>
						<cfoutput><b>#invoiceNumber#</b></cfoutput>
						</td>
						
						<td>
						<cfoutput>#dateformat(invoiceDate, application.eonDateFormat)#</cfoutput>
						</td>
						
						<td>
						Fuel Surcharge
						</td>
						<td>
						<cfoutput>#dollarFormat(getFuel.fuelCharge)#</cfoutput>
						</td>				
					</tr>				
				</cfif>
						
				<cfoutput>
		        	<tr class="#rowclass#">
						<td colspan="2">

						</td>
						
						<td>
							#categoryDescr#
						</td>
						<td>
							#dollarFormat(amount)#
						</td>
					</tr>
		        </cfoutput>
				
			<cfset invNum = invoiceNumber>
		</cfloop>
		
	</table>
	
	</cfif>

<cfinclude template="footer.cfm">