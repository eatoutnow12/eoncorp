﻿<cfif application.eonMonitorTestMode EQ 0>
<!---	<cftry>
		<cfhttp method="Get" url="http://www.eatoutnow.net/ping.cfm?requestTimeout=50000" />
		
		<cfif cfhttp.StatusCode NEQ "200 OK">
			<cfset application.eonMonitorTestMode = 1>
		<cfelse>
			<cfset application.eonMonitorTestMode = 0>
		</cfif>
		
		<cfcatch>
			<cfset application.eonMonitorTestMode = 1>
		</cfcatch>
	</cftry>--->

	<cfset variables.criticalMinutes = 10>
	
	<cfquery name="getStatus" datasource="#application.eonProcData#">
		SELECT [businessUnitName]
		      ,[lastConnection]      
		FROM [storeSystemMonitor]
		WHERE DATEDIFF("n",lastConnection,GETDATE()) > #variables.criticalMinutes#
		AND notified = 0
	</cfquery>
	
	<cfloop query="getStatus">
	
		<!---Update Store status to 1 and send notification--->
		<cfquery name="updateStatus" datasource="#application.eonProcData#">
			UPDATE [storeSystemMonitor]
			SET [notified] = 1
			WHERE businessUnitName = '#getStatus.businessUnitName#'
		</cfquery>
	
		<cfset mailAttributes = {
			message="Internet is NOT working at Store: #getStatus.businessUnitName# since #TimeFormat(Now(),"long")# on #DateFormat(Now(),application.eonDateFormat)#",
			sendTo="notify@eatoutnow.net",
			sentFrom="Store Offline",
			subject="Store #getStatus.businessUnitName# is Offline"
		}>	
		
		<cfmodule template="sendMail.cfm" attributecollection="#mailAttributes#">
		
	</cfloop>
</cfif>