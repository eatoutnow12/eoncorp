﻿<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="1">

		
		<style>
		 	.headerRow {
		 		border-color: #727272;
				background-color:#ccc;
		 	}
			
			.highlighted {			
				background-color: yellow;			
			}
			
			.auditComment {
				background-color: #F2BABB;
			}

			td {
				cursor:default;
			}			
			
			#leftcolumn { width: 1050px; border: 1px solid #727272; magin: 50px;}
			#rightcolumn { width: 730px; border: 1px solid #727272; magin: 50px}
			#yoySales { width: 470px; border: 1px solid #727272; magin: 50px}
			.window {
				  position:absolute;
				  left:0;
				  top:0;
				  width:440px;
				  height:200px;
				  display:none;
				  z-index:9999;
				  padding:20px;
			}
			
			#mask {
			  position:absolute;
			  left:0;
			  top:0;
			  z-index:9000;
			  background-color:#000;
			  display:none;
			}
		</style>
			
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" /> 
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script> 
		<script type="text/javascript" src="js/jquery-ui-1.8.custom.min.js"></script> 
		
		<script type="text/javascript"> 
		$(function() {
			$("#startDate").datepicker();
		});
		
		$(function() {
			$("#endDate").datepicker();
		});	
	

		</script>
		
		<script>
			function modalWin(id) {
				if (window.showModalDialog) {
					window.showModalDialog("updateDSR.cfm" + '?id=' + id ,"name",
					"dialogWidth:400px;dialogHeight:450px");
				} else {
					window.open('updateDSR.cfm' + '?id=' + id,'name',
					'height=450,width=400,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no ,modal=yes, location=no, top=200, left=200');
				}
			}
			
			function submitForm()
			{
				document.forms[0].submit();
			}
		</script>
		
<script> 
var cancelclick; 
function ItemClicked(el, currentStatus) 
{ 
  cancelclick = false; 
  setTimeout("execclick()",500); 

} 

function execclick() 
{ 
  if(!cancelclick) console.log('booo');//alert("click executed"); 
} 

function ItemDoubleClicked(itemId) 
{ 
  	<cfif CompareNoCase(Trim(session.permission1), "E") EQ 0>
		cancelclick = true; 
		modalWin(itemId); 
		return false;
	</cfif>
} 

function handleEvent(oEvent,el, currentStatus) {
    <cfif CompareNoCase(Trim(session.permission1), "E") EQ 0>
	if (oEvent.ctrlKey) {
	  $.post("dbProxy.cfm", { action: 'setDSRStatus', id: el.id, statusId: currentStatus }, function(data){    	 
		  document.forms[0].submit();
   		} );
    }
	</cfif>
}

</script> 
		
<script> 
 
	$(document).ready(function() {	
	 
		//select all the a tag with name equal to modal
		$('a[name=modal]').click(function(e) {
			//Cancel the link behavior
			e.preventDefault();
			
			//Get the A tag
			var id = $(this).attr('href');
		
			//Get the screen height and width
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();
		
			//Set heigth and width to mask to fill up the whole screen
			$('#mask').css({'width':maskWidth,'height':maskHeight});
			
			//transition effect		
			$('#mask').fadeIn(1000);	
			$('#mask').fadeTo("slow",0.8);	
		
			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();
	              
			//Set the popup window to center
			$(id).css('top',  winH/2-$(id).height()/2);
			$(id).css('left', winW/2-$(id).width()/2);
		
			//transition effect
			$(id).fadeIn(2000); 
		
		});
		
		//if close button is clicked
		$('.window .close').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();
			
			$('#mask').hide();
			$('.window').hide();
		});		
		
		//if mask is clicked
		$('#mask').click(function () {
			$(this).hide();
			$('.window').hide();
		});			
		
	});
	 
</script>		
		
<cfset noteEnd = '<br/><br/><input type="button" value="Close it" class="close"/>'>	
<cfset notesStyle = "background:url(img/notice.png) no-repeat 0 0 transparent; width:326px; height:229px; padding:50px 0 20px 25px;">
<cfscript>
	objUtil = createObject("component", "cfc.Utils");	
</cfscript>
<cfoutput>
		
		
		
		<cfquery name="getBUnits" datasource="#application.eonDsn#">
			SELECT [businessUnit].[id], [businessUnit].[unitName] FROM [businessUnit]
			WHERE inactiveSinceDate IS NULL
			ORDER BY [unitName]
		</cfquery>
		
		<div id="formBody" style="padding-left:10px; padding-top:10px">	
			<form id="getDsrForm" name="getDsrForm" action="dsrReport.cfm" method="post">
				<p>
					<label for="bUnit">Select Store:</label>
					<select id="bUnit" name="bUnit" onchange="submitForm();">
						<cfloop query="getBUnits">
							<option id="#id#" value="#id#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ '#id#'>selected</cfif>>
							#unitName#
							</option>
						</cfloop>
					</select>					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					<label for="startDate">From Date:</label>			
					<input name="startDate" id="startDate" maxlength="12" size="10" type="text"<cfif IsDefined("form.startDate")> value="#form.startDate#"<cfelse>value=""</cfif> />
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					<label for="endDate">To Date:</label>			
					<input name="endDate" id="endDate" maxlength="12" size="10" type="text"<cfif IsDefined("form.endDate")> value="#form.endDate#"<cfelse>value=""</cfif> />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" name="submitDsr" id="submitDsr" value="Show DSR" onclick="submitForm();"></p>

			</form>
		</div>
	<hr noshade>
	<cfif IsDefined("form.startDate")>
			<cfset variables.startDate = form.startDate>
			<cfset variables.bUnit = form.bUnit>
			
			<cfif NOT IsDate(variables.startDate)>
				<b>Error: Please choose a From Date to view</b>
				<cfabort>
			</cfif>
			
			<cfif NOT IsDefined("form.endDate") OR NOT IsDate(form.endDate)>
				<cfset variables.endDate = form.startDate>
			<cfelse>
				<cfset variables.endDate = form.endDate>
			</cfif>
			
			
			
			<cfif DateDiff("d", Dateformat(variables.startDate, application.eonDateFormat), DateFormat(variables.endDate,application.eonDateFormat)) LT 0>			
				<b>Error: To Date should be after From Date</b>
				<cfabort>			
			</cfif>
	<div style="padding:10px">		


		<h3> Sales Summary</h3>			
		<div id="leftcolumn">		

			<table width="100%">
			
			<tr class="headerRow">
	
				<td valign="top">
					Date
				</td>
	
				<td valign="top">
					Day
				</td>
				
				<td valign="top">
					Net Sales
					<cfset NetSalesTotal = 0>
				</td>
				
				<td valign="top">
					Sales Tax
					<cfset SalesTaxTotal = 0>
				</td>
				
				<td valign="top">
					Total Deposit S/B
					<cfset TotalSBTotal = 0>
				</td>
							
				<td valign="top">
					Paid Outs
					<cfset PaidOutsTotal = 0>
				</td>
	
				<td valign="top">
					GC Redeemed
					<cfset GCRedeemedTotal = 0>
				</td>
				
				<td valign="top">
					A/R
					<cfset ArTotal = 0>
				</td>
										
				<td valign="top">
					AM Desposits
					<cfset amDepositsTotal = 0>
				</td>
				
				<td valign="top">
					PM Desposits
					<cfset pmDepositsTotal = 0>
				</td>
	
				<td valign="top">
					Visa/MC/Discover
					<cfset VisaTotal = 0>
				</td>
	
				<td valign="top">
					Amex
					<cfset AmexTotal = 0>
				</td>
	
				<td valign="top">
					Cash Over/Short
					<cfset CosTotal = 0>
				</td>
	
				<td valign="top">
					Coupon $
					<cfset couponTotal = 0>
				</td>
				
				<td valign="top">
					Promo $
					<cfset promoTotal = 0>
				</td>			
	
	
			</tr>
				<cfset ctr = 1>
				<cfloop index="theDate" from="#parseDateTime(variables.startDate)#" to="#parseDateTime(variables.endDate)#" step="1">
					
					<cfset formattedDate = dateFormat(theDate, application.eonDateFormat)>
					
					<cfquery name="getDSR" datasource="#application.eonDsn#">
						SELECT [dailySales].[dateOfSale] AS date, 
	
						NetSales = (SELECT [credit] FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						NetSalesId = (SELECT [Id] FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						NetSalesNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						SalesTax = (SELECT [credit] FROM [dailySales] 
						WHERE [note] = 'Sales Tax Payable' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						SalesTaxId = (SELECT [Id] FROM [dailySales] 
						WHERE [note] = 'Sales Tax Payable' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						SalesTaxNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'Sales Tax Payable' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PaidOuts = (SELECT [debit] FROM [dailySales] 
						WHERE [note] = 'Paid outs' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PaidOutsId = (SELECT [Id] FROM [dailySales] 
						WHERE [note] = 'Paid outs' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PaidOutsNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'Paid outs' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PaidOutsStatus = (SELECT [status] FROM [dailySales] 
						WHERE [note] = 'Paid outs' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						AmDeposit = (SELECT [debit] FROM [dailySales] 
						WHERE [note] = 'AM Deposit' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						AmDepositId = (SELECT [id] FROM [dailySales] 
						WHERE [note] = 'AM Deposit' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						AmDepositNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'AM Deposit' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						AmDepositStatus = (SELECT [status] FROM [dailySales] 
						WHERE [note] = 'AM Deposit' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PmDeposit = (SELECT [debit] FROM [dailySales] 
						WHERE [note] = 'PM Deposit' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PmDepositNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'PM Deposit' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PmDepositStatus = (SELECT [status] FROM [dailySales] 
						WHERE [note] = 'PM Deposit' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PmDepositId = (SELECT [id] FROM [dailySales] 
						WHERE [note] = 'PM Deposit' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						VisaMc = (SELECT [debit] FROM [dailySales] 
						WHERE [note] = 'Visa/MC/Discover' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')  
						,
						VisaMcId = (SELECT [id] FROM [dailySales] 
						WHERE [note] = 'Visa/MC/Discover' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						VisaMcNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'Visa/MC/Discover' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						VisaMcStatus = (SELECT [status] FROM [dailySales] 
						WHERE [note] = 'Visa/MC/Discover' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						Amex = (SELECT [debit] FROM [dailySales] 
						WHERE [note] = 'Amex' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')  
						,
						AmexId = (SELECT [id] FROM [dailySales] 
						WHERE [note] = 'Amex' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')  
						,
						AmexNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'Amex' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						AmexStatus = (SELECT [status] FROM [dailySales] 
						WHERE [note] = 'Amex' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						GCRedeemed = (SELECT [debit] FROM [dailySales] 
						WHERE [note] = 'G/C Redeemed' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						GCRedeemedId = (SELECT [id] FROM [dailySales] 
						WHERE [note] = 'G/C Redeemed' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						GCRedeemedNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'G/C Redeemed' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						GCRedeemedStatus = (SELECT [status] FROM [dailySales] 
						WHERE [note] = 'G/C Redeemed' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						AR = (SELECT [debit] FROM [dailySales] 
						WHERE [note] = 'A/R' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						ARId = (SELECT [id] FROM [dailySales] 
						WHERE [note] = 'A/R' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						ARNote = (SELECT [modifyComment] FROM [dailySales] 
						WHERE [note] = 'A/R' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						ARStatus = (SELECT [status] FROM [dailySales] 
						WHERE [note] = 'A/R' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						Coupon = (SELECT TOP 1 [debit] FROM [dailySales] 
						WHERE [note] = 'Coupon $' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#' AND credit = 0)
						,
						CouponId = (SELECT TOP 1 [id] FROM [dailySales] 
						WHERE [note] = 'Coupon $' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#' AND credit = 0)
						,
						CouponNote = (SELECT TOP 1 [modifyComment] FROM [dailySales] 
						WHERE [note] = 'Coupon $' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#' AND credit = 0)
						,
						Promo = (SELECT TOP 1 [debit] FROM [dailySales] 
						WHERE [note] = 'Promos $' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#' AND credit = 0)
						,
						PromoId = (SELECT TOP 1 [id] FROM [dailySales] 
						WHERE [note] = 'Promos $' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#' AND credit = 0)
						,
						PromoNote = (SELECT TOP 1 [modifyComment] FROM [dailySales] 
						WHERE [note] = 'Promos $' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#' AND credit = 0)	
						,
						GcSold = (SELECT  giftChecksSold
						FROM         dailyGeneralStoreData
						WHERE [dailyGeneralStoreData].businessUnitId = '#variables.bUnit#'
						AND [dailyGeneralStoreData].dateOccured = '#formattedDate#')
						,
						GcPaidOut = (SELECT  giftChecksPaidOut
						FROM         dailyGeneralStoreData
						WHERE [dailyGeneralStoreData].businessUnitId = '#variables.bUnit#'
						AND [dailyGeneralStoreData].dateOccured = '#formattedDate#')					
					 
						FROM [dailySales]
						WHERE [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#'
						GROUP BY [dailySales].[dateOfSale], [businessUnitId]
					</cfquery>
					
				<cfif isNumeric(getDSR.NetSales) AND isNumeric(getDSR.SalesTax)>
				<tr height="10" bgcolor="###iif(ctr MOD 2,DE('ffffff'),DE('efefef'))#">
			
					<td valign="top">
						#formattedDate#				
					</td>
					
					<td valign="top">
						#DayOfWeekAsString(DayOfWeek(formattedDate))#				
					</td>
	
					<td valign="top" id="#getDSR.NetSalesId#" onclick="ItemClicked(this.id)" ondblclick="ItemDoubleClicked(this.id)">
						<cfif Len(Trim(getDSR.NetSalesNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.NetSales)#
							<cfset NetSalesTotal += getDSR.NetSales> 
						<cfif Len(Trim(getDSR.NetSalesNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDsr.NetSalesId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>
									
					</td>
					
					<td valign="top" id="#getDSR.SalesTaxId#" ondblclick="ItemDoubleClicked(this.id)">
						<cfif Len(Trim(getDSR.SalesTaxNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.SalesTax)#
							 <cfset SalesTaxTotal += getDSR.SalesTax>
						<cfif Len(Trim(getDSR.SalesTaxNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDsr.SalesTaxId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>
						
					</td>
					
					<td valign="top">
						<cfif isNumeric(getDSR.NetSales) AND  isNumeric(getDSR.SalesTax)>
							<cfset temp = (LSParseNumber(getDSR.NetSales)+LSParseNumber(getDSR.SalesTax))>
							#dollarFormat(temp)#
							<cfset TotalSBTotal += temp>
						</cfif>
					</td>
					
					<td valign="top" <cfif getDSR.PaidOutsStatus eq 1> class="highlighted"<cfelseif getDSR.PaidOutsStatus eq 2> class="auditComment"</cfif> id="#getDSR.PaidOutsId#" onclick="handleEvent(event, this, #getDSR.PaidOutsStatus#)" ondblclick="ItemDoubleClicked(this.id)">
						<cfif Len(Trim(getDSR.PaidOutsNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.PaidOuts)#
							<cfset PaidOutsTotal += getDSR.PaidOuts>
						
						<cfif Len(Trim(getDSR.PaidOutsNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDsr.PaidOutsId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>
					</td>
	
					<td valign="top" <cfif getDSR.GCRedeemedStatus eq 1> class="highlighted"<cfelseif getDSR.GCRedeemedStatus eq 2> class="auditComment"</cfif> id="#getDSR.GCRedeemedId#" onclick="handleEvent(event, this, #getDSR.GCRedeemedStatus#)" ondblclick="ItemDoubleClicked(this.id)">
						<cfif Len(Trim(getDSR.GCRedeemedNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.GCRedeemed)#
							<cfset GCRedeemedTotal += getDSR.GCRedeemed>
						
						<cfif Len(Trim(getDSR.GCRedeemedNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDsr.GCRedeemedId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>						
					</td>
					
					<td valign="top" <cfif getDSR.ARStatus eq 1> class="highlighted"<cfelseif getDSR.ARStatus eq 2> class="auditComment"</cfif> id="#getDSR.ARId#" onclick="handleEvent(event, this, #getDSR.ARStatus#)" ondblclick="ItemDoubleClicked(this.id)">
						<cfif Len(Trim(getDSR.ARNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.AR)#
							<cfset ArTotal += getDSR.AR>
						
						<cfif Len(Trim(getDSR.ARNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDsr.ARId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>
					</td>
									
					<td valign="top" <cfif getDSR.AmDepositStatus eq 1> class="highlighted"<cfelseif getDSR.AmDepositStatus eq 2> class="auditComment"</cfif> id="#getDSR.AmDepositId#" onclick="handleEvent(event, this, #getDSR.AmDepositStatus#)" ondblclick="ItemDoubleClicked(this.id)">
						
						<cfif Len(Trim(getDSR.AmDepositNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.AmDeposit)#
							<cfset amDepositsTotal += getDSR.AmDeposit>
						
						<cfif Len(Trim(getDSR.AmDepositNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDSR.AmDepositId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>	

					</td>
			
					<td valign="top" <cfif getDSR.PmDepositStatus eq 1> class="highlighted"<cfelseif getDSR.PmDepositStatus eq 2> class="auditComment"</cfif> id="#getDSR.PmDepositId#" onclick="handleEvent(event, this, #getDSR.PmDepositStatus#)" ondblclick="ItemDoubleClicked(this.id)">
					
						
						<cfif Len(Trim(getDSR.PmDepositNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.PmDeposit)#
							<cfset pmDepositsTotal += getDSR.PmDeposit>
						
						<cfif Len(Trim(getDSR.PmDepositNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDSR.PmDepositId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>	
					
								
					</td>
	
					<td valign="top" id="#getDSR.VisaMCId#" <cfif getDSR.VisaMCStatus eq 1> class="highlighted"<cfelseif getDSR.VisaMCStatus eq 2> class="auditComment"</cfif> id="#getDSR.VisaMCId#" onclick="handleEvent(event, this, #getDSR.VisaMCStatus#)" ondblclick="ItemDoubleClicked(this.id)">
						
						<cfif Len(Trim(getDSR.VisaMCNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.VisaMC)#
							<cfset VisaTotal += getDSR.VisaMC>
							
						<cfif Len(Trim(getDSR.VisaMCNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDSR.VisaMCId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>						

					</td>
	
					<td valign="top" <cfif getDSR.AmexStatus eq 1> class="highlighted"<cfelseif getDSR.AmexStatus eq 2> class="auditComment"</cfif> id="#getDSR.AmexId#" onclick="handleEvent(event, this, #getDSR.AmexStatus#)" ondblclick="ItemDoubleClicked(this.id)">
						
						<cfif Len(Trim(getDSR.AmexNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.Amex)#
							<cfset AmexTotal += getDSR.Amex>
						
						<cfif Len(Trim(getDSR.AmexNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDSR.AmexId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>						
						
					</td>					
	
					<td valign="top">
										
					<cfif isNumeric(getDSR.netSales) AND  isNumeric(getDSR.SalesTax)>
					<cfset Calc1 = Evaluate(LSParseNumber(getDSR.Amex) + LSParseNumber(getDSR.VisaMc) + LSParseNumber(getDSR.AMdeposit) + LSParseNumber(getDSR.PMdeposit) + LSParseNumber(getDSR.gcRedeemed) + LSParseNumber(getDSR.Ar) + LSParseNumber(getDSR.paidOuts))>
					<cfset Calc2 = Evaluate(LSParseNumber(getDSR.netSales) + LSParseNumber(getDSR.salesTax))>
					<cfset variables.cashOverShort = Evaluate(Calc1 - Calc2)>

							<cfif variables.cashOverShort GT 0>
								#dollarFormat(variables.cashOverShort)#
							<cfelseif variables.cashOverShort LT 0>
								#dollarFormat(variables.cashOverShort)#
							<cfelse>
								<cfset variables.cashOverShort = 0>
								#dollarFormat(variables.cashOverShort)#
							</cfif>
							
							<cfset CosTotal += variables.cashOverShort>
					</cfif>
					</td>
	
					<td valign="top" id="#getDSR.CouponId#" ondblclick="ItemDoubleClicked(this.id)">						
						<cfif Len(Trim(getDSR.CouponNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.Coupon)#
							<cfset couponTotal += getDSR.Coupon>
						
						<cfif Len(Trim(getDSR.CouponNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDSR.CouponId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>	
					</td>
	
					<td valign="top" id="#getDSR.PromoId#" ondblclick="ItemDoubleClicked(this.id)">
						<cfif Len(Trim(getDSR.PromoNote))>
							<div style="display:table-cell;">
						</cfif>
							
							#DollarFormat(getDSR.Promo)#
							<cfset promoTotal += getDSR.Promo>
						
						<cfif Len(Trim(getDSR.PromoNote))>
							</div>
							<div style="cursor:pointer; vertical-align:top; display:table-cell;"><a href="##note-#getDSR.PromoId#" name="modal">
								<img src="img/comment.png" align="right" vspace="0" height="6" width="5" border="0"></a>
							</div>
						</cfif>	
					</td>	
			
				</tr>
				</cfif>			
					<cfset ctr++>
					
					<cfif Len(Trim(getDSR.GCRedeemedNote))>
						<div id="note-#getDsr.GCRedeemedId#" style="#notesStyle#" class="window">#getDSR.GCRedeemedNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.VisaMCNote))>
						<div id="note-#getDSR.VisaMCId#" style="#notesStyle#" class="window">#getDSR.VisaMCNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.AmexNote))>
						<div id="note-#getDSR.AmexId#" style="#notesStyle#" class="window">#getDSR.AmexNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.AmDepositNote))>
						<div id="note-#getDSR.AmDepositId#" style="#notesStyle#" class="window">#getDSR.AmDepositNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.PmDepositNote))>
						<div id="note-#getDSR.PmDepositId#" style="#notesStyle#" class="window">#getDSR.PmDepositNote# #noteEnd#</div>
					</cfif>
					
					<cfif Len(Trim(getDSR.NetSalesNote))>
						<div id="note-#getDsr.NetSalesId#" style="#notesStyle#" class="window">#getDSR.NetSalesNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.SalesTaxNote))>
						<div id="note-#getDsr.SalesTaxId#" style="#notesStyle#" class="window">#getDSR.SalesTaxNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.PaidOutsNote))>
						<div id="note-#getDsr.PaidOutsId#" style="#notesStyle#" class="window">#getDSR.PaidOutsNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.ARNote))>
						<div id="note-#getDsr.ARId#" style="#notesStyle#" class="window">#getDSR.ARNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.CouponNote))>
						<div id="note-#getDsr.CouponId#" style="#notesStyle#" class="window">#getDSR.CouponNote# #noteEnd#</div>
					</cfif>
					<cfif Len(Trim(getDSR.PromoNote))>
						<div id="note-#getDsr.PromoId#" style="#notesStyle#" class="window">#getDSR.PromoNote# #noteEnd#</div>
					</cfif>
				</cfloop>
				
				
				<tr style="font-weight:bold" bgcolor="###iif(ctr MOD 2,DE('ffffff'),DE('efefef'))#">
					<td colspan="2">						
						Totals:						
					</td>
					<td>
					#dollarFormat(NetSalesTotal)#
					</td>
					
					<td>
					#dollarFormat(SalesTaxTotal)#
					</td>
					
					<td>
					#dollarFormat(TotalSBTotal)#
					</td>
					
					<td>
					#dollarFormat(PaidOutsTotal)#
					</td>
					
					<td>
					#dollarFormat(GCRedeemedTotal)#
					</td>
					
					<td>
					#dollarFormat(ArTotal)#
					</td>
					
					<td>
					#dollarFormat(amDepositsTotal)#
					</td>					
					
					<td>
					#dollarFormat(pmDepositsTotal)#
					</td>
					
					<td>
					#dollarFormat(VisaTotal)#
					</td>
					
					<td>
					#dollarFormat(AmexTotal)#
					</td>
					
					<td>
					#dollarFormat(CosTotal)#
					</td>
					
					<td>
					#dollarFormat(couponTotal)#
					</td>
					
					<td>
					#dollarFormat(promoTotal)#
					</td>
				</tr>
	
			</table>
				
		</div>
		
		<h3> Other Data </h3>
				<div id="rightcolumn">
					<table>
					
					<tr class="headerRow">
						<td valign="top">
							Date
						</td>
						
						<td valign="top">
							Day
						</td>
						
						<td valign="top">
							Coupon ##
							<cfset couponNumTotal = 0>
						</td>
						
						<td valign="top">
							Promos ##
							<cfset promoNumTotal = 0>
						</td>				
						
						<td valign="top">
							Deletes
							<cfset deletesNumTotal = 0>
						</td>
						
						<td valign="top">
							Cust. Count
							<cfset custNumTotal = 0>
						</td>
									
						<td valign="top">
							Labor $
							<cfset laborNumTotal = 0>
						</td>
									
						<td valign="top">
							Labor %
							<cfset netSalesNumTotal = 0>
						</td>
			
						<td valign="top">
							GC Sold
							<cfset gcSoldNumTotal = 0>
						</td>				
			
						<td valign="top">
							GC Paid Out
							<cfset gcPaidOutNumTotal = 0>
						</td>
			
					</tr>
		
					
		
						<cfset ctr = 1>
						<cfloop index="theDate" from="#parseDateTime(variables.startDate)#" to="#parseDateTime(variables.endDate)#" step="1">
							
							<cfset formattedDate = dateFormat(theDate, application.eonDateFormat)>
							
							<cfquery name="getGenData" datasource="#application.eonDsn#">						
								SELECT  couponsNum, promosNum, deletesNum, customerCount, giftChecksSold, laborDollar, giftChecksPaidOut, NetSales = (SELECT [credit] FROM [dailySales] 
								WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#variables.bUnit#'
								AND [dateOfSale] = '#formattedDate#')
								FROM         dailyGeneralStoreData
								WHERE [dailyGeneralStoreData].businessUnitId = '#variables.bUnit#'
								AND [dailyGeneralStoreData].dateOccured = '#formattedDate#'
							</cfquery>
					<cfif IsNumeric(getGenData.NetSales)>	
						<tr bgcolor="###iif(ctr MOD 2,DE('ffffff'),DE('efefef'))#">
							<td>
								#formattedDate#
							</td>
						
							<td valign="top">
								#DayOfWeekAsString(DayOfWeek(formattedDate))#				
							</td>
							
							<td valign="top">
								#getGenData.couponsNum#
								<cfset couponNumTotal += getGenData.couponsNum>				
							</td>
							
							<td valign="top">
								#getGenData.promosNum#
								<cfset promoNumTotal += getGenData.promosNum>
							</td>					
							
							<td valign="top">
								#DollarFormat(getGenData.deletesNum)#
								<cfset deletesNumTotal += getGenData.deletesNum>
							</td>
							
							<td valign="top">
								#getGenData.customerCount#
								<cfset custNumTotal += getGenData.customerCount>
							</td>
							
							<td valign="top">
								#dollarFormat(getGenData.laborDollar)#
								<cfset laborNumTotal += getGenData.laborDollar>
							</td>
							
							<td valign="top">
							<cfif isNumeric(getGenData.NetSales) AND getGenData.NetSales GT 0>
								#DecimalFormat((getGenData.laborDollar/getGenData.NetSales * 100))#%
								<cfset netSalesNumTotal += getGenData.NetSales>
							</cfif>
							</td>
							
							<td valign="top">
								#dollarFormat(getGenData.giftChecksSold)#
								<cfset gcSoldNumTotal += getGenData.giftChecksSold>
							</td>					
												
							<td valign="top">
								#dollarFormat(getGenData.giftChecksPaidOut)#
								<cfset gcPaidOutNumTotal += getGenData.giftChecksPaidOut>
							</td>	
						</tr>
					</cfif>				
							<cfset ctr++>
						</cfloop>
						
						<tr style="font-weight:bold">
							<td colspan="2">
								Totals:
							</td>
					
							<td>
								#couponNumTotal#
							</td>
							
							<td>
								#promoNumTotal#
							</td>
							
							<td>
								#dollarFormat(deletesNumTotal)#
							</td>
							
							<td>
								#custNumTotal#
							</td>
							
							<td>
								#dollarFormat(laborNumTotal)#
							</td>
							
							<td>
							<cfif netSalesNumTotal GT 0>
								#DecimalFormat((laborNumTotal/netSalesNumTotal) * 100)# %
							</cfif>
							</td>
							
							<td>
								#dollarFormat(gcSoldNumTotal)#
							</td>
							
							<td>
								#dollarFormat(gcPaidOutNumTotal)#
							</td>
							
						</tr>
					
					</table>
				
				</div>
		
				




<h3> YOY Sales Comparison</h3>
		<cfset CurrentYear = Year(form.startDate)>
		<cfset PreviousYear = CurrentYear - 1>
		
		<div id="yoySales">
			<table>
			
			<tr class="headerRow">
				<td valign="top">
					Date
				</td>
				
				<td valign="top">
					Day
				</td>
				
				<td valign="top">
					Net Sales - #CurrentYear#
					<cfset NetSalesCurrYearTotal = 0>
				</td>
				
				<td valign="top">
					Net Sales - #PreviousYear#
					<cfset NetSalesPriorYearTotal = 0>
				</td>				
				
				<td valign="top">
					% Diff
				</td>
	
			</tr>

				<cfset ctr = 1>
				<cfloop index="theDate" from="#parseDateTime(variables.startDate)#" to="#parseDateTime(variables.endDate)#" step="1">

					<cfset formattedDate = dateFormat(theDate, application.eonDateFormat)>										
					
					<cfquery name="getCalendarCurrent" datasource="#application.eonDsn#">
						SELECT StartDate from businessCalendar
						where year = '#CurrentYear#' 
						AND businessEntityId = (SELECT entityId FROM businessUnit WHERE id= '#variables.bUnit#')
					</cfquery>
					
					<cfset currentDateAsString = DateFormat(getCalendarCurrent.StartDate, application.eonDateFormat)>
					
					<cfset periodForDate = objUtil.getPeriod(currentDateAsString, formattedDate)>
					<cfset weekForDate = objUtil.getWeek(currentDateAsString, formattedDate)>
					<cfset dayForDate = objUtil.getDay(formattedDate)>
					
					<cfquery name="getCalendarPrior" datasource="#application.eonDsn#">
						SELECT StartDate from businessCalendar
						where year = '#PreviousYear#' 
						AND businessEntityId = (SELECT entityId FROM businessUnit WHERE id= '#variables.bUnit#')
					</cfquery>
					<cfset priorDateAsString = DateFormat(getCalendarPrior.StartDate, application.eonDateFormat)>
					
				<cfif getCalendarPrior.RecordCount>
					
					<cfset priorYearDate = objUtil.getDateByPeriodWeekDay(priorDateAsString, periodForDate, weekForDate, dayForDate)>
					<cfset priorYearDate = DateFormat(priorYearDate, application.eonDateFormat)>
					
					<cfquery name="getYOY" datasource="#application.eonDsn#">
						SELECT 
						[dailySales].[dateOfSale] AS date
						, 
						NetSales = (SELECT TOP 1 [credit] FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#')
						,
						PriorNetSales = (SELECT TOP 1 [credit] FROM [dailySales] 
						WHERE [note] = 'Net Food Sales' AND [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#priorYearDate#')
					 
						FROM [dailySales]
						WHERE [businessUnitId] = '#variables.bUnit#'
						AND [dateOfSale] = '#formattedDate#'
						GROUP BY [dailySales].[dateOfSale], [businessUnitId]
					</cfquery>					
				</cfif>
				<cfif isNumeric(getYOY.NetSales)>	
				<tr bgcolor="###iif(ctr MOD 2,DE('ffffff'),DE('efefef'))#">
					<td>
						#formattedDate#
					</td>
				
					<td valign="top">
						#DayOfWeekAsString(DayOfWeek(formattedDate))#				
					</td>
					
					<td valign="top">
						#dollarFormat(getYOY.NetSales)#
						<cfset NetSalesCurrYearTotal += getYOY.NetSales>				
					</td>
					
					<td valign="top" title="#DateFormat(priorYearDate, "full")#">
						#dollarFormat(getYOY.PriorNetSales)#
						<cfset NetSalesPriorYearTotal += getYOY.PriorNetSales>
					</td>					
					
					<td valign="top">
						<cfif IsNumeric(getYOY.NetSales) AND IsNumeric(getYOY.PriorNetSales) AND getYOY.PriorNetSales GT 0>
							#DecimalFormat(Evaluate((getYOY.NetSales - getYOY.PriorNetSales)/getYOY.PriorNetSales) * 100)# %
						<cfelse>
							0%
						</cfif>
					</td>					

				</tr>
				</cfif>	
					<cfset ctr++>
				</cfloop>
				
				<tr style="font-weight:bold">
					<td colspan="2">
						Totals:
					</td>
					<td>
						#dollarFormat(NetSalesCurrYearTotal)#
					</td>
					
					<td>
						#dollarFormat(NetSalesPriorYearTotal)#
					</td>
					
					<td>
						<cfif IsNumeric(NetSalesCurrYearTotal) AND IsNumeric(NetSalesPriorYearTotal) AND NetSalesPriorYearTotal GT 0>
						#DecimalFormat(Evaluate((NetSalesCurrYearTotal - NetSalesPriorYearTotal)/NetSalesPriorYearTotal) * 100)# %
						</cfif>
					</td>
				</tr>
			
			</table>
		
		</div>
		
				</div>



			
				</div>
	
	</cfif>	
	
	 

	 
	<!-- Mask to cover the whole screen --> 
	<div id="mask"></div> 	
</cfoutput>	

<cfinclude template="footer.cfm">