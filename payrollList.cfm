﻿<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="2">
		
		<style>
		 	.headerRow {
		 		border-color: #727272;
				background-color:#ccc;
		 	}
			
			.highlighted {			
				background-color: yellow;			
			}
			
			.auditComment {
				background-color: #F2BABB;
			}

			td {
				cursor:default;
			}			
			
			#leftcolumn { width: 1050px; border: 1px solid #727272; magin: 50px;}
			#rightcolumn { width: 730px; border: 1px solid #727272; magin: 50px}
			#yoySales { width: 470px; border: 1px solid #727272; magin: 50px}
			.window {
				  position:absolute;
				  left:0;
				  top:0;
				  width:440px;
				  height:200px;
				  display:none;
				  z-index:9999;
				  padding:20px;
			}
			
			#mask {
			  position:absolute;
			  left:0;
			  top:0;
			  z-index:9000;
			  background-color:#000;
			  display:none;
			}
		</style>
			
		
		<script type="text/javascript"> 
		$(function() {
			$("#startDate").datepicker();
		});
		
		$(function() {
			$("#endDate").datepicker();
		});	
	

		</script>
		
		<script>
			function modalWin(id) {
				if (window.showModalDialog) {
					window.showModalDialog("updateDSR.cfm" + '?id=' + id ,"name",
					"dialogWidth:400px;dialogHeight:450px");
				} else {
					window.open('updateDSR.cfm' + '?id=' + id,'name',
					'height=450,width=400,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no ,modal=yes, location=no, top=200, left=200');
				}
			}
		</script>
		

		
<script> 
 
	$(document).ready(function() {	
	 
		//select all the a tag with name equal to modal
		$('a[name=modal]').click(function(e) {
			//Cancel the link behavior
			e.preventDefault();
			
			//Get the A tag
			var id = $(this).attr('href');
		
			//Get the screen height and width
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();
		
			//Set heigth and width to mask to fill up the whole screen
			$('#mask').css({'width':maskWidth,'height':maskHeight});
			
			//transition effect		
			$('#mask').fadeIn(1000);	
			$('#mask').fadeTo("slow",0.8);	
		
			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();
	              
			//Set the popup window to center
			$(id).css('top',  winH/2-$(id).height()/2);
			$(id).css('left', winW/2-$(id).width()/2);
		
			//transition effect
			$(id).fadeIn(2000); 
		
		});
		
		//if close button is clicked
		$('.window .close').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();
			
			$('#mask').hide();
			$('.window').hide();
		});		
		
		//if mask is clicked
		$('#mask').click(function () {
			$(this).hide();
			$('.window').hide();
		});			
		
	});
	 
</script>		

		<cfquery name="getDates" datasource="#application.eonProcData#">
			SELECT CONVERT(VARCHAR(10),ppeDate,101) as ppeDatevar
			FROM dbo.payrollDaily
			GROUP BY ppeDate
			ORDER BY ppeDate desc
		</cfquery>
		
		<div style="padding:10px">
			<cfoutput>
            	<form id="getDsrForm" name="getDsrForm" action="payrollList.cfm" method="post">
					<p>
						<label for="bUnit">Select Store:</label>
						<select id="bUnit" name="bUnit">
							<cfloop query="application.getAllUnits">
								<option id="#Trim(unitName)#" value="#Trim(unitName)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ '#Trim(unitName)#'>selected</cfif>>
								#Trim(unitName)#
								</option>
							</cfloop>
						</select>					
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End Date:
						<select id="ppeDate" name="ppeDate">
							<cfloop query="getDates">
								<option id="#ppeDatevar#" value="#ppeDatevar#" <cfif IsDefined("form.ppeDate") AND form.ppeDate EQ '#ppeDatevar#'>selected</cfif>>
								#Trim(ppeDatevar)#
								</option>
							</cfloop>
						</select>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="periodData" value="1" <cfif IsDefined("form.periodData")> checked</cfif>> Show Period Data 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="submit" name="submit" value="Show Payroll">
				</form>
            </cfoutput>
		<cfif IsDefined("form.ppeDate")>	

				<cfquery name="getPayrollDaily" datasource="#application.eonProcData#">
					SELECT businessUnit
					,[payRollId]
					,[empName]
					,[jobId]
					,CONVERT(VARCHAR(10),jobDate,101) as jobDate
					,CONVERT(VARCHAR(10),jobInTime,8) as ClockIn
					,CONVERT(VARCHAR(10),jobOutTime,8) as ClockOut
					,[hoursWorked]
					FROM [payrollDaily]
					WHERE ppeDate = '#form.ppeDate#'  
					AND businessUnit = '#form.bUnit#'
					ORDER BY businessUnit, empName, jobDate, jobId
				</cfquery>
				<cfdump var="#getPayrollDaily#" metainfo="false" label="Daily Payroll Data">

			<cfif IsDefined("form.periodData")>
				<cfquery name="getPayrollPeriod" datasource="#application.eonProcData#">
					SELECT [businessUnit]
					,[empName]
					,[regularHours]
					,[OtHours] AS OvertimeHours
					,[regularPay]
					,[otPay]  AS overTimePay    
					,[payrollId]
					FROM [payrollPeriod]
					WHERE ppeDate = '#form.ppeDate#'  
					AND businessUnit = '#form.bUnit#'
					ORDER BY businessUnit, empName
				</cfquery>
				<cfdump var="#getPayrollPeriod#" metainfo="false" label="Payroll Period Data">
			</cfif>			
			
			
			
			
		</cfif>
	</div>

<cfinclude template="footer.cfm">