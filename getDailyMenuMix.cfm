﻿	<cfparam name="attributes.mmixFileList" default="">

			
	<cfset fileCount = attributes.mmixFileList.RecordCount-1>
	
	<cfset busiDate = DateAdd("d",-1,now())>
	<cfset busiDate = DateFormat(busiDate,application.eonDateFormat)>
	
<cfif attributes.mmixFileList.RecordCount>
	<cftry>
		<cftransaction>
			<cfloop index="fileIdx" from="0" to="#fileCount#">
			
				<cffile action = "read" file = "#attributes.mmixFileList.Directory#\MMIX#fileIdx#.TXT" variable = "mmixData" charset="ISO-8859-1">
				
				<cfset lineDelim = Chr(13) & Chr(10)>
				
				<cfset columnDelim = '^'>
				
				<cfset numDelim = Chr(32)>			
				
				<cfset parsedData = ListToArray(mmixData,lineDelim, false)>			
				
				<cfset storeNum = Trim(listGetAt(parsedData[1],2,'-'))>
				<cfif Len(storeNum) EQ 4 AND IsNumeric(storeNum)>
					<cfquery name="getStoreId" dbtype="query">
						SELECT id FROM application.getAllUnits
						WHERE unitName = '#storeNum#'
					</cfquery>
					
					<cfset storeId = getStoreId.id>
					
					<cfloop index="itemNum" from="2" to="#arraylen(parsedData)#">
						
							<cfset mmLine = ListToArray(parsedData[itemNum],columnDelim, false)>
							<cfset numData = ListToArray(mmLine[3],numDelim, false)>
							
							<cfset mmixDataStruct = StructNew()>
							
							<cfset mmixDataStruct.businessUnit = storeId>
							<cfset mmixDataStruct.businessDate = busiDate>
							<cfset mmixDataStruct.itemId = Trim(mmLine[1])>
							<cfset mmixDataStruct.itemDescr = Trim(mmLine[2])>
							<cfset mmixDataStruct.qtySold = Trim(numData[1])>
							<cfset mmixDataStruct.netSales = Trim(LSParseNumber(numData[2]))>
							
							<cfif isNumeric(mmixDataStruct.qtySold) AND mmixDataStruct.qtySold GT 0>
								<cfset application.daoObj.InsertDailyMenuMix(argumentCollection = mmixDataStruct)>
							</cfif>

					</cfloop>
				</cfif>
	
		</cfloop>
	</cftransaction>							
		<cfcatch type="any">
			<cfmodule template="errorReport.cfm" report="#cfcatch#" parsedData="#parsedData#">
		</cfcatch>	
					
	</cftry>

</cfif>