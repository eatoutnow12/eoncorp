﻿		<cfset borrowDetail = queryNew("PayrollId, Employee, Store, W1_Normal, W1_OT, W2_Normal, W2_OT","VarChar, VarChar, VarChar, Decimal, Decimal, Decimal, Decimal")>

		<cfset ppeDate = dateformat(dateAdd("d",-1,Now()),application.eonDateFormat)>
		<cfset W1ppeDate = dateformat(dateAdd("d",-7,ppeDate),application.eonDateFormat)>
		
	
		<cfset Week2EndDate = dateformat(dateAdd("d",-1,Now()),application.eonDateFormat)>
		<cfset Week1EndDate = dateformat(dateAdd("d",-7,Week2EndDate),application.eonDateFormat)>
		
		<cfset Week1StartDate = dateformat(dateAdd("d",-6,Week1EndDate),application.eonDateFormat)>
		<cfset Week2StartDate = dateformat(dateAdd("d",-6,Week2EndDate),application.eonDateFormat)>		
		
		<cfquery name="getCal" datasource="#application.eonDsn#">
			SELECT startDate 
			FROM businessCalendar
			WHERE YEAR = '#year(Now())#'
		</cfquery>
		
		<cfset CurrentWeek = application.utilObj.getWeek(getCal.startDate, Week2EndDate)>

		<cfset mailAttributes = {
			from="Payroll Reports <payroll@eatoutnow.net>",
			to="management@eatoutnow.net",
			cc="office@eatoutnow.net",
			subject="Payroll Reports - PPE: #ppeDate#"
		}
		/>
		
	<cfmail attributeCollection="#mailAttributes#" type="html">
			<p><br>
			<cfquery name="NoPayRollId" datasource="#application.eonProcData#" >
				SELECT businessUnit as Store, payrollId, empName as Employee
				FROM  payrollPeriod
				WHERE ({ fn LENGTH(payrollId) } <> 5)
				AND ppeDate = '#ppeDate#'
				ORDER BY businessUnit, empName
			</cfquery>
			<cfdump var="#NoPayRollId#" metainfo="false" label="Employees with Missing/Incorrect Payroll Ids" >
			
		<cfif CurrentWeek EQ 2 OR CurrentWeek EQ 4>
			<p><br>
			<cfquery name="NewHires" datasource="#application.eonProcData#">
				DECLARE @ThisPPEDate DATETIME;
				DECLARE @LastPPEDate DATETIME;
				
				SET @ThisPPEDate = DATEADD(DAY,-1,GETDATE())
				SET @LastPPEDate = DATEADD(DAY,-14,@ThisPPEDate)
				
				SELECT payrollId, empName as Employee, businessUnit as Store
				FROM payrollDaily WHERE 
				ppeDate = CONVERT(VARCHAR(10),@ThisPPEDate,101)
				AND
				LEN(payrollId) = 5
				AND 
				payrollId NOT IN (SELECT payrollId FROM payrollDaily WHERE ppeDate = CONVERT(VARCHAR(10),@LastPPEDate,101))
				GROUP BY payrollId, empName, businessUnit
				ORDER BY businessUnit, empName
			</cfquery>
			
			<cfdump var="#NewHires#" metainfo="false" label="New Hires" >

			<p><br>			
			<cfquery name="Terminations" datasource="#application.eonProcData#">
				DECLARE @ThisPPEDate DATETIME;
				DECLARE @LastPPEDate DATETIME;
				
				SET @ThisPPEDate = DATEADD(DAY,-1,GETDATE())
				SET @LastPPEDate = DATEADD(DAY,-14,@ThisPPEDate)
				
				SELECT payrollId, empName as Employee, businessUnit as Store
				FROM payrollDaily WHERE 
				ppeDate = CONVERT(VARCHAR(10),@LastPPEDate,101)
				AND 
				payrollId NOT IN (SELECT payrollId FROM payrollDaily WHERE ppeDate = CONVERT(VARCHAR(10),@ThisPPEDate,101))
				GROUP BY payrollId, empName, businessUnit
				ORDER BY businessUnit, empName
			</cfquery>
			
			<cfdump var="#Terminations#" metainfo="false" label="Terminations" >

			<p><br>			
			<cfquery name="Borrowed" datasource="#application.eonProcData#">
				DECLARE @AllPayrollIds TABLE 
				(payRollId NCHAR(10),businessUnit NCHAR(10), empName NVARCHAR(50));
				
				INSERT INTO @AllPayrollIds (
					payRollId,
					businessUnit,
					empName
				)
				SELECT  payRollId,businessUnit, empName
				FROM payrollDaily
				WHERE LEN(payRollId) = 5 AND ppeDate = '#ppeDate#'
				GROUP BY payRollId,businessUnit, empName;
				
				DECLARE @DupePayrollIds TABLE 
				(payRollId NCHAR(10), empName NVARCHAR(50));
				INSERT INTO @DupePayrollIds (
					payRollId,
					empName
				)SELECT payRollId, empName
				FROM @AllPayrollIds
				GROUP BY payRollId, empName
				HAVING ( COUNT(payRollId) > 1 )
				ORDER BY empName
				SELECT payRollId AS PayrollId, empName AS Employee, businessUnit AS Store FROM @AllPayrollIds
				WHERE payRollId IN (SELECT payRollId FROM @DupePayrollIds) ORDER BY empName, businessUnit
			</cfquery>
			
			<cfloop query="Borrowed">			
				<cfquery name="borrowInfo" datasource="#application.eonProcData#">
					SELECT 
					W1_Normal = ISNULL((SELECT regularHours FROM dbo.payrollPeriod WHERE businessUnit = '#Store#' 
					AND payrollId = '#PayrollId#' 
					AND ppeDate = '#W1ppeDate#'),0), 
					W1_OT = ISNULL((SELECT OtHours FROM dbo.payrollPeriod WHERE businessUnit = '#Store#' 
					AND payrollId = '#PayrollId#' 
					AND ppeDate = '#W1ppeDate#'),0),
					W2_Normal = (ISNULL((SELECT regularHours FROM dbo.payrollPeriod WHERE businessUnit = '#Store#' 
					AND payrollId = '#PayrollId#' 
					AND ppeDate = '#ppeDate#'),0)-ISNULL((SELECT regularHours FROM dbo.payrollPeriod WHERE businessUnit = '#Store#' 
					AND payrollId = '#PayrollId#' 
					AND ppeDate = '#W1ppeDate#'),0)),
					W2_OT = (ISNULL((SELECT OtHours FROM dbo.payrollPeriod WHERE businessUnit = '#Store#' 
					AND payrollId = '#PayrollId#' 
					AND ppeDate = '#ppeDate#'),0)-ISNULL((SELECT OtHours FROM dbo.payrollPeriod WHERE businessUnit = '#Store#' 
					AND payrollId = '#PayrollId#' 
					AND ppeDate = '#W1ppeDate#'),0))
					FROM dbo.payrollPeriod
					WHERE 
					businessUnit = '#Store#' 
					AND payrollId = '#PayrollId#' 
					AND ppeDate = '#ppeDate#'
				</cfquery>
				
				<cfif borrowInfo.RecordCount>			
					<cfset queryAddRow(borrowDetail)>
					
					<cfset querySetCell(borrowDetail, "payRollId", #payRollId#)>
					<cfset querySetCell(borrowDetail, "Employee", #Employee#)>
					<cfset querySetCell(borrowDetail, "Store", #Store#)>
					<cfset querySetCell(borrowDetail, "W1_Normal", #borrowInfo.W1_Normal#)>
					<cfset querySetCell(borrowDetail, "W1_OT", #borrowInfo.W1_OT#)>
					<cfset querySetCell(borrowDetail, "W2_Normal", #borrowInfo.W2_Normal#)>
					<cfset querySetCell(borrowDetail, "W2_OT", #borrowInfo.W2_OT#)>
				</cfif>
				
			</cfloop>
			
			<cfdump var="#borrowDetail#" metainfo="false" label="Borrowed Employees">
			
		</cfif>
			<p><br>
			<cfquery name="InAfter11" datasource="#application.eonProcData#">
				SELECT payrollId, empName as Employee, businessUnit as Store, CONVERT(VARCHAR(10),jobInTime,8) as ClockIn, CONVERT(VARCHAR(10),jobOutTime,8) as ClockOut, 
				CONVERT(VARCHAR(10),jobDate,101) as Date  FROM payrollDaily
				WHERE 
				ppeDate = '#ppeDate#'
				AND
				(
				(DATEPART(hh,jobInTime) = 23)
				OR
				(DATEPART(hh,jobInTime) BETWEEN 1 AND 7)
				)
				ORDER BY businessUnit, empName, jobDate
			</cfquery>
			
			<cfdump var="#InAfter11#" metainfo="false" label="Clocked In between 11pm and 8:00am" >
			<p><br>
			
			
			<cfquery name="AutoClockout" datasource="#application.eonProcData#">
				SELECT payrollId, empName as Employee, businessUnit as Store, CONVERT(VARCHAR(10),jobInTime,8) as ClockIn, CONVERT(VARCHAR(10),jobOutTime,8) as ClockOut, 
				CONVERT(VARCHAR(10),jobDate,101) as Date  FROM payrollDaily
				WHERE 
				1 = 1
				AND DATEPART(hh,jobOutTime) = 23
				AND DATEPART(mi,jobOutTime) = 00
				AND DATEPART(ss,jobOutTime) = 00
				AND ppeDate = '#ppeDate#'
				ORDER BY businessUnit, empName, jobDate
			</cfquery>
			
			<cfdump var="#AutoClockout#" metainfo="false" label="Automatic clock-outs" >
		<p><br>
	
			
			<cfquery name="TrainingHours" datasource="#application.eonProcData#">
				SELECT payrollId, empName as Employee, businessUnit as Store, SUM(HoursWorked) AS hours FROM payrollDaily
				WHERE 
				jobId IN (13,14)
				AND
				ppeDate = '#ppeDate#'
				GROUP BY payrollId, empName, businessUnit
			</cfquery>
			
			<cfdump var="#TrainingHours#" metainfo="false" label="Training Hours" >
			
			<cfif CurrentWeek EQ 2 OR CurrentWeek EQ 4>
				<p><br>	
						
				<cfquery name="OTHours" datasource="#application.eonProcData#">
					SELECT     empName AS Employee, businessUnit AS Store, otHours AS OverTimeHours
					FROM         payrollPeriod
					WHERE     (OtHours >= 30) 
					AND ppeDate = '#ppeDate#'
					ORDER BY businessUnit, empName
				</cfquery>
				
				<cfdump var="#OTHours#" metainfo="false" label="Overtime Hours Exceeding 30 hours">
			<cfelse>
				<p><br>	
						
				<cfquery name="OTHours" datasource="#application.eonProcData#">
					SELECT     empName AS Employee, businessUnit AS Store, otHours AS OverTimeHours
					FROM         payrollPeriod
					WHERE     (OtHours >= 15) 
					AND
					ppeDate = '#ppeDate#'
					ORDER BY businessUnit, empName
				</cfquery>
				
			<cfdump var="#OTHours#" metainfo="false" label="Overtime Hours Exceeding 15 hours">
			</cfif>
			
			<p><br>		
			
		<cfif CurrentWeek EQ 2 OR CurrentWeek EQ 4>
					
			<cfquery name="NoOtHours" datasource="#application.eonProcData#">
				SELECT    empName AS Employee, businessUnit AS Store, otHours AS OverTimeHours, regularHours AS RegularHours
				FROM      payrollPeriod
				WHERE     (regularHours > 80 AND OtHours = 0)
				AND
				ppeDate = '#ppeDate#'
				ORDER BY businessUnit, empName
			</cfquery>
			
			<cfdump var="#NoOtHours#" metainfo="false" label="Regular Hours Exceed 80, yet No Over-time Hours">
			<p><br>
		</cfif>	
	</cfmail>
	
	<cfif CurrentWeek EQ 2 OR CurrentWeek EQ 4>
		<cfmodule template="createTime002Paychex.cfm">
	</cfif>