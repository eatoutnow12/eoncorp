<cfset folderPrefix = DateFormat(Now(),"yyyymmdd")>
<cfset save_path = ExpandPath('./') & "Invoices\" & folderPrefix & "\">
<cfset daoObj = createObject("component","cfc.dsrDAO")>

<cfif NOT directoryExists(save_path)>
	<cfdirectory action="create" directory="#save_path#">
</cfif>

<cfquery name="getAllUnits" datasource="#application.eonDsn#">
	SELECT id, unitIdSysco FROM dbo.businessUnit
</cfquery>	

<!---<p>Open a connection--->
<cfftp connection = "lpConnect" 
   username = "POPEYENOW"
   password = "sys2PPE##"
   server = "b2b.sysco.com"
   action = "open" 
   stopOnError = "Yes" timeout="50000" passive="true"> 

<cfif cfftp.succeeded>
	<cfftp connection = "lpConnect"
	    action = "LISTDIR"
	    stopOnError = "Yes"
	    name = "ListDirs"
	    directory = "/Outbound" timeout="50000" passive="true">
</cfif>

<cfloop query="ListDirs">
	<cfif NOT isdirectory>
		<cfftp
		action="getfile"
		connection="lpConnect"
		localfile="#save_path##name#"
		remotefile="#path#" 
		failifexists="false"  
		transfermode="auto" 
		timeout="50000" 
		passive="true"
		stoponerror="true"  
		/>
	</cfif>
	
	<cfif cfftp.succeeded>
		<cfftp
		action="remove"
		connection="lpConnect"
		item="#path#" stoponerror="true">
	</cfif>
</cfloop>

<!---<p>Close the connection:--->
<cfftp 
	connection = "lpConnect"
    action = "close"
    stopOnError = "true">
<!---<p>Did it succeed? <cfoutput>#cfftp.succeeded#</cfoutput>--->

<cfdirectory directory="#save_path#" action="list" name="invoiceFiles" filter="*.txt" />
<!---<cftry>
	<cftransaction>--->
	<cfloop query="invoiceFiles">
		<cffile action = "read" file = "#directory#\#name#" variable = "invoiceFile">
		<cfset lineDelim = Chr(13) & Chr(10)>
		<cfset columnDelim = chr(32) & chr(32)>
		
		<cfscript>
			invoiceData = ListToArray(invoiceFile,lineDelim, false);
		</cfscript>
	
		<cfloop index="i" from="2" to="#ArrayLen(invoiceData)#">
	
			<cfset invoiceLineData = invoiceData[i]>			
			<cfset buyerId = Trim(Right(Mid(invoiceLineData,87,10),4))>
		
			<cfquery name="getBunit" dbtype="query">
				SELECT id FROM getAllUnits 
				WHERE unitIdSysco = '#buyerId#'
			</cfquery>		
			<cfset bUnitId = getBunit.id>
							
			<cfset dcNumber = Trim(Mid(invoiceLineData,1,7))>
			<cfset invoiceNumber = Trim(Mid(invoiceLineData,56,7))>
			<cfset itemNumber = Trim(Mid(invoiceLineData,140,6))>		
			<cfset quantity = Trim(Mid(invoiceLineData,132,5))>
			<cfset invoiceLineNumber = Trim(Mid(invoiceLineData,126,4))>	
			<cfdump var="#variables#" abort="true" >
			<cfif isNumeric(quantity) AND Left(quantity,1) NEQ "0" AND quantity GT 0 AND IsNumeric(invoiceLineNumber)>
				
				<cfset invoiceDate = Trim(Mid(invoiceLineData,46,8))>
				<cfset invoiceDate = Insert("-",invoiceDate,4)>
				<cfset invoiceDate = Insert("-",invoiceDate,2)>
				<cfset invoiceDate = DateFormat(invoiceDate, application.eonDateFormat)>				
				
				<cfset invoiceType = Trim(Mid(invoiceLineData,42,2))>			

				<cfset invoiceLineNumber = abs(invoiceLineNumber)>
					
				<cfset unitOfMeasure = Trim(Mid(invoiceLineData,208,2))>
				<cfset itemPack = Trim(Mid(invoiceLineData,212,4))>
				<cfset itemSize = Trim(Mid(invoiceLineData,218,7))>
				<cfset itemCube = Trim(Mid(invoiceLineData,227,7))>
				
				
				<cfset itemUPC = Trim(Mid(invoiceLineData,271,14))>
				<cfset itemPrice = Trim(Mid(invoiceLineData,338,13))>
				
				<cfif Right(itemPrice,1) EQ "-">
					<cfset itemPrice = removeChars(itemPrice,Len(itemPrice), 1)>
					<cfset itemPrice = (itemPrice * -1)>
				</cfif>				
				
				<cfset itemExtnPrice = Trim(Mid(invoiceLineData,358,13))>
				
				<cfif Right(itemExtnPrice,1) EQ "-">
					<cfset itemExtnPrice = removeChars(itemExtnPrice,Len(itemExtnPrice), 1)>
					<cfset itemExtnPrice = (itemExtnPrice * -1)>
				</cfif>	
				
				<cfif NOT IsNumeric(itemPrice)>
					<cfset itemPrice = 0>
				</cfif>
				
				<cfif NOT IsNumeric(itemExtnPrice)>
					<cfset itemExtnPrice = 0>
				</cfif>
				
				<cfset itemTaxCode = Trim(Mid(invoiceLineData,373,1))>
				<cfset itemTaxAmount = Trim(Mid(invoiceLineData,377,13))>
				
				<cfif NOT IsNumeric(itemTaxAmount)>
					<cfset itemTaxAmount = 0>
				</cfif>
				
				<cfset temp = daoObj.InsertInvoiceItem('#dcNumber#','#bUnitId#','#invoiceNumber#','#invoiceDate#','#invoiceType#','#invoiceLineNumber#','#quantity#','#itemNumber#',
					'#unitOfMeasure#','#itemPack#','#itemSize#','#itemCube#','#itemUPC#','#itemPrice#','#itemTaxCode#','#itemTaxAmount#','#itemExtnPrice#')>
				
			</cfif>
			
			<cfset invoiceLineData = "">
			<cfset itemNumber = "">
			<cfset quantity = "">
			<cfset invoiceLineNumber = "">
		</cfloop>
		
	</cfloop>
<!---</cftransaction>
	<cfcatch>
		<cfmodule template="errorReport.cfm" report="#cfcatch#" parsedData="#invoiceData#">
	</cfcatch>
</cftry>--->