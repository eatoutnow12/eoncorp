<cftry>

<cffile action = "read" file = "#ExpandPath('./')#\DSR\Bank.csv" variable = "Report" charset="ISO-8859-1">

<cfset myObj = createObject("component", "cfc.utils")>

<!--- Convert the CSV to an array of arrays. --->
<cfset arrCSV = myObj.CSVToArray(
	CSVData = Report,
	Delimiter = ",",
	Qualifier = """"
	) />	

	<cfset mailAttributes = {
		from="do-not-reply@eatoutnow.net",
		to="kenny@eatoutnow.net",
		subject="Bank Deposits Report"
	}
	/>
	
<cfset bankQry = queryNew("UnitName,PostingDate,Debit,TypeId","Integer,Date,Decimal,Integer")>

<cfloop from="2" to="#arraylen(arrCSV)#" index="i">
	
	<cfif arrCSV[i][1] EQ "D">
		
		<cfset StoreId = arrCSV[i][6]>		
		<cfset StoreId = Trim(RemoveChars(storeId,1,8))>
				
		<cfset TypeId = arrCSV[i][7]>
		
		<cfset PostingDate = arrCSV[i][2]>
		<cfset PostingDate = dateFormat(PostingDate, application.eonDateFormat)>
		
		<cfset PostingAmount = arrCSV[i][9]>
		
		<cfif Len(Trim(PostingAmount)) AND PostingAmount GT 0 AND ListFind("165,301", TypeId) AND isNumeric(StoreId)>
			<cfset temp = queryAddRow(bankQry)>
			
			<cfset temp = querySetCell(bankQry,"UnitName", storeId)>
			<cfset temp = querySetCell(bankQry,"PostingDate", PostingDate)>
			<cfset temp = querySetCell(bankQry,"Debit", PostingAmount)>
			<cfset temp = querySetCell(bankQry,"TypeId", TypeId)>
		</cfif>
		
	</cfif>
	
</cfloop>
<cftransaction>

	<cfquery name="GetDSR" datasource="#application.eonDsn#">
		SELECT  [dailySales].id, businessUnit.unitName, dateOfSale, debit,
		CASE
		    WHEN [dailySales].note IN ('AMEX', 'Visa/MC/Discover') THEN CONVERT(INTEGER,165)
		    WHEN [dailySales].note IN ('AM Deposit', 'PM Deposit', 'G/C Redeemed') THEN CONVERT(INTEGER,301)
		    END AS 'typeId', note,[dailySales].status
		FROM         dailySales
		INNER JOIN businessUnit
		ON dailySales.businessUnitId = businessUnit.id
		WHERE note IN ('AMEX', 'AM Deposit', 'PM Deposit', 'Visa/MC/Discover', 'G/C Redeemed')
		AND status = 0		
		AND  debit > CASE WHEN note IN ('AMEX','G/C Redeemed') THEN 0 ELSE -1 END
		ORDER BY dateOfSale, businessUnitId
	</cfquery>
	
	<cfset openIssues = queryNew("UnitName,DateOfSale,Debit,Type","Integer,VarChar,Decimal,VarChar")>
	<cfset closedTodayIssues = queryNew("UnitName,DateOfSale,Debit,Type","Integer,VarChar,Decimal,VarChar")>
	
	<cfloop query="GetDSR">
		<cfquery dbtype="query" name="closedIssues">
			SELECT * FROM bankQry
			WHERE 
			bankQry.UnitName = #getDSR.unitName#
			AND bankQry.Debit = #NumberFormat(getDSR.debit, ".00")#
			AND bankQry.TypeId = #getDSR.typeId#
		</cfquery>
		<cfif NOT closedIssues.RecordCount>	
			<cfset temp = queryAddRow(openIssues)>
					
			<cfset temp = querySetCell(openIssues,"UnitName", getDSR.unitName)>
			<cfset temp = querySetCell(openIssues,"DateOfSale", DateFormat(getDSR.dateOfSale,application.eonDateFormat))>
			<cfset temp = querySetCell(openIssues,"Debit", getDSR.debit)>
			<cfset temp = querySetCell(openIssues,"Type", getDSR.note)>
		<cfelse>
			<cfif getDSR.status EQ 0>
				<cfquery name="HighLightItems" datasource="#application.eonDsn#">
					UPDATE dailySales
					SET status = 1
					WHERE id = '#getDSR.id#'
				</cfquery>
			</cfif>
			<cfset temp = queryAddRow(closedTodayIssues)>
					
			<cfset temp = querySetCell(closedTodayIssues,"UnitName", getDSR.unitName)>
			<cfset temp = querySetCell(closedTodayIssues,"DateOfSale", DateFormat(getDSR.dateOfSale,application.eonDateFormat))>
			<cfset temp = querySetCell(closedTodayIssues,"Debit", getDSR.debit)>
			<cfset temp = querySetCell(closedTodayIssues,"Type", getDSR.note)>
		</cfif>
	</cfloop>
	
		<cfquery dbtype="query" name="openIssues">
			SELECT UnitName,DateOfSale,Debit,Type FROM openIssues
			ORDER by DateOfSale, Type, UnitName
		</cfquery>
		
		<cfmail port="465" useSSL="true" attributeCollection="#mailAttributes#" type="html">
			<cfdump var="#openIssues#" label="Open Issues To Date" metainfo="false"><br>
			<cfdump var="#closedTodayIssues#" label="Deposits found and marked closed today" metainfo="false">
		</cfmail>

</cftransaction>

	<cfcatch type="any">
		<cfdump var="#cfcatch#">
		<cfmail attributeCollection="#mailAttributes#" type="html">
			<cfdump var="#cfcatch#" label="Error Occured">
		</cfmail>
	</cfcatch>

</cftry>