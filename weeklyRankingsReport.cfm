﻿	<cfif application.dsrNoError EQ 1>
		
		<cfset variables.EndDate = DateFormat(DateAdd("d",now(),-1),application.eonDateFormat)>
		<cfset variables.StartDate = DateFormat(DateAdd("d",variables.EndDate,-6),application.eonDateFormat)>
		
		<cfset variables.PrevYearEndDate = DateFormat(application.utilObj.getDateForYOY(variables.EndDate),application.eonDateFormat)>
		<cfset variables.PrevYearStartDate = DateFormat(DateAdd("d",variables.PrevYearEndDate,-6),application.eonDateFormat)>
		
		<cfset reportPeriod = application.utilObj.getPeriod(application.eonYearStartDate,variables.StartDate)>
		<cfset reportWeek = application.utilObj.getWeek(application.eonYearStartDate,variables.StartDate)>
		<cfset reportYear = application.eonYear>
		<cfset reportFile = ExpandPath('./') & "\logs\weeklyRankings\" & "rankings-P" & reportPeriod & "W" & reportWeek & "-" & reportYear & ".xls">
		<cfset reportFilePdf = ExpandPath('./') & "\logs\weeklyRankings\" & "weeklyRankings.pdf">		
		
		<cfquery name="Sales" datasource="#application.eonDsn#">
			SELECT dbo.businessUnit.unitName AS Store,FLOOR(SUM(credit)) AS Data
			FROM dbo.dailySales
			INNER JOIN dbo.businessUnit
			ON dbo.dailySales.businessUnitId = dbo.businessUnit.id
			WHERE dbo.dailySales.dateOfSale BETWEEN '#variables.StartDate#' AND '#variables.EndDate#'
			AND dbo.dailySales.note = 'Net Food Sales'
			GROUP BY dbo.businessUnit.unitName
			ORDER BY SUM(credit) DESC
		</cfquery>
		
		<cfquery name="YoYSales" datasource="#application.eonDsn#">
			SELECT dbo.businessUnit.unitName AS Store, Data = (SELECT [dbo].[GetYOYSalesDiffByStoreAndDates] (
			   '#variables.StartDate#'
			  ,'#variables.EndDate#'
			   ,'#variables.PrevYearStartDate#'
			  ,'#variables.PrevYearEndDate#'
			  ,businessUnit.id))
			FROM dbo.businessUnit
			ORDER BY Data DESC
		</cfquery>
		
		<cfquery name="Labor" datasource="#application.eonDsn#">
			SELECT dbo.businessUnit.unitName AS Store, Data = (SELECT [dbo].[GetLaborPercentByStoreAndDates] (
			   '#variables.StartDate#'
			  ,'#variables.EndDate#'   
			  ,businessUnit.id))
			FROM dbo.businessUnit
			ORDER BY Data
		</cfquery>
		
		<cfquery name="Cos" datasource="#application.eonDsn#">
			SELECT businessUnit.unitName AS Store, Data = (SELECT [dbo].[GetCOSPercentByStoreAndDates] (
			  '#variables.StartDate#'
			  ,'#variables.EndDate#'
			  ,businessUnit.id   
			  ))
			FROM dbo.businessUnit
			ORDER BY Data
		</cfquery>
		
		<cfquery name="Sos" datasource="#application.eonDsn#">
			SELECT businessUnit.unitName AS Store, Data = (SELECT [dbo].[GetWTDSosByStoreAndDateRange] (
			  businessUnit.id   
			  ,'#variables.StartDate#'
			  ,'#variables.EndDate#'
			  ,7
			  ))
			FROM dbo.businessUnit
			ORDER BY Data
		</cfquery>
		
		<cfquery name="avgCheck" datasource="#application.eonDsn#">
			SELECT dbo.businessUnit.unitName AS Store, Data = (SELECT [dbo].[GetAvgCheckByStoreAndDate] (
			   '#variables.StartDate#'
			  ,'#variables.EndDate#'   
			  ,businessUnit.id))
			FROM dbo.businessUnit
			ORDER BY Data DESC
		</cfquery>
		
		<cfquery name="yoyAvgCheck" datasource="#application.eonDsn#">
			SELECT dbo.businessUnit.unitName AS Store, Data = (SELECT [dbo].[GetYOYAvgCheckByStoreAndDates] (
			   '#variables.StartDate#'
			  ,'#variables.EndDate#'
			   ,'#variables.PrevYearStartDate#'
			  ,'#variables.PrevYearEndDate#'
			  ,businessUnit.id))
			FROM dbo.businessUnit
			ORDER BY Data DESC
		</cfquery>
		
		<cfquery name="transactions" datasource="#application.eonDsn#">
			SELECT dbo.businessUnit.unitName AS Store, Data = (SELECT SUM(customerCount) 
			FROM dailyGeneralStoreData WHERE [businessUnitId] = businessUnit.id
			AND [dateOccured] BETWEEN '#variables.StartDate#' and '#variables.EndDate#')
			FROM dbo.businessUnit
			ORDER BY Data DESC
		</cfquery>
		
		<cfquery name="yoyTransactions" datasource="#application.eonDsn#">
			SELECT dbo.businessUnit.unitName AS Store, Data = (SELECT [dbo].[GetYOYGuestCountByStoreAndDates] (
			   '#variables.StartDate#'
			  ,'#variables.EndDate#'
			   ,'#variables.PrevYearStartDate#'
			  ,'#variables.PrevYearEndDate#'
			  ,businessUnit.id))
			FROM dbo.businessUnit
			ORDER BY Data DESC
		</cfquery>
		
		<cfprocessingdirective suppressWhiteSpace = "Yes">
		<cfsavecontent variable="rankingsContent">
		<style>
					
					table {				
						font-family:monospace;
						font-size:14pt;
						font-weight:bold;
					}
					
											
					td {
						width:inherit;
						padding-left:10px;
					}
		
					
		</style>
			<table border="1" cellpadding="1" cellspacing="2">
				<tr>
					<td colspan="10">
						&nbsp;
					</td>			
				</tr>
				
				<tr>
					<td colspan="10" align="center">
						<cfoutput><b>EON Rankings - P#reportPeriod# Week#reportWeek#</b></cfoutput>
					</td>			
				</tr>
				
				<tr>
					<td colspan="10">
						&nbsp;
					</td>			
				</tr>
				
				<tr>
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>Net Sales</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="Sales">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td>
									#DollarFormat(Data)#
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>YoY Sales +/-</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="YoYSales">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td <cfif Sgn(Data) EQ -1> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
									#Data#%
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>Labor</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="Labor">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td <cfif Data GT 21.5> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
									#Data#%
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>COS</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="Cos">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td <cfif Data GT 33> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
									#Data#%
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>SOS</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="Sos">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td <cfif Data GTE 181 AND Data LTE 210> bgcolor="##F88017" style="color:##FFFFFF"<cfelseif Data GT 210> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
									#Data#
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
				</tr>
				
				<tr>
					<td colspan="10">
						&nbsp;
					</td>			
				</tr>
				
				<tr>
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>Avg Check</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="avgCheck">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td>
									#Dollarformat(Data)#
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>YoY Avg Check +/-</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="yoyAvgCheck">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td <cfif Sgn(Data) EQ -1> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
									#Data#%
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>Transactions</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="transactions">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td>
									#Data#
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
					<td>
						&nbsp;
					</td>
					
					<td>
						<table border="1" cellpadding="1" cellspacing="2">
						<tr>
							<td colspan="2"><b>YoY Transactions +/-</b></td>
						</tr>
						<cfoutput>
		                	
							<cfloop query="yoyTransactions">
							<tr>
								<td>
									<b>#Store#</b>
								</td>
								
								<td <cfif Sgn(Data) EQ -1> bgcolor="##EE0000" style="color:##FFFFFF"</cfif>>
									#Data#
								</td>
							</tr>
							</cfloop>
							
		                </cfoutput>
						</table>
					</td>
					
					<td>
						&nbsp;
					</td>
					
					<td>
						&nbsp;
					</td>
					
				</tr>
			</table>
		
		</cfsavecontent>
		
		</cfprocessingdirective>
		
		<cffile action="write" file="#reportFile#" output="#rankingsContent#">
		
				<cfset mailAttributes = {
					from="Eon Reports <do-not-reply@eatoutnow.net>",
					to="management@eatoutnow.net",
					cc="allstores@eatoutnow.net",
					subject="Weekly Rankings Report: P#reportPeriod#-W#reportWeek#"
				}
				/>
				
		<cfmail attributeCollection="#mailAttributes#" type="html">
			#replace(rankingsContent,'border="1" cellpadding="1" cellspacing="2"','','all')#
			<cfsilent>				
				<cfmailparam file="#reportFile#">
			</cfsilent>
		</cfmail>
		
	</cfif>