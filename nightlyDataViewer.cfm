<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="15">	
		
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" /> 
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script> 
		<script type="text/javascript" src="js/jquery-ui-1.8.custom.min.js"></script> 
		
		<script type="text/javascript"> 
		$(function() {
			$("#startDate").datepicker();
		});
		
		$(function() {
			$("#endDate").datepicker();
		});	
	

		</script>

		<script type="text/javascript">
			function ValidateSubmit()
			{
				document.forms[0].submit();
			}	
		</script>
<div style="margin:10px">

<h3>Nightly Data Viewer</h3><br>

<cfoutput><form action="#getFileFromPath( cgi.script_name )#" method="post"></cfoutput>
	<cfif NOT IsDefined("cookie.storeId")>
				<label for="bUnit">Select Store:</label>	
				<select id="bUnit" name="bUnit">
					<cfloop query="application.getAllUnits">
						<cfoutput>
			            	<option id="#Trim(unitName)#" value="#Trim(id)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ id>selected</cfif>>
							#Trim(unitName)#
							</option>
			            </cfoutput>
					</cfloop>
				</select>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<cfelse>
				<cfoutput><input type="hidden" name="bUnit" value="#cookie.storeId#"></cfoutput>
	</cfif>
 
					<label for="startDate">From Date:</label>			
					<cfoutput><input name="startDate" id="startDate" maxlength="12" size="10" type="text"<cfif IsDefined("form.startDate")> value="#form.startDate#"<cfelse>value=""</cfif> /></cfoutput>
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					<label for="endDate">To Date:</label>			
					<cfoutput><input name="endDate" id="endDate" maxlength="12" size="10" type="text"<cfif IsDefined("form.endDate")> value="#form.endDate#"<cfelse>value=""</cfif> /></cfoutput>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type="button"  onclick="ValidateSubmit()" value="Show Data"></p>

</form>		

<cfif IsDefined("form.bUnit")>
	<cfset startDate = Trim(form.startDate)>
	<cfset endDate = Trim(form.endDate)>
	<cfset variables.storeId = Trim(form.bUnit)>

	<cfif NOT isDate(endDate)>
		<cfset endDate = startDate>
	</cfif>

	<cfset sosDayNum = DateDiff("d", Dateformat(startDate, application.eonDateFormat), DateFormat(endDate,application.eonDateFormat))>
	
	<cfset sosDayNum = sosDayNum+1>
	
	<cfquery datasource="#application.eonDsn#" name="getSos">
		SELECT dateEntered, dateModified, businessDate, sosSeconds, carCountTimer, carCountMicros, safeTotal, gcTotal
		FROM storeNightlyData
		WHERE businessDate BETWEEN '#startDate#' AND '#endDate#'
		AND businessUnitId = '#variables.storeId#'	
		ORDER BY businessDate
	</cfquery>
	
	<cfquery datasource="#application.eonDsn#" name="getAvgSos">
		SELECT [dbo].[GetWTDSosByStoreAndDateRange] ('#variables.storeId#','#startDate#','#endDate#',#sosDayNum#) AS avgNum
	</cfquery>

				<table width="800" style="border:green double 2px;margin:20px">
					<tr>
						<td>
						<b>Business Date</b>
						</td>
						<td>
						<b>Date Entered</b>
						</td>
						<td>
						<b>Date Modified</b>
						</td>
						<td>
						<b>SOS</b>
						</td>
						<td>
						<b># Cars - Timer</b>
						</td>
						<td>
						<b># Cars - Micros</b>
						</td>
						<td>
						<b>Safe Total</b>
						</td>
						<td>
						<b>GC Total</b>
						</td>
					</tr>
					<cfif getsos.RecordCount>
					<cfloop query="getSos">
					<tr>
						<td>
						<cfoutput>#dateFormat(BusinessDate, application.eonDateFormat)#</cfoutput>
						</td>
						<td>
						<cfoutput>#dateFormat(dateEntered, application.eonDateFormat)# #timeFormat(dateEntered, "short")#</cfoutput>
						</td>
						<td>
						<cfoutput>#dateFormat(dateModified, application.eonDateFormat)# #timeFormat(dateModified, "short")#</cfoutput>
						</td>
						<td>
						<cfoutput>#sosSeconds#</cfoutput>
						</td>
						<td>
						<cfoutput>#carCountTimer#</cfoutput>
						</td>
						<td>
						<cfoutput>#carCountMicros#</cfoutput>
						</td>
						<td>
						<cfoutput>#dollarFormat(safeTotal)#</cfoutput>
						</td>
						<td>
						<cfoutput>#dollarFormat(gcTotal)#</cfoutput>
						</td>
					</tr>
					</cfloop>
					<tr><td colspan="8" align="center"><b>Weighted Average:</b> <cfoutput>#getAvgSos.avgNum#</cfoutput></td></tr>
					<cfelse>
						<tr><td colspan="8">No Data submitted</td></tr>
					</cfif>
		
				</table>	
	
</cfif>

</div>

<cfinclude template="footer.cfm">