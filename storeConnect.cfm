﻿	<cfparam name="url.storeId" default="0" type="numeric">
	<cfset variables.storeId = Trim(storeId)>
<cfif application.eonMonitorTestMode EQ 0>

	
	<cfset mailAttributes = {
		message="Internet is restored at Store: #variables.storeId#",
		sendTo="notify@eatoutnow.net",
		sentFrom="Store Online",
		subject="Store #variables.storeId# is back Online"
	}>
	
	<cfif Len(variables.storeId) EQ 4>
	
		<!---Get Last Known Status--->
		<cfquery name="getStatus" datasource="#application.eonProcData#">
			SELECT [notified]
			FROM [storeSystemMonitor]
			WHERE businessUnitName = '#variables.storeId#'
			AND [storeSystemMonitor].[notified] = 1
		</cfquery>
		
		<!---If status = 1, send message saying: "Internet Working"--->
		<cfif getStatus.RecordCount>
			<cfmodule template="sendMail.cfm" attributecollection="#mailAttributes#">
		</cfif>
		
		<!---Update Store Access Record with current date and time and set status to 0.--->
		<cfquery name="updateStatus" datasource="#application.eonProcData#">
		UPDATE [storeSystemMonitor]
		   SET [lastConnection] = GETDATE()
		      ,[notified] = 0
		      WHERE businessUnitName = '#variables.storeId#'
		</cfquery>
		
	</cfif>
<cfelse>
		<!---Update Store Access Record with current date and time and set status to 0.--->
		<cfquery name="updateStatus" datasource="#application.eonProcData#">
		UPDATE [storeSystemMonitor]
		   SET [lastConnection] = GETDATE()
		      ,[notified] = 0
		      WHERE businessUnitName = '#variables.storeId#'
		</cfquery>
</cfif>