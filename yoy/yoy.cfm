<cfset myObj = createObject("component", "cfc.utils")>
<cfset daoObj = createObject("component","cfc.dsrDAO")>

<cfdirectory action="list" directory="#ExpandPath('./')#" name="csvList" filter="*.csv">

<cfloop query="csvList">
				<cffile action = "read" file = "#ExpandPath('./')##csvList.name#" variable = "Report" charset="ISO-8859-1">
				
				
				<!--- Convert the CSV to an array of arrays. --->
				<cfset arrCSV = myObj.CSVToArray(
					CSVData = Report,
					Delimiter = ",",
					Qualifier = """"
					) />
				
				<cfset startDaysList = "1,13,25,37">
				
				<cfloop index="i" from="1" to="#ArrayLen(arrCSV)#">	
					<cfif ListFind(startDaysList, i)>
						<cfset startDate = arrCSV[i][1]>
						<cfset startDate = DateFormat(startDate, "mm/dd/yyyy")>
						<cfset endDate = DateAdd("d", startDate,6)>
					<cfelse>
						<cfset storeId = arrCSV[i][1]>
						
						<cfquery name="getBusinessUnit" datasource="#application.eonDsn#">
							SELECT id FROM businessUnit 
							WHERE unitName = '#storeId#' 
						</cfquery>
						<cfif getBusinessUnit.recordcount>	
							<cfset variables.netSalesCodeId = daoObj.getAccountCode(getBusinessUnit.id, '4100-%')>
							
							<cfset weeklySale = arrCSV[i][2]>
							<cfset weeklySale = lsParseNumber(weeklySale)>
							<cfset dailySale = (weeklySale/7)>
							<cfset dailySale = lsParseNumber(dailySale)>
							<cfset dailySale = decimalFormat(dailySale)>
							<cfset dailySale = lsParseNumber(dailySale)>
					
							<cfloop index="theDate" from="#parseDateTime(startDate)#" to="#parseDateTime(endDate)#" step="1">
								<cfset theDate = DateFormat(theDate, "mm/dd/yyyy")>
								<!---<cfdump var="#theDate# - #storeId# , #dailySale#"><br>--->
								<cfset daoObj.InsertSalesData(getBusinessUnit.id, theDate, variables.netSalesCodeId, '0', dailySale, 'Net Food Sales')>
							</cfloop>
						</cfif>
					</cfif>
					
				
				</cfloop>
				
</cfloop>