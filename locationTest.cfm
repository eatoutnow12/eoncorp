﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>Location</title>
		
	</head>

	<script>
		if (navigator.geolocation) {
		  navigator.geolocation.getCurrentPosition(success, error);
		} else {
		  alert("Not Supported!");
		}
		
		function success(position) {
		  console.log(position.coords.latitude);
		  console.log(position.coords.longitude);
		}
		
		function error(msg) {
		  console.log(typeof msg == 'string' ? msg : "error");
		}
		
		var watchId = navigator.geolocation.watchPosition(function(position) {  
		  console.log(position.coords.latitude);
		  console.log(position.coords.longitude);
		});
		
		navigator.geolocation.clearWatch(watchId);
	</script>

	
	<body>
	</body>
	
	
<html>