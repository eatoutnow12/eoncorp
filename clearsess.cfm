<a href="index.cfm"><img src="images/login_images/back.gif" border="0"></a>
<br><br>

<h2>Session Variables:</h2>

<cfdump var="#session#">

<br><br>
	<!--- clear all application variables --->
	<cflock scope="SESSION" type="EXCLUSIVE" timeout="10">
		<cfset StructClear(session)>
	</cflock>
	
<br><br>	

<h2>Session Variables After Clear:</h2>

<cfdump var="#session#">