﻿<cfparam name="url.period" default="">

<cfif isNumeric(url.period)>

<cfset pathToCSV = ExpandPath('./') & '\Bank\P' & url.period & '\'>

<cfif NOT directoryExists(pathToCSV)>
	<cfset directoryCreate(pathToCSV)>
</cfif>

	<cftry>
	
	<cffile action = "read" file = "#pathToCSV#BankTrans.csv" variable = "Report" charset="ISO-8859-1">
	
	<cfset myObj = createObject("component", "cfc.utils")>
	<cfset daoObj = createObject("component", "cfc.dsrDAO")>
	
	<!--- Convert the CSV to an array of arrays. --->
	<cfset arrCSV = myObj.CSVToArray(
		CSVData = Report,
		Delimiter = ",",
		Qualifier = """"
		) />
	
	<cfquery name="Stores" datasource="#application.eonDsn#">
	Select * from businessUnit
	</cfquery>
	
	<cfloop from="2" to="#arraylen(arrCSV)#" index="i">
		
		<cfif arrCSV[i][1] EQ "D">
			
			<cfset StoreId = arrCSV[i][6]>		
			<cfset StoreId = Trim(RemoveChars(storeId,1,8))>
			
			<cfquery dbtype="query" name="store">
				select id from Stores where unitName = '#StoreId#'
			</cfquery>
			
			<cfset StoreId = Trim(store.id)>	
						
			<cfset TypeId = arrCSV[i][7]>
	
		
			<cfset PostingDate = arrCSV[i][2]>
			<cfset PostingDate = dateFormat(PostingDate, application.eonDateFormat)>
			
			<cfset PostingAmount = arrCSV[i][9]>
			
			
			<cfif Len(Trim(PostingAmount)) AND PostingAmount GT 0 AND ListFind("165,301,455,690", TypeId)>
				
				<cfif ListFind("165,301", TypeId)>
					<cfset entryTypeId = 1>
				<cfelse>
					<cfset entryTypeId = 2>
				</cfif>			
				
				<cfset temp = daoObj.InsertBankTransactions(storeId,TypeId,PostingDate,entryTypeId,PostingAmount)>
				
			</cfif>
			
		</cfif>
		
	</cfloop>
	
		<cfcatch>
			<cfdump var="#cfcatch#">
		</cfcatch>
	
	</cftry>
</cfif>