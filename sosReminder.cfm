﻿	<cfset yesterday = dateAdd("d",-1,Now())>
	<cfset yesterday = dateFormat(yesterday,application.eonDateFormat)>
	
	<cfset dayNum = dayOfWeek(Now())>
	
	<cfif dayNum EQ 1>
		<cfset dayNum = 8>
	<cfelseif dayNum EQ 2>
		<cfset dayNum = 9>
	</cfif>
	
	<cfset mondayNum = (dayNum-2)*-1>
	<cfset mondayDate = dateAdd("d",mondayNum,Now())>
	<cfset mondayDate = dateFormat(mondayDate,application.eonDateFormat)>
 
<cfloop query="application.getAllUnits">
	
	<cfset NoSosDates = arrayNew(1)>
	
	<cfset variables.storeId = application.getAllUnits.id>
	<cfset variables.storeName = application.getAllUnits.unitName>
	<cfset variables.storeZone = application.getAllUnits.unitZoneId>
	 
	<cfquery datasource="#application.eonDsn#" name="getSos">
		SELECT businessDate
		FROM storeNightlyData
		WHERE businessDate BETWEEN '#mondayDate#' AND '#yesterday#'
		AND businessUnitId = '#variables.storeId#'	
		ORDER BY businessDate
	</cfquery>
	
	<cfloop index="theDate"	from="#mondayDate#"	to="#yesterday#" step="#CreateTimeSpan( 1, 0, 0, 0 )#">
		<cfset sosVerifyDate = dateFormat(theDate,application.eonDateFormat)>
		
		<cfquery dbtype="query" name="findSos">
			SELECT businessDate FROM getSos
			WHERE businessDate = '#sosVerifyDate#'
		</cfquery>
		
		<cfif NOT findSos.RecordCount>
			<cfset arrayAppend(NoSosDates,sosVerifyDate)>
		</cfif>
		
	</cfloop>


	<cfif arraylen(NoSosDates)>
		
		<cfif variables.storeZone NEQ 2>
			<cfset mailAttributes = {
				from="Eon Reports <do-not-reply@eatoutnow.net>",
				to="#variables.storeName#popeyes@eatoutnow.net",
				cc="brad.lau@eatoutnow.net",
				bcc="kenny@eatoutnow.net",
				subject="Nightly Data Not Received",
				priority="1"
			}
			/>
		<cfelse>
			<cfset mailAttributes = {
				from="Eon Reports <do-not-reply@eatoutnow.net>",
				to="#variables.storeName#popeyes@eatoutnow.net",
				cc="sami.hynek@eatoutnow.net",
				bcc="kenny@eatoutnow.net",
				subject="Nightly Data Not Received",
				priority="1"
			}
			/>
		</cfif>
				
		<cfmail attributeCollection="#mailAttributes#" type="html">
			<b>Reminder for store: #variables.storeName#</b><br><br>
			Nightly Data has not been submitted duly in this week.<br>
			Please submit all your Nightly Data into the EON Portal for the following date(s):<br>
			<cfloop array="#NoSosDates#" index="i">
			<b>#i#</b><br>
			</cfloop>
		</cfmail>
	
	</cfif>

</cfloop>