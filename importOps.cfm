<cffile action = "read" file = "#ExpandPath('./')#\mas90\ops.CSV" variable = "Report" charset="ISO-8859-1">

<cfset myObj = createObject("component", "cfc.utils")>
<cfset daoObj = application.daoObj>

<!--- Convert the CSV to an array of arrays.--->
<cfset arrCSV = myObj.CSVToArray(
	CSVData = Report,
	Delimiter = ",",
	Qualifier = """"
	) />
	
<cfquery name="getBankCodes" datasource="#application.eonDsn#">
	SELECT [account_code]
		  ,[bank_code]
	      ,[account_descr]
	      ,[account_num]
	FROM [bankCodes]
</cfquery>

<cftry>

	<cftransaction>

		<cfloop from="2" to="#arrayLen(arrCSV)#" index="i">
			<cfset dataArr = arrCSV[i]>
			
			<cfset transDate = Trim(dataArr[1])>
			<cfset transDate = dateFormat(transDate,application.eonDateFormat)>
			<cfset amount = 0>					
			<cfset entryType = Trim(dataArr[5])>
			
<!---Transfers From Store Accounts--->			
			<cfif entryType EQ "CR" AND FindNoCase("Trsf from",dataArr[3]) GT 0>				

				<cfset storeId = Trim(dataArr[3])>
				<cfset storeId = Trim(ListGetAt(storeId,3," " ))>
			
				<cfset amount = Trim(dataArr[4])>
				
				<cfif NOT IsNumeric(storeId) OR Len(storeId) NEQ 4>
					<cfthrow message="Store Id invalid on row number:  #i#. Delete that line to continue.">
				</cfif>
				
				<cfif amount gt 0>
					<cfset amount = DecimalFormat(amount)>
					<cfset amount = LSParseNumber(amount)>
				</cfif>
				
				<!---Make Debit Side Entry--->
				<cfset code = "1100-0000">
				<cfset note = "ACH TRANSFER TO OPS">
				<cfset transType = 1>
				
				<cfif amount gt 0>
					<cfset application.daoObj.Insertmas90Entry(code,note,transDate,amount,transType,'JE','misc')>
				</cfif>
				
				<!---Make Credit Side Entry--->
				<cfset code = "1101-#storeId#">
				<cfset transType = 2>
				
				<cfif amount gt 0>
					<cfset application.daoObj.Insertmas90Entry(code,note,transDate,amount,transType,'JE','misc')>
				</cfif>

				
<!---Transfers To ZBAs--->
			<cfelseif entryType EQ "DR" AND FindNoCase("TRANSFER TO DDA",dataArr[3]) GT 0>
						
				<cfset bankAcc = Trim(dataArr[3])>
				<cfset accPos = Find("1177",bankAcc)>
				<cfif accPos GT 0>
					<cfset bankAcc = Mid(bankAcc,accPos,8)>
					<cfif NOT IsNumeric(bankAcc) OR Len(bankAcc) NEQ 8>
						<cfthrow message="Bank Account Number invalid on row number:  #i#. Delete that line to continue.">
					</cfif>
				<cfelse>
					<cfthrow message="Bank Account Number not found on row number:  #i#. Delete that line to continue.">
				</cfif>
				
				<cfset amount = Trim(dataArr[4])>
					
				<cfquery name="getCode" dbtype="query">
					SELECT account_code, account_descr 
					FROM getBankCodes
					WHERE account_num = #bankAcc#
				</cfquery>

				<cfif getCode.recordcount>					
					<cfset code = getCode.account_code>
					
					<!---Make Debit Side Entry--->
					<cfset note = "OPS TO #getCode.account_descr#">
					<cfset transType = 1>
					
					<cfif amount gt 0>
						<cfset application.daoObj.Insertmas90Entry(code,note,transDate,amount,transType,'JE','misc')>
					</cfif>
					
					<!---Make Credit Side Entry--->
					<cfset code = "1100-0000">
					<cfset transType = 2>
					
					<cfif amount gt 0>
						<cfset application.daoObj.Insertmas90Entry(code,note,transDate,amount,transType,'JE','misc')>
					</cfif>
				<cfelse>
					<cfthrow message="Bank Code not in Db. See row number:  #i#. Delete that line to continue.">
				</cfif>				
					
			</cfif>

		</cfloop>
		
	</cftransaction>
	
	<cfcatch type="any">
		Import Failed	
	<cfdump var="#cfcatch#. Row number: #i#">
	</cfcatch>
</cftry>
		
