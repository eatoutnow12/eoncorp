﻿<cfparam name="form.userName" default="">
<cfparam name="form.password" default="">

<cfset variables.userName = Trim(form.userName)>
<cfset variables.password = Trim(form.password)>

<cfset variables.error = "">

<cfif NOT Len(Trim(variables.userName)) OR NOT Len(Trim(variables.password))>
	<cfset variables.error = "Please enter a valid username and password">
</cfif>

<cfoutput>
	
	<cfif Len(Trim(variables.error))>
		<cflocation url="login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">
	</cfif>	
	
	<cfquery name="getUser" datasource="#application.eonDSn#">
		SELECT [id],[userFName],[userLName], [userTypeId]
		FROM [users]
		WHERE userEmail = <cfqueryparam cfsqltype="cf_sql_varchar" value="#variables.userName#">
		AND userPword = <cfqueryparam cfsqltype="cf_sql_varchar" value="#variables.password#">
	</cfquery>
	
	
	<cfif getUser.RecordCount>
		<cfset session.userId = getUser.id>
		<cfset session.firstName = getUser.userFName>
		<cfset session.lastName = getUser.userLName>
		<cfset session.userTypeId = getUser.userTypeId>
		
		<cfquery name="updateLogin" datasource="#application.eonDsn#">
			UPDATE [users]
			SET dateLastLogin = getdate()
			WHERE id = '#session.userId#'		
		</cfquery>
		
		<cfif NOT Len(Trim(variables.error))>
			<cfif session.userTypeId EQ 1>
				<cflocation url="index.cfm" addtoken="false">
			<cfelseif NOT isdefined("cookie.storeId") OR NOT len(trim(cookie.storeId))>
				<cfset variables.error = "This store/computer is currently not setup for use.<br>Please contact the IT department by calling 402-408-6351.">
				<cflocation url="login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">
			<cfelse>
				<cflocation url="index.cfm" addtoken="false">
			</cfif>
		<cfelse>
			<cflocation url="login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">
		</cfif>
		
	<cfelse>
	
		<cfset variables.error = "Email address / password did not match. Please try again.">
		<cflocation url="login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">
	
	</cfif>
</cfoutput>