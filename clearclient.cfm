<a href="index.cfm"><img src="images/login_images/back.gif" border="0"></a>
<br><br>

<h2>Client Variables:</h2>

<cfset clientNames = GetClientVariablesList()>
<cfoutput>
	<cfloop list="#clientNames#" index="clientVarName">
		#clientVarName# = #evaluate("Client.#clientVarName#")#<br>
	</cfloop>


<br><br>

<!--- Kill Client Scope --->
<cfloop list="#clientNames#" index="clientVarName">
	<cfif Left(clientVarName, 3) EQ "GMI" OR LCase(clientVarName) EQ "iso">
		<cfset temp = DeleteClientVariable(clientVarName)>
	</cfif>
</cfloop>	

<h2>Client Variables After Clear:</h2>
<cfset clientNames = GetClientVariablesList()>
	<cfloop list="#clientNames#" index="clientVarName">
		#clientVarName# = #evaluate("Client.#clientVarName#")#<br>
	</cfloop>
</cfoutput>