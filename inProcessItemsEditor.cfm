<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="6">

<div style="margin:10px"><h3>In-process Items Editor</h3><br>
<cfif IsDefined("form.gridEntered") is True>
    <cfgridupdate grid = "FirstGrid" dataSource = "#application.eonDsn#" Keyonly="true"
        tableName = "storeInProcessItems">
 </cfif>

<!--- Query the database to fill up the grid. --->
<cfquery name = "GetItems" dataSource = "#application.eonDsn#">
	SELECT [id] 
		,[itemNumber]     
      ,[itemDescription]
      ,[storeCategoryId]
	  ,[itemStatus]
	  ,[itemUnitId]
	FROM [storeInProcessItems]
  	ORDER BY itemStatus desc, itemNumber
</cfquery>

<cfquery name="getCats" datasource="#application.eonDsn#">
	SELECT [itemCategoryId]
	      ,[itemCategoryLabel]
	  FROM [storeItemCategories]
	  ORDER BY itemCategoryId
</cfquery>
<cfquery name="getUnits" datasource="#application.eonDsn#">
	SELECT [itemUnitId]
	      ,[itemUnitLabel]
	  FROM [storeItemUnits]
	  ORDER BY itemUnitId
</cfquery>

<cfset cat_values = valueList(getCats.itemCategoryId)>

<cfset cat_lables = valueList(getCats.itemCategoryLabel)>

<cfset stat_values = "1,0">
<cfset stat_lables = "Show,Hide">

<cfset unit_values = valueList(getUnits.itemUnitId)>
<cfset unit_lables = valueList(getUnits.itemUnitLabel)>

<cfform>
<cfgrid name = "FirstGrid" width = "1200" height="600" 
    query = "GetItems" 
    font = "Verdana" rowHeaders = "Yes" autowidth="yes" 
    colHeaderBold = "Yes"
    selectMode = "EDIT">
    <cfgridcolumn name = "id" select="false">
	<cfgridcolumn name = "itemNumber" select="false">
	<cfgridcolumn name = "itemDescription" select="false">
	<cfgridcolumn name = "itemUnitId" select="true" header="Unit" bgcolor="##99B1C9" bold="true" values="#unit_values#" valuesdisplay="#unit_lables#">	
	<cfgridcolumn name = "storeCategoryId" select="true" header="Category/Location" bgcolor="##FFFF99" width="1" bold="true" values="#cat_values#" valuesdisplay="#cat_lables#">
	<cfgridcolumn name = "itemStatus" select="true" header="Show/Hide Status" bgcolor="##91C571" width="1" bold="true" values="#stat_values#" valuesdisplay="#stat_lables#">	
	
</cfgrid><br><br>
<cfinput type="submit" name="gridEntered" value="Save Changes">
</cfform>
</div>

<cfinclude template="footer.cfm">