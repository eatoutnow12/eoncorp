﻿<cfset startDate = dateFormat(dateAdd("d", now(), -7), "mm/dd/yyyy")>

<cfquery name="getBUnits" datasource="#application.eonDsn#">
	SELECT [businessUnit].[id] FROM [businessUnit] ORDER BY [unitName]
</cfquery>
<cfset NewLine = Chr(13) & Chr(10)>
<cfset popeyesString = "">
<cfset fileString = "">

<cfoutput>

<cfloop query="getBUnits">	
	<cfquery name="getDSR" datasource="#application.eonDsn#">
		DECLARE @StartDate DATETIME;
		SET @StartDate = '#startDate#';
		DECLARE @EndDate DATETIME;
		SET @EndDate = DATEADD(day, 6, @StartDate);
		PRINT @EndDate
		SELECT [businessUnit].[unitName], CONVERT(INT, ROUND(FLOOR(SUM([dailySales].[credit])), 0)) AS NetSales, 
		(SELECT SUM([dailyGeneralStoreData].[customerCount]) FROM [dailyGeneralStoreData] WHERE 
		[dailyGeneralStoreData].[dateOccured] BETWEEN @StartDate AND @EndDate
		AND [dailyGeneralStoreData].[businessUnitId] = '#getBUnits.id#') AS CustomerCount, 
		(SELECT SUM([dailyGeneralStoreData].[laborDollar]) FROM [dailyGeneralStoreData] WHERE 
		[dailyGeneralStoreData].[dateOccured] BETWEEN @StartDate AND @EndDate
		AND [dailyGeneralStoreData].[businessUnitId] = '#getBUnits.id#') AS LaborDollar
		FROM [dailySales]
		INNER JOIN [businessUnit]
		ON [dbo].[dailySales].[businessUnitId] = [dbo].[businessUnit].[id]
		WHERE [dailySales].[note] = 'Net Food Sales' 
		AND dailySales.[dateOfSale] BETWEEN @StartDate AND @EndDate
		AND [businessUnitId] = '#getBUnits.id#' 
		GROUP BY [businessUnit].[unitName]
	</cfquery>
	
	<cfset laborPercent = DecimalFormat(((getDSR.LaborDollar/getDSR.NetSales)*100)) & "%">
	
	<cfif getBUnits.CurrentRow NEQ getBUnits.RecordCount>
		<cfset fileString = fileString & Trim(getDSR.unitName) & ',' & Trim(getDSR.NetSales) & ',' & Trim(getDSR.customerCount) & ',' & Trim(laborPercent) & NewLine>
	<cfelse>
		<cfset fileString = fileString & Trim(getDSR.unitName) & ',' & Trim(getDSR.NetSales) & ',' & Trim(getDSR.customerCount) & ',' & Trim(laborPercent)>
	</cfif>
	
	<cfif getBUnits.CurrentRow NEQ getBUnits.RecordCount>
		<cfset popeyesString = popeyesString & Trim(getDSR.unitName) & ',' & Trim(getDSR.NetSales) & ',' & Trim(getDSR.customerCount) & NewLine>
	<cfelse>
		<cfset popeyesString = popeyesString & Trim(getDSR.unitName) & ',' & Trim(getDSR.NetSales) & ',' & Trim(getDSR.customerCount)>
	</cfif>
	
	<cfset laborPercent = "">
	
</cfloop>


		<cffile action="write" file="#ExpandPath('./')#\logs\AfcExport.csv" output="#fileString#">

		<cfset mailAttributes = {
			server="smtp.gmail.com",
			username="do-not-reply@eatoutnow.net",
			password="eonsystem",
			from="do-not-reply@eatoutnow.net",
			to="nikhil@eatoutnow.net",
			cc="kenny@eatoutnow.net",
			subject="Weekly AFC Report"
		}
		/>
		
		<cfmail port="465" useSSL="true" attributeCollection="#mailAttributes#" type="text" mimeattach="#ExpandPath('./')#\logs\AfcExport.csv">
			#popeyesString#
		</cfmail>


</cfoutput>
