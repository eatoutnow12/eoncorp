<CFAPPLICATION name = "EONCorp"
    SETCLIENTCOOKIES="Yes" 
    SESSIONMANAGEMENT="Yes" 
	applicationtimeout="#CreateTimeSpan(2,0,0,0)#"
	SESSIONTIMEOUT="#CreateTimeSpan(0,8,0,0)#">

	<CFSET gmt = gettimezoneinfo()>
	<CFSET gmt = gmt.utcHourOffset>
	<CFIF gmt EQ 0>
	   <CFSET gmt = "">
	<CFELSEIF gmt GT 0>
	   <CFSET gmt = "+" & gmt >
	</CFIF>
	
	<CFHEADER NAME="Pragma" VALUE="no-cache">
	<CFHEADER NAME="Cache-Control" VALUE="no-cache, must-revalidate">
	<CFHEADER NAME="Last-Modified" VALUE="#DateFormat(now(), 'ddd, dd mmm yyyy')# #TimeFormat(now(), 'HH:mm:ss')# GMT#gmt#">
	<CFHEADER NAME="Expires" VALUE="Mon, 26 Jul 1997 05:00:00 GMT">
	<cfset application.eonProcData = "eonProcData">	
	
	<cfif NOT structKeyExists(application,"eonMonitorTestMode")>
		<!---Update Store Access Record with current date and time and set status to 0.--->
		<cfquery name="updateStatus" datasource="#application.eonProcData#">
			UPDATE [storeSystemMonitor]
		    SET [lastConnection] = GETDATE()
		        ,[notified] = 0
		</cfquery>
	</cfif>

	<!--- uncomment to reset application vars --->
	<cfset application.isLoaded = false>
	<cfparam name="application.dsrNoError" default="0">
	<cfparam name="application.eonMonitorTestMode" default="0">
	
	<!--- SET APP VARS --->
	<cfif NOT structKeyExists(application,"eonDsn")>
		<cflock scope="APPLICATION" type="EXCLUSIVE" timeout="10">
			<cfset application.appDao = StructNew()>
			<cfset application.eonDsn = "eonCorp">
			<cfset application.eonFinDsn = "eonFin">
			<cfset application.eonDateFormat = "mm/dd/yyyy">
			<cfset application.MicrosDateFormat = "yyyy/mm/dd">
			<cfset application.daoObj = createObject("component","cfc.dsrDAO")>
			<cfset application.utilObj = createObject("component","cfc.Utils")>
			
			<cfset application.appDao.storeMenuMixItems = new cfc.eondao("storeMenuMixItems")>
			<cfset application.appDao.storeMenuMixSales = new cfc.eondao("storeMenuMixSales")>
			
			<cfset application.baseURL = "http://corp.eatoutnow.net/">
			<cfset application.cashLevel = 800>
		</cflock>
	</cfif>
	
	<cfif NOT structKeyExists(application,"getAllUnits")>
		<cfquery name="getAllUnits" datasource="#application.eonDsn#">
			SELECT [businessUnit].[id], [businessUnit].[unitName],
			[businessUnit].[unitIdLP],[businessUnit].[unitIdSysco],[businessUnit].[unitZoneId],[businessUnit].[microsStoreId]
			FROM [businessUnit]
			WHERE inactiveSinceDate IS NULL
			ORDER BY [unitName]
		</cfquery>
		<cfset application.getAllUnits = getAllUnits>	
	</cfif>
	
	<cfif NOT structKeyExists(application,"eonYear")>
		<cfquery name="getCurrentCalendar" datasource="#application.eonDsn#">
			SELECT year, StartDate from businessCalendar
			where year = '#Year(Now())#'
		</cfquery>
		<cfset application.eonYear = getCurrentCalendar.year>
		<cfset application.eonYearStartDate = getCurrentCalendar.StartDate>
	<cfelseif application.eonYear NEQ year(now())>
		<cfquery name="getCurrentCalendar" datasource="#application.eonDsn#">
			SELECT year, StartDate from businessCalendar
			where year = '#Year(Now())#'
		</cfquery>
		<cfset application.eonYear = getCurrentCalendar.year>
		<cfset application.eonYearStartDate = getCurrentCalendar.StartDate>
	</cfif>
	
	<cfset application.isLoaded = true>