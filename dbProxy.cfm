﻿<cfparam name="form.action" default="" />
<cfparam name="form.id" default="" />
<cfparam name="form.statusId" default="" /> 
<cfparam name="form.item" default="0" /> 
<cfparam name="form.itemQty" default="" />
<cfparam name="form.toStoreId" default="" />
<cfparam name="form.fromStoreId" default="" />
<cfparam name="form.transferDate" default="" />
<cfparam name="form.userId" default="" />
<cfparam name="form.transferId" default="" />
<cfparam name="form.inProcess" default="0" /> 
<cfparam name="form.priceType" default="0" /> 


<cfset variables.itemNumber = Trim(form.item)>
<cfset variables.itemQty = Trim(form.itemQty)>
<cfset variables.toStoreId = Trim(form.toStoreId)>
<cfset variables.fromStoreId = Trim(form.fromStoreId)>
<cfset variables.transferDate = Trim(form.transferDate)>
<cfset variables.userId = Trim(form.userId)>
<cfset variables.transferId = Trim(form.transferId)>
<cfset variables.inProcess = Trim(form.inProcess)>
<cfset variables.priceType = Trim(form.priceType)>

<cfswitch expression="#form.action#">
	<cfcase value="setDSRStatus">
		<cfif Len(form.id)>		
			<cfif form.statusId EQ 0>
				<cfif session.userId EQ "9e6deda4-36b9-432c-aacb-e4cd6933ec3c">
					<cfset variables.newStatus = 2>
				<cfelse>
					<cfset variables.newStatus = 1>
				</cfif>
			<cfelse>
				<cfset variables.newStatus = 0>
			</cfif>
			
			
			<cfquery name="updateStatus" datasource="#application.eonDsn#">
				UPDATE dailySales 
				SET status = '#variables.newStatus#'
				WHERE id = '#form.id#'
			</cfquery>
		</cfif>
	</cfcase>
	<cfcase value="getItemData">
			<cfsetting enablecfoutputonly="yes">
			<cfset itemPrice = 0>
			<cfset itemUnit = "">
			
			<cfif variables.inProcess EQ 0>
				<cfquery name="getItem" datasource="#application.eonDsn#">
					SELECT dbo.GetItemPriceByItemNumber('#variables.itemNumber#') AS itemPrice
				</cfquery>
			<cfelse>
				<cfquery name="getItem" datasource="#application.eonDsn#">
					SELECT dbo.GetUnitPriceByItemNumber('#variables.itemNumber#','#variables.inProcess#') AS itemPrice
				</cfquery>
			</cfif>
			
			<cfif getItem.RecordCount>
				<cfset itemPrice = decimalformat(getItem.itemPrice)>
			</cfif>
			
			<cfquery name="getItemUnit" datasource="#application.eonDsn#">
				SELECT dbo.storeItemUnits.itemUnitLabel
				FROM dbo.purchaseItems
				INNER JOIN dbo.storeItemUnits
				ON dbo.purchaseItems.itemUnitId = dbo.storeItemUnits.itemUnitId
				WHERE itemNumber = '#variables.itemNumber#'
			</cfquery>
			
			<cfif getItemUnit.RecordCount>
				<cfset itemUnit = getItemUnit.itemUnitLabel>
			</cfif>
			
			<cfoutput>
			#itemPrice#:#itemUnit#
			</cfoutput>
	</cfcase>
	
	<cfcase value="getItemPrice">
			<cfsetting enablecfoutputonly="yes">
			<cfset itemPrice = 0>
			
			<cfif variables.priceType EQ 0>
				<cfquery name="getItem" datasource="#application.eonDsn#">
					SELECT dbo.GetItemPriceByItemNumber('#variables.itemNumber#') AS itemPrice
				</cfquery>
			<cfelse>
				<cfquery name="getItem" datasource="#application.eonDsn#">
					SELECT dbo.GetUnitPriceByItemNumber('#variables.itemNumber#','#variables.inProcess#') AS itemPrice
				</cfquery>
			</cfif>
			
			<cfif getItem.RecordCount>
				<cfset itemPrice = decimalformat(getItem.itemPrice)>
			</cfif>
			
			<cfoutput>
			#itemPrice#
			</cfoutput>
	</cfcase>
	
	<cfcase value="InsertInventoryTransfer">
		<cfset application.daoObj.InsertInventoryTransfer('#variables.transferDate#','#variables.fromStoreId#','#variables.toStoreId#','#variables.itemNumber#','#variables.itemQty#','#variables.userId#','#variables.inProcess#','#variables.priceType#')>
	</cfcase>
	
	<cfcase value="AcceptInventoryTransfer">
		<cfquery name="UpdateInventoryTransfer" datasource="#application.eonDsn#">
			UPDATE storeInventoryTransfers
			SET acceptedByUser = '#variables.userId#',
				dateAccepted = getdate()
			WHERE id = '#variables.transferId#'
		</cfquery>
	</cfcase>
	
	<cfcase value="DeleteInventoryTransfer">
		<cfquery name="UpdateInventoryTransfer" datasource="#application.eonDsn#">
			UPDATE storeInventoryTransfers
			SET deletedByUser = '#variables.userId#',
				dateDeleted = getdate()
			WHERE id = '#variables.transferId#'
		</cfquery>
	</cfcase>
</cfswitch>