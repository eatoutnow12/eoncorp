﻿<cfset variables.yesterday = dateAdd("d",-1,Now())>
<cfparam name="attributes.saleDate" default="#variables.yesterday#">
<cfparam name="attributes.microsStoreId" default="0">
<cfparam name="attributes.result" default="result">

<cfset salesDataStruct = structNew()>

<cfset salesDataStruct.entryDate = dateformat(attributes.saleDate,application.MicrosDateFormat)>

<cfset variables.dayString = DayOfWeekAsString(DayOfWeek(salesDataStruct.entryDate))>
	<cfset salesDataStruct.microsStoreId = attributes.microsStoreId>
	<cfquery name="getStore" dbtype="query">
		SELECT id
		from application.getAllUnits
		where microsStoreId = #salesDataStruct.microsStoreId#
	</cfquery>
	
	<cfset salesDataStruct.businessUnitId = getStore.id>


<!---Net Sales $--->
	<cfset variables.dataType = "Net Sales">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(net_sls_ttl) as amount 
		from micros.dly_sys_ttl 
		where 
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.netSalesDollar = 0>		
	<cfelse>
		<cfset salesDataStruct.netSalesDollar = LSParseNumber(getData.amount)>	
	</cfif>						
	

<!---Sales Tax $--->						
	<cfset variables.dataType = "Sales Tax">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(tax_coll_ttl) as amount 
		from micros.dly_sys_ttl 
		where 
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.salesTaxDollar = 0>	
	<cfelse>
		<cfset salesDataStruct.salesTaxDollar = LSParseNumber(getData.amount)>	
	</cfif>	

<!---Paid outs $--->						
	<cfset variables.dataType = "Paid Outs">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_25) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.paidOutsDollar = 0>		
	<cfelse>
		<cfset salesDataStruct.paidOutsDollar = LSParseNumber(getData.amount)>	
	</cfif>	

<!---GC Redeemed $--->						
	<cfset variables.dataType = "GC Redeemed">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_13) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.gcRedeemedDollar = 0>	
	<cfelse>
		<cfset salesDataStruct.gcRedeemedDollar = LSParseNumber(getData.amount)>	
	</cfif>	


<!---A/R $--->						
	<cfset variables.dataType = "AR">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_12) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.ArDollar = 0>	
	<cfelse>
		<cfset salesDataStruct.ArDollar = LSParseNumber(getData.amount)>	
	</cfif>	

<!---Visa/MC $--->						
	<cfset variables.dataType = "Visa/MC">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_17 + trk_ttl_18 + trk_ttl_19) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.VisaMcDollar = 0>	
	<cfelse>
		<cfset salesDataStruct.VisaMcDollar = LSParseNumber(getData.amount)>	
	</cfif>	


<!---Amex $--->						
	<cfset variables.dataType = "Amex">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_20) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.AmexDollar = 0>		
	<cfelse>
		<cfset salesDataStruct.AmexDollar = LSParseNumber(getData.amount)>	
	</cfif>	


<!---AM Deposit $--->	
	<cfset variables.dataType = "AM Deposit">

<cfquery name="getData" datasource="MicrosEM">
	select sum(micros.em_cm_transaction_dtl.base_item_amt) as amount
	From 
	micros.em_cm_transaction_dtl,
	micros.em_cm_receptacle_dtl 
	where
	micros.em_cm_transaction_dtl.transaction_receptacle_seq = micros.em_cm_receptacle_dtl.receptacle_seq And
	micros.em_cm_transaction_dtl.base_item_amt > 0 AND 
	micros.em_cm_receptacle_dtl.receptacle_name not like '%template' AND
	micros.em_cm_transaction_dtl.count_item_seq Is Not Null AND
	micros.em_cm_transaction_dtl.store_id = #attributes.microsStoreId# 
	and
	micros.em_cm_transaction_dtl.business_date = '#salesDataStruct.entryDate#'
	and micros.em_cm_receptacle_dtl.receptacle_name = '#variables.dayString# AM'
</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.AMdepositsDollar = 0>		
	<cfelse>
		<cfset salesDataStruct.AMdepositsDollar = LSParseNumber(getData.amount)>	
	</cfif>						

<!---PM Deposit $--->		
	<cfset variables.dataType = "PM Deposit">				
<cfquery name="getData" datasource="MicrosEM">
	select sum(micros.em_cm_transaction_dtl.base_item_amt) as amount
	From 
	micros.em_cm_transaction_dtl,
	micros.em_cm_receptacle_dtl 
	where
	micros.em_cm_transaction_dtl.transaction_receptacle_seq = micros.em_cm_receptacle_dtl.receptacle_seq And
	micros.em_cm_transaction_dtl.base_item_amt > 0 AND 
	micros.em_cm_receptacle_dtl.receptacle_name not like '%template' AND
	micros.em_cm_transaction_dtl.count_item_seq Is Not Null AND
	micros.em_cm_transaction_dtl.store_id = #attributes.microsStoreId# 
	and
	micros.em_cm_transaction_dtl.business_date = '#salesDataStruct.entryDate#'
	and micros.em_cm_receptacle_dtl.receptacle_name = '#variables.dayString# PM'
</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.PMdepositsDollar = 0>		
	<cfelse>
		<cfset salesDataStruct.PMdepositsDollar = LSParseNumber(getData.amount)>	
	</cfif>	
	
<!---Coupon $--->						
	<cfset variables.dataType = "Coupon">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_48) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.couponDollar = 0>	
	<cfelse>
		<cfset salesDataStruct.couponDollar = LSParseNumber(getData.amount)>	
		<cfset salesDataStruct.couponDollar = Abs(salesDataStruct.couponDollar)>
	</cfif>	
	
	
<!---Promo $--->						
	<cfset variables.dataType = "Promo">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_07) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.promosDollar = 0>	
	<cfelse>
		<cfset salesDataStruct.promosDollar = LSParseNumber(getData.amount)>	
		<cfset salesDataStruct.promosDollar = Abs(salesDataStruct.promosDollar)>
	</cfif>	

						

<!---Gift Certs Sold--->						
	<cfset variables.dataType = "GC Sold">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_24) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.GcSold = 0>		
	<cfelse>
		<cfset salesDataStruct.GcSold = LSParseNumber(getData.amount)>	
	</cfif>	

<!---Gift Certs Paid out--->						
	<cfset variables.dataType = "GC PaidOut">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_ttl_27) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.GcPaidOut = 0>		
	<cfelse>
		<cfset salesDataStruct.GcPaidOut = LSParseNumber(getData.amount)>	
	</cfif>	

	<!---Coupons Num--->
	<cfset variables.dataType = "Coupons Num">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_cnt_48) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.CouponsNum = 0>			
	<cfelse>
		<cfset salesDataStruct.CouponsNum = LSParseNumber(getData.amount)>	
	</cfif>	

	
	<!---Promos Num--->						
	<cfset variables.dataType = "Promos Num">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(trk_cnt_07) as amount 
		from micros.dly_sys_trk_ttl 
		where 
		trk_grp_seq = 2 
		and
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.PromosNum = 0>		
	<cfelse>
		<cfset salesDataStruct.PromosNum = LSParseNumber(getData.amount)>	
	</cfif>	
	
	<!---Deletes Num--->						
	<cfset variables.dataType = "Deletes Num">
	
	<cfquery name="getData" datasource="MicrosEM">
		select sum(void_ttl) as amount 
		from micros.dly_sys_ttl 
		where 
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.DeletesNum = 0>		
	<cfelse>
		<cfset salesDataStruct.DeletesNum = Abs(getData.amount)>
		<cfset salesDataStruct.DeletesNum = LSParseNumber(salesDataStruct.DeletesNum)>
	</cfif>
	

	
	<!---Customer Count --->						
	<cfset variables.dataType = "Customer Count">
	
	<cfquery name="getData" datasource="MicrosEM">
		select chk_paid_cnt as amount
		from micros.dly_sys_ttl 
		where 
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.CustomerCount = 0>	
	<cfelse>
		<cfset salesDataStruct.CustomerCount = LSParseNumber(getData.amount)>	
	</cfif>	

	<!--- Labor --->						
	<cfset variables.dataType = "Labor">
	
	<cfquery name="getData" datasource="MicrosEM">
		select ((labor_cat_1_reg_ttl)+(labor_cat_1_ovt_ttl)+(labor_cat_3_reg_ttl)+(labor_cat_3_ovt_ttl)) as amount
		from micros.dly_sys_ttl 
		where 
		business_date = '#salesDataStruct.entryDate#' 
		and 
		store_id = #attributes.microsStoreId#
	</cfquery>
	
	<cfif NOT IsNumeric(getData.amount)>
		<cfset salesDataStruct.laborNum = 0>	
	<cfelse>
		<cfset salesDataStruct.laborNum = LSParseNumber(getData.amount)>	
	</cfif>	

	<cfset caller["salesDataStruct"] = salesDataStruct>
<cfdump var="#salesDataStruct#">