﻿<cfparam name="url.id" default="">
<cfparam name="form.id" default="">

<cfset variables.valType = 0>
	<cfquery name="getEditVals" datasource="#application.eonDsn#">
		SELECT debit, credit, modifyComment from dailySales where id = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.id#">
	</cfquery>
	
	<cfif getEditVals.debit GT 0>
		<cfset variables.valType = 1>
	</cfif>
	
	<cfif getEditVals.credit GT 0>
		<cfset variables.valType = 2>
	</cfif> 
<cfif IsDefined("form.id") AND Len(form.id)>

	<cfquery name="setEditVals" datasource="#application.eonDsn#">
		<cfif form.valType EQ 1>
			UPDATE dailySales
			SET debit = '#lsParseNumber(form.newVal)#'
			,modifyComment = '#Trim(form.comment)#'
			,credit = 0			
			WHERE id = '#form.id#'
		<cfelse>
			UPDATE dailySales
			SET credit = '#lsParseNumber(form.newVal)#'
			,modifyComment = '#Trim(form.comment)#'
			,debit = 0
			WHERE id = '#form.id#'
		</cfif>
	</cfquery>
</cfif>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/eon.css">
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>DSR Update</title>
		
		<script>
			function trim(str, chars) {
				return ltrim(rtrim(str, chars), chars);
			}
			 
			function ltrim(str, chars) {
				chars = chars || "\\s";
				return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
			}
			 
			function rtrim(str, chars) {
				chars = chars || "\\s";
				return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
			}			
			
			function SubmitVals() {
				var theForm = document.getElementById('updateDsrFrm');
				if(trim(theForm.comment.value, ' ').length == 0) {
					alert('Please enter a comment for this change.');
					return false;
				}
				else if(!theForm.valType1.checked && !theForm.valType2.checked) {
					alert('Please specify Debit or Credit.');
					return false;
				}				
				else {
					theForm.submit();
					window.opener.document.forms[0].submit();
					self.close();
				}
			}		
		</script>
			
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" /> 
	</head>
	
<body>
<cfoutput>
	<div style="padding:10px">
	
		<form name="updateDsrFrm" id="updateDsrFrm" action="updateDSR.cfm?id=#url.id#" method="post">
			<label for="newVal">Amount $:</label>
			<cfif variables.valType EQ 1>	
				<input type="text" id="newVal" name="newVal" value="#getEditVals.debit#">
			<cfelse>
				<input type="text" id="newVal" name="newVal" value="#getEditVals.credit#">
			</cfif><br>
			<label for="valType1">Debit</label>
			<input type="radio" id="valType1" name="valType" value="1"<cfif variables.valType EQ 1> checked</cfif>>
			
			<label for="valType2">Credit</label>
			<input type="radio" id="valType2" name="valType" value="2"<cfif variables.valType EQ 2> checked</cfif>>
			
			<br><br>
			<label for="comment">Notes:</label><br>
			<textarea rows="10" cols="40" id="comment" name="comment">#Trim(getEditVals.modifyComment)#</textarea>
			<br><br>
			<input type="hidden" value="#url.id#" name="id">
			<input type="button" value="Save" onclick="SubmitVals();">		
		
		</form>
	
	</div>
</cfoutput>
</body>

</html>