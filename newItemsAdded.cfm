﻿	<cfquery name="getNewItems" datasource="#application.eonDsn#">
		SELECT itemNumber, itemDescription 
		FROM purchaseItems 
		WHERE dateAdded > DATEADD(DAY,-1,GETDATE())
	</cfquery>

	<cfif getNewItems.RecordCount>
	
		<cfsavecontent variable="mailData">
			<cfdump var='#getNewItems#' metainfo='false' label='New Lincoln Poultry Items'>
		</cfsavecontent>
	
		<cfset mailAttributes = {
		message="New Inventory Items added:<br> #mailData#",
		sendTo="management@eatoutnow.net",
		sendCc="5273popeyes@eatoutnow.net",
		sentFrom="Inventory Monitor",
		subject="New Inventory Items added to LP Order Guide",
		mailFormat="html"
		}>	
		
		<cfmodule template="sendMail.cfm" attributecollection="#mailAttributes#">	
	
	<cfelse>
		<cfset mailAttributes = {
		message="New Inventory Items report ran. Nothing to report.",
		sendTo="kenny@eatoutnow.net",
		sentFrom="Inventory Monitor",
		subject="No New Inventory Items added",
		mailFormat="html"
		}>	
		
		<cfmodule template="sendMail.cfm" attributecollection="#mailAttributes#">	
	
	</cfif>