	function IsNumeric(ele,customMessage)
	{
		
		var input = ele.value;
		if (input != '') {
						if ((input - 0) == input && input.length > 0) {
							ele.className = "inputNoErr";
						}
						else {
							if (customMessage == undefined) {
								alert('Please enter numbers only for count. You can enter decimals too. \n Enter a zero for blank');
							}
							else {
								alert(customMessage);
							}
							
							setTimeout(function(){
								ele.focus();
							}, 1);
							ele.className = "inputErr";
							ele.select();
						};
					}
	}
	
	function Is_int(ele){ 
		var input = ele.value;
	  if((parseFloat(input) == parseInt(input)) && !isNaN(input)){
	      ele.className = "inputNoErr";
		  return true;
	  } else { 
	      alert('Please enter whole numbers only. Do not enter decimals or fractions');
	  } 
	   setTimeout(function() {ele.focus();}, 1);
	   ele.className = "inputErr";
	   ele.select();
	}

	function checkdate(input){
		var validformat=/^\d{2}\/\d{2}\/\d{4}$/ //Basic check for format validity
		var returnval=false
		if (!validformat.test(input.value))
		alert("Invalid Date Format. Please correct and submit again.")
		else{ //Detailed check for valid date ranges
		var monthfield=input.value.split("/")[0]
		var dayfield=input.value.split("/")[1]
		var yearfield=input.value.split("/")[2]
		var dayobj = new Date(yearfield, monthfield-1, dayfield)
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
		alert("Invalid Day, Month, or Year range detected. Please correct and submit again.")
		else
		returnval=true
		}
		if (returnval==false) input.select()
		return returnval
	}