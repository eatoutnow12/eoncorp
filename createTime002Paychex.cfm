﻿
<cfquery datasource="#application.eonProcData#" name="payRollQry">

SELECT [payrollId]
      ,[empName]
      ,[hours]
      ,[businessUnit]
      ,[eCode]
      , 
	CASE WHEN department = 10 THEN 5000 
	ELSE 5010 
	END AS department
  FROM [payrollExport]
  ORDER BY empName, businessUnit, payrollId
	
</cfquery>
 
 

<cfset arrData = [] />
 

<cfloop query="payRollQry">
 
	<!---
		Create an array to hold all the fields for this row
		of data. In this array, each index of the array will
		represent a single data field.
	--->
	<cfset arrRowData = [] />
 
	<!--- payroll ID. --->
	<cfset arrRowData[ 1 ] = RJustify( Trim(payRollQry.payrollId), 6 ) />
 
	<!--- Emplyee name. --->
	<cfset arrRowData[ 2 ] = RJustify( "", 25 ) />
 
	<!--- Override Department --->
	<cfset arrRowData[ 3 ] = RJustify( Trim(payRollQry.department), 6 ) />
 
	<!--- Job --->
	<cfset arrRowData[ 4 ] = RJustify( "", 12 ) /> 
	
	<!--- Shift --->
	<cfset arrRowData[ 5 ] = RJustify( "", 1 ) />  
	
	<!--- D/E --->
	<cfset arrRowData[ 6 ] = RJustify( "E", 1 ) />  
	
	<!--- Earning or Deduction Code --->
	<cfset arrRowData[ 7 ] = RJustify( Trim(payRollQry.eCode), 2 ) />  	
	
	<!--- Override Rate --->
	<cfset arrRowData[ 8 ] = RJustify( "", 9 ) /> 
	
	<!--- Hours --->
	<cfset arrRowData[ 9 ] = RJustify( Trim(payRollQry.hours), 8 ) /> 	
	
	<!--- Year --->
	<cfset arrRowData[ 10 ] = RJustify( "", 4 ) /> 		
	
	<!--- Month --->
	<cfset arrRowData[ 11 ] = RJustify( "", 2 ) /> 	
		
	<!--- Day --->
	<cfset arrRowData[ 12 ] = RJustify( "", 2 ) />
		
	<!--- Filler --->
	<cfset arrRowData[ 13 ] = RJustify( "", 4 ) />	
		
	<!--- Amount --->
	<cfset arrRowData[ 14 ] = RJustify( "", 9 ) />	
		
	<!--- Sequence Number --->
	<cfset arrRowData[ 15 ] = RJustify( "", 1 ) />	
		
	<!--- Override Division --->
	<cfset arrRowData[ 16 ] = RJustify( "", 6 ) />	
		
	<!--- Override Branch --->
	<cfset arrRowData[ 17 ] = RJustify( Trim(payRollQry.businessUnit), 6 ) />	
			
	<!--- Override State --->
	<cfset arrRowData[ 18 ] = RJustify( "", 2 ) />	
			
	<!--- Override Local --->
	<cfset arrRowData[ 19 ] = RJustify( "", 10 ) />		
		
	<!--- State/Local Misc Field --->
	<cfset arrRowData[ 20 ] = RJustify( "", 1 ) />	
		
	<!--- Rate ## --->
	<cfset arrRowData[ 21 ] = RJustify( "", 1 ) />		
		
	<!--- Social Security ## --->
	<cfset arrRowData[ 22 ] = RJustify( "", 11 ) />	
		
	<!---
		Now that our array is full of the field data, each of
		which is "padded" to be fixed length, we need to add
		it to the master data array. As we do this, however,
		we are gonna collapse this row into a single string
		by converting it into a list that has no delimiter.
	--->
	<cfset ArrayAppend(
		arrData,
		ArrayToList( arrRowData, "" )
		) />
 
</cfloop>
 
 
<!---
	At this point, our master data array has a finalized row of
	data in each index. We now need to collapse the master data
	array down into a single data string. We will do this by
	converting the data array into a list that uses line breaks
	as a delimiter.
--->
<cfset strFinalData = ArrayToList(
	arrData,
	(Chr( 13 ) & Chr( 10 ))
	) />
 
 
<cffile action="write" file="C:\X608_TA.txt" output="#strFinalData#" />
 
<!---
	When we output the data, let's replace the spaces with
	periods so that we can see on the web page how the fields
	were padding using LJustify().
--->
<cfoutput>
	<pre>#Replace( strFinalData, " ", ".", "all" )#</pre>
</cfoutput>