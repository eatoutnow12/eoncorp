<a href="index.cfm"><img src="images/login_images/back.gif" border="0"></a>
<br><br>

<h2>Application Variables:</h2>

<cfdump var="#application#">

<br><br>
	<!--- clear all application variables --->
	<cflock scope="APPLICATION" type="EXCLUSIVE" timeout="10">
		<cfset StructClear(application)>
	</cflock>
	
<br><br>	

<h2>Application Variables After Clear:</h2>

<cfdump var="#application#">