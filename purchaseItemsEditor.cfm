﻿<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="5">

<div style="margin:10px"><h3>LP Items Editor</h3><br>
<cfif IsDefined("form.gridEntered") is True>
    <cfgridupdate grid = "FirstGrid" dataSource = "#application.eonDsn#" Keyonly="true"
        tableName = "purchaseItems">
 </cfif>

<!--- Query the database to fill up the grid. --->
<cfquery name = "GetItems" dataSource = "#application.eonDsn#">
	SELECT [itemNumber]     
      ,[itemDescription]
      ,[itemBrand]
      ,[itemVendor]
      ,[storeCategoryId]
	  ,[itemStatus]
	  ,[itemUnitId]
	  ,[itemsPerPack]
	  ,price = (SELECT TOP 1 itemPrice FROM purchaseInvoices WHERE purchaseInvoices.itemNumber = purchaseItems.itemNumber ORDER BY invoiceDate DESC)
	FROM [purchaseItems]
	WHERE 
	1 = 1
	<!---AND itemNumber IN (SELECT itemNumber FROM dbo.purchaseInvoices WHERE invoiceDate >= DATEADD("m",-6,GETDATE()))--->
	<cfif IsDefined("form.sItemNumber") AND isNumeric(form.sItemNumber)>
	AND itemNumber = '#form.sItemNumber#'
	<cfelseif IsDefined("form.sItemNumber") AND NOT isNumeric(form.sItemNumber)>
	AND itemDescription LIKE '%#form.sItemNumber#%'
	</cfif>
  	ORDER BY itemStatus desc, itemNumber
</cfquery>

<cfquery name="getCats" datasource="#application.eonDsn#">
	SELECT [itemCategoryId]
	      ,[itemCategoryLabel]
	FROM [storeItemCategories]
	ORDER BY itemCategoryId
</cfquery>
<cfquery name="getUnits" datasource="#application.eonDsn#">
	SELECT [itemUnitId]
	      ,[itemUnitLabel]
	FROM [storeItemUnits]
	ORDER BY itemUnitId
</cfquery>

<cfset cat_values = valueList(getCats.itemCategoryId)>

<cfset cat_lables = valueList(getCats.itemCategoryLabel)>

<cfset stat_values = "1,0">
<cfset stat_lables = "Show,Hide">

<cfset unit_values = valueList(getUnits.itemUnitId)>
<cfset unit_lables = valueList(getUnits.itemUnitLabel)>
<form name="form1" action="purchaseItemsEditor.cfm" method="post">
<label for="sItemNumber">Item Number:</label>
<input id="sItemNumber" type="text" name="sItemNumber" value="<cfif IsDefined("form.sItemNumber")><cfoutput>#form.sItemNumber#</cfoutput></cfif>">
<input type="submit" name="submit" value="Search">
<input type="button" value="Clear" onClick="document.getElementById('sItemNumber').value=''" />
</form>
<br>

<cfform>
<cfgrid name = "FirstGrid" width = "1200" height="600" 
    query = "GetItems" 
    font = "Verdana" rowHeaders = "Yes" autowidth="yes" 
    colHeaderBold = "Yes"
    selectMode = "EDIT">
    
	<cfgridcolumn name = "itemNumber" select="false">
	<cfgridcolumn name = "itemDescription" select="false">
	<cfgridcolumn name = "itemBrand" select="false" display="false">
	<cfgridcolumn name = "itemVendor" select="false" display="false">
	<cfgridcolumn name = "itemUnitId" select="true" header="Unit" bgcolor="##99B1C9" bold="true" values="#unit_values#" valuesdisplay="#unit_lables#">	
	<cfgridcolumn name = "storeCategoryId" select="true" header="Category/Location" bgcolor="##FFFF99" width="1" bold="true" values="#cat_values#" valuesdisplay="#cat_lables#">
	<cfgridcolumn name = "itemStatus" select="true" header="Show/Hide Status" bgcolor="##91C571" width="1" bold="true" values="#stat_values#" valuesdisplay="#stat_lables#">	
	<cfgridcolumn name = "itemsPerPack" select="true" header="Unit Count" bgcolor="##FFFF99" width="1" bold="true">
	<cfgridcolumn name = "price" header="InvoicePrice" select="false" numberformat="$____.__">
</cfgrid><br><br>
<cfif IsDefined("form.sItemNumber") AND isNumeric(form.sItemNumber)>
<cfoutput><input id="sItemNumber" type="hidden" name="sItemNumber" value="#form.sItemNumber#"></cfoutput>
</cfif>
<cfinput type="submit" name="gridEntered" value="Save Changes">
</cfform>
</div>

<cfinclude template="footer.cfm">