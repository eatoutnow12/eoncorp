<cfset folderPrefix = DateFormat(Now(),"yyyymmdd")>
<cfset save_path = ExpandPath('./') & "orderGuides\" & folderPrefix & "\">
<cfset daoObj = createObject("component","cfc.dsrDAO")>
<cfset utilsObj = createObject("component","cfc.Utils")>

<cfif NOT directoryExists(save_path)>
	<cfdirectory action="create" directory="#save_path#">
</cfif>

<p>Open a connection
<cfftp connection = "lpConnect" 
   username = "EatOutNow"
   password = "LAchicken!267"
   server = "orders.lincolnpoultry.com"
   action = "open" 
   stopOnError = "Yes" timeout="50000" passive="true"> 

<cfif cfftp.succeeded>
	<cfftp connection = "lpConnect"
	    action = "LISTDIR"
	    stopOnError = "Yes"
	    name = "ListDirs"
	    directory = "/home/customer/EatOutNow/bids" timeout="50000" passive="true">
</cfif>

<cfloop query="ListDirs">
	<cfif NOT isdirectory>
		<cfftp
		action="getfile"
		connection="lpConnect"
		localfile="#save_path##name#"
		remotefile="#path#" 
		failifexists="false"  
		transfermode="auto" 
		timeout="50000" 
		passive="true"
		stoponerror="true"  
		/>
	</cfif>
	
	<cfif cfftp.succeeded>
		<cfftp
		action="remove"
		connection="lpConnect"
		item="#path#" stoponerror="true">
	</cfif>
</cfloop>

<p>Close the connection:
<cfftp 
	connection = "lpConnect"
    action = "close"
    stopOnError = "true">
<p>Did it succeed? <cfoutput>#cfftp.succeeded#</cfoutput>

<cffile action = "read" file = "#save_path#\BA126700.000" variable = "orderGuide" charset="ISO-8859-1">
<cfset lineDelim = Chr(13) & Chr(10)>
<cfset columnDelim = chr(32) & chr(32)>

<cfscript>
	parsedData = ListToArray(orderGuide,lineDelim, false);
</cfscript>

<cfset catList = "">

<cfset newCatFound = false>

<cfloop index="i" from="1" to="#ArrayLen(parsedData)#">
	<cfset parsedArr = ListToArray(parsedData[i], '  ', false, true)>
	<cfset parsedStr = parsedArr[1]>
	<cfset parsedStr = lJustify(parsedStr,Len(parsedStr))>
	
	<cfset itemNumber = mid(parsedStr,1,6)>
	<cfset categoryId = abs(mid(parsedStr,16,5))>
	
	<cfset catList = listAppend(catList,categoryId)>
</cfloop>

<cfset catList = utilsObj.ListDeleteDuplicates(catList)>

<cfloop index="j" list="#catList#">
	<cfset temp = daoObj.InsertpurchaseCategory(j)>
	<cfif temp eq true>
		<cfset newCatFound = true>
	</cfif>
</cfloop>

<cfif newCatFound>
	<cfmodule template="errorReport.cfm" report="New Purchase Categories found, Add Description in db. #catList#">
</cfif>

<cfloop index="i" from="1" to="#ArrayLen(parsedData)#">
	<cfset parsedArr = ListToArray(parsedData[i], '  ', false, true)>
	<cfset parsedStr = parsedArr[1]>
	<cfset parsedStr = lJustify(parsedStr,Len(parsedStr))>
	
	<cfset itemNumber = mid(parsedStr,1,6)>
	<cfset categoryId = abs(mid(parsedStr,16,5))>
	<cfset itemDescription = mid(parsedData[i],40,31)>
	<cfset itemDescription = replaceNoCase(itemDescription,"!","","all")>
	<cfset itemDescription = replaceNoCase(itemDescription,"*","","all")>
	<cfset itemDescription = replaceNoCase(itemDescription,"@","","all")>
	<cfset itemDescription = Trim(itemDescription)>
	<cfset itemBrand = mid(parsedData[i],32,8)>
	
	<cfset catList = listAppend(catList,categoryId)>
	
	<cfset temp = daoObj.InsertpurchaseItem(itemNumber, categoryId,itemDescription,itemBrand)>
</cfloop>