﻿		<cfparam name="attributes.message" default="">
		<cfparam name="attributes.sendTo" default="">
		<cfparam name="attributes.sendCc" default="">
		<cfparam name="attributes.sentFrom" default="">
		<cfparam name="attributes.subject" default="">
		<cfparam name="attributes.priority" default="normal">
		<cfparam name="attributes.mailFormat" default="text">
		
		<cfset mailAttributes = {
			from="#attributes.sentFrom# <reports@eatoutnow.net>",
			to="#attributes.sendTo#",
			subject="#attributes.Subject#",
			priority="#attributes.priority#",
			cc="#attributes.sendCc#"
		}
		/>
		
		<cfmail attributeCollection="#mailAttributes#" type="#attributes.mailFormat#">
			#attributes.message#
		</cfmail>