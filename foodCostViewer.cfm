﻿<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="10">

		<cfset itemTypesList = "FOOD,BEVERAGE,PAPER">
		<cfset itemCodeList = "4600,4601,5720">
		<script type="text/javascript">
			function ValidateSubmit()
			{
				document.forms[0].submit();
			}	
		</script>
		
		<style type="text/css">
			tr.lightrow td {
			background-color: #FFFFFF;
			}
			tr.darkrow td {
			background-color: #EFEFEF;
			}
		</style>
		
		<div style="margin:10px"><h3>Food Cost Viewer</h3><br>
		<form action="foodCostViewer.cfm" method="post">		
			
			<cfif NOT IsDefined("cookie.storeId")>
				<label for="bUnit">Select Store:</label>	
				<select id="bUnit" name="bUnit">
					<cfloop query="application.getAllUnits">
						<cfoutput>
			            	<option id="#Trim(unitName)#" value="#Trim(id)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ id>selected</cfif>>
							#Trim(unitName)#
							</option>
			            </cfoutput>
					</cfloop>
					<option id="0" value="0" <cfif IsDefined("form.bUnit") AND form.bUnit EQ 0>selected</cfif>>All</option>
				</select>&nbsp;&nbsp;&nbsp;&nbsp;
			<cfelse>
				<cfoutput><input type="hidden" name="bUnit" value="#cookie.storeId#"></cfoutput>
			</cfif>
			
			
			<label for="fcYear">Year:</label>
			<select id="fcYear" name="fcYear">
				<cfloop index="i" from="#year(now())#" to="2011" step="-1">
					<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcYear") AND form.fcYear EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
				</cfloop>
			</select>	
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label for="fcPeriod">Period:</label>
			<select id="fcPeriod" name="fcPeriod">
				<cfloop index="i" from="1" to="13">
					<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcPeriod") AND form.fcPeriod EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
				</cfloop>
			</select>
			
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label for="fcWeek">Week:</label>
			<select id="fcWeek" name="fcWeek">					
				<cfloop index="i" from="1" to="4">
					<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcWeek") AND form.fcWeek EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
				</cfloop>
				<option id="0" value="0" <cfif IsDefined("form.fcWeek") AND form.fcWeek EQ 0>selected</cfif>>Period</option>
			</select>

			<input type="button" onclick="ValidateSubmit()" value="View Food Cost">
		
		</form>

			<cfif IsDefined("form.bUnit")>
				
				<cfquery name="getCal" datasource="#application.eonDsn#">
					SELECT startDate 
					FROM businessCalendar
					WHERE YEAR = #form.fcYear#
				</cfquery>				
				
				<cfif form.fcWeek EQ 0>
					<cfset currentDateEnd = application.utilObj.getDateByPeriodWeekDay(getCal.startDate,form.fcPeriod,4,7)>					
					<cfset prevDateEnd = dateAdd("d",-28,currentDateEnd)>
					<cfset saleStartDate = dateAdd("d",1,prevDateEnd)>
				<cfelse>
					<cfset currentDateEnd = application.utilObj.getDateByPeriodWeekDay(getCal.startDate,form.fcPeriod,form.fcWeek,7)>
					<cfset prevDateEnd = dateAdd("d",-7,currentDateEnd)>
					<cfset saleStartDate = dateAdd("d",1,prevDateEnd)>
				</cfif>
				
				<cfset mondayDate = dateAdd("d",1,prevDateEnd)>
				
				<cfset saleStartDate = DateFormat(saleStartDate,application.eonDateFormat)>
				
				<cfset currentDateEnd = dateFormat(currentDateEnd,application.eonDateFormat)>
				<cfset prevDateEnd = dateFormat(prevDateEnd,application.eonDateFormat)>
				<cfset mondayDate = dateFormat(mondayDate,application.eonDateFormat)>				
				
				<cfquery name="getSales" datasource="#application.eonDsn#">
					SELECT SUM([credit]) AS NetSales FROM [dailySales] 
					WHERE
					1 = 1 
					AND [note] = 'Net Food Sales' 
					<cfif form.bUnit NEQ 0>
					AND [businessUnitId] = '#form.bUnit#'
					</cfif>
					AND [dateOfSale] BETWEEN '#saleStartDate#' AND '#currentDateEnd#'
				</cfquery>
			
				<cfquery name="getFoodCats" datasource="#application.eonDsn#">
					SELECT accountCode,categoryId FROM dbo.purchaseCategories
					WHERE accountCode = '4600'
					GROUP BY accountCode,categoryId
					ORDER BY accountCode	
				</cfquery>
				
				<p><cfoutput><b>For Week Ending:</b> #currentDateEnd#. <br><b>Net Sale:</b> #dollarFormat(getSales.NetSales)#</cfoutput></p>
				
				<table width="1000" style="border:green double 2px;margin:5px">
					<tr>
						<td>
						<b>Category</b>
						</td>
						<td>
						<b>Beg Inv.</b>
						</td>
						<td>
						<b>Purchases</b>
						</td>
						<td>
						<b>Transfers Received</b>
						</td>
						<td>
						<b>Transfers Sent</b>
						</td>
						<td>
						<b>End Inv.</b>
						</td>
						<td>
						<b>Usage</b>
						</td>
						<td>
						<b>%</b>
						</td>
					</tr>	
					<cfset idx = 1>
					<cfloop query="getFoodCats">
						
						<cfset idx++>
						<cfset rowclass = "lightrow"> 
						<cfif idx mod 2>
							<cfset rowclass = "darkrow">
						</cfif>
						
						<cfoutput>
								<cfquery name="getCosts" datasource="#application.eonDsn#">
									DECLARE @begInvVar MONEY;
									DECLARE @purchaseVar MONEY;
									DECLARE @endInvVar MONEY;
									DECLARE @transfersFrom MONEY;
									DECLARE @transfersTo MONEY;
									<cfif form.bUnit NEQ 0>
									SET @begInvVar = (SELECT ISNULL(InventoryAmount,0) FROM dbo.GetNewestInventorySnapshotByStoreDateCategoryId(
									'#form.bUnit#'
									,#getFoodCats.categoryId#
									,'#prevDateEnd#'));
									
									SET @endInvVar = (SELECT ISNULL(InventoryAmount,0) FROM dbo.GetNewestInventorySnapshotByStoreDateCategoryId(
									'#form.bUnit#'
									,#getFoodCats.categoryId#
									,'#currentDateEnd#'));									
									
									SET @purchaseVar = (SELECT ISNULL(PurchaseAmount,0) FROM GetPurchasesByStoreDateRangeAndCatId(
									'#form.bUnit#'
									,#getFoodCats.categoryId#
									,'#mondayDate#'
									,'#currentDateEnd#'));
									
									SET @transfersFrom = (SELECT ISNULL(TransfersFromAmount,0) FROM GetTransferFromStoreByStoreDateRangeCatId(
									'#form.bUnit#'
									,#getFoodCats.categoryId#
									,'#mondayDate#'
									,'#currentDateEnd#'));
									
									SET @transfersTo = (SELECT ISNULL(TransfersToAmount,0) FROM GetTransferToStoreByStoreDateRangeCatId(
									'#form.bUnit#'
									,#getFoodCats.categoryId#
									,'#mondayDate#'
									,'#currentDateEnd#'));
									
									<cfelse>
									
									SET @begInvVar = (SELECT ISNULL(InventoryAmount,0) FROM dbo.GetNewestInventorySnapshotAllStoresByDateCategoryId(
									#getFoodCats.categoryId#
									,'#prevDateEnd#'));
									
									SET @endInvVar = (SELECT ISNULL(InventoryAmount,0) FROM dbo.GetNewestInventorySnapshotAllStoresByDateCategoryId(
									#getFoodCats.categoryId#
									,'#currentDateEnd#'));
									
									
									SET @purchaseVar = (SELECT ISNULL(PurchaseAmount,0) FROM GetPurchasesByStoreDateRangeAndCatId(
									NULL
									,#getFoodCats.categoryId#
									,'#mondayDate#'
									,'#currentDateEnd#'));
									
									SET @transfersFrom = (SELECT ISNULL(TransfersFromAmount,0) FROM GetTransferFromStoreByStoreDateRangeCatId(
									NULL
									,#getFoodCats.categoryId#
									,'#mondayDate#'
									,'#currentDateEnd#'));
									
									SET @transfersTo = (SELECT ISNULL(TransfersToAmount,0) FROM GetTransferToStoreByStoreDateRangeCatId(
									NULL
									,#getFoodCats.categoryId#
									,'#mondayDate#'
									,'#currentDateEnd#'));
									
									</cfif>

									
									SELECT categoryDescr = (SELECT categoryDescr FROM dbo.purchaseCategories WHERE categoryId = #getFoodCats.categoryId#),
									begInv = (SELECT @begInvVar),
									purchases = (SELECT @purchaseVar),
									endInv = (SELECT @endInvVar),
									transfersTo = (SELECT @transfersTo),
									transfersFrom = (SELECT @transfersFrom),
									usage = (SELECT (@begInvVar+@purchaseVar+@transfersTo-@transfersFrom-@endInvVar))								
								</cfquery>
						</cfoutput>
								
						<cfoutput query="getCosts">    
						   	
								<tr class="#rowclass#">
									<td>
									#categoryDescr#
									</td>
									<td>
									#dollarFormat(begInv)#
									</td>
									<td>
									#dollarFormat(purchases)#
									</td>
									<td>
									#dollarFormat(transfersTo)#
									</td>
									<td>
									#dollarFormat(transfersFrom)#
									</td>
									<td>
									#dollarFormat(endInv)#
									</td>
									<td>
									#dollarFormat(usage)#
									</td>
									<td>
									<cfif IsNumeric(usage) AND IsNumeric(getSales.NetSales)>
										#DecimalFormat((usage/getSales.NetSales)*100)# %
									<cfelse>
										-
									</cfif>
									</td>
								</tr>	
				        </cfoutput>						
					</cfloop>					
													
							<cfset totBegInv = 0>
							<cfset totpurchases = 0>
							<cfset totTransfersTo = 0>
							<cfset totTransfersFrom = 0>
							<cfset totEndInv = 0>
							<cfset totUsage = 0>
							<cfset totPercent = 0>
						<cfloop index="accountCode" list="4600,4601,5720">												
							<cfquery name="getSums" datasource="#application.eonDsn#">
								DECLARE @begInvVar MONEY;
								DECLARE @purchaseVar MONEY;
								DECLARE @endInvVar MONEY;
								DECLARE @transfersFrom MONEY;
								DECLARE @transfersTo MONEY;
								
								<cfif form.bUnit NEQ 0>
								SET @begInvVar = (SELECT ISNULL(InventoryAmount,0) FROM GetInventoryByStoreDateAccountCode (
								   '#form.bUnit#'
								  ,'#accountCode#'
								  ,'#prevDateEnd#'));
								
								SET @endInvVar = (SELECT ISNULL(InventoryAmount,0) FROM GetInventoryByStoreDateAccountCode (
								   '#form.bUnit#'
								  ,'#accountCode#'
								  ,'#currentDateEnd#'));
								  
								SET @purchaseVar = (SELECT ISNULL(PurchaseAmount,0) FROM GetPurchasesByStoreAndDateRange (
								   '#form.bUnit#'
								  ,'#accountCode#'
								  ,'#mondayDate#','#currentDateEnd#'));
								  
								SET @transfersFrom = (SELECT ISNULL(TransfersFromAmount,0) FROM GetTransferFromStoreByStoreAndDateRange (
								   '#form.bUnit#'
								  ,'#accountCode#'
								  ,'#mondayDate#','#currentDateEnd#'));	
								  
								SET @transfersTo = (SELECT ISNULL(TransfersToAmount,0) FROM GetTransferToStoreByStoreAndDateRange (
								   '#form.bUnit#'
								  ,'#accountCode#'
								  ,'#mondayDate#','#currentDateEnd#'));	
								  												  
								
								<cfelse>
								
								SET @begInvVar = (SELECT ISNULL(InventoryAmount,0) FROM GetInventoryAllStoresByDateAccountCode (
								   '#accountCode#'
								  ,'#prevDateEnd#'));
								
								SET @endInvVar = (SELECT ISNULL(InventoryAmount,0) FROM GetInventoryAllStoresByDateAccountCode (
								   '#accountCode#'
								  ,'#currentDateEnd#'));
								  
								SET @purchaseVar = (SELECT ISNULL(PurchaseAmount,0) FROM GetPurchasesByStoreAndDateRange (
								   NULL
								  ,'#accountCode#'
								  ,'#mondayDate#','#currentDateEnd#'));
								  
								SET @transfersFrom = (SELECT ISNULL(TransfersFromAmount,0) FROM GetTransferFromStoreByStoreAndDateRange (
								   NULL
								  ,'#accountCode#'
								  ,'#mondayDate#','#currentDateEnd#'));	
								  
								SET @transfersTo = (SELECT ISNULL(TransfersToAmount,0) FROM GetTransferToStoreByStoreAndDateRange (
								   NULL
								  ,'#accountCode#'
								  ,'#mondayDate#','#currentDateEnd#'));	
								  					
								
								</cfif> 

								
								SELECT categoryDescr = 'TOTAL #listgetat(itemTypesList,listFind(itemCodeList,accountCode))#',
								begInv = (SELECT @begInvVar),
								purchases = (SELECT @purchaseVar),
								endInv = (SELECT @endInvVar),
								transfersTo = (SELECT @transfersTo),
								transfersFrom = (SELECT @transfersFrom),
								usage = (SELECT (@begInvVar+@purchaseVar+@transfersTo-@transfersFrom-@endInvVar))
							</cfquery>
								<tr><td colspan="8"><hr style="border:dashed green; border-width:2px 0 0; height:0;"></td></tr>
							
						<cfoutput query="getSums">        	
								<tr>
									<td>
									<b>#categoryDescr#</b>
									</td>
									<td>
									#dollarFormat(begInv)#
									<cfset totBegInv = totBegInv+begInv>
									</td>
									<td>
									#dollarFormat(purchases)#
									<cfset totpurchases = totpurchases+purchases>
									</td>
									<td>
									#dollarFormat(transfersTo)#
									<cfset totTransfersTo = totTransfersTo+transfersTo>
									</td>
									<td>
									#dollarFormat(transfersFrom)#
									<cfset totTransfersFrom = totTransfersFrom+transfersFrom>
									</td>
									<td>
									#dollarFormat(endInv)#
									<cfset totEndInv = totEndInv+endInv>
									</td>
									<td>
									#dollarFormat(usage)#
									<cfset totUsage = totUsage+usage>
									</td>
									<td>
									<cfif IsNumeric(usage) AND IsNumeric(getSales.NetSales)>
										<cfset perc = ((usage/getSales.NetSales)*100)>
										<b>#DecimalFormat(perc)# %</b>
										<cfset totPercent = totPercent+perc>
									<cfelse>
										-
									</cfif>
									</td>
								</tr>	
				        </cfoutput>								
					</cfloop>
								<tr><td colspan="8"><hr style="border:dashed red; border-width:2px 0 0; height:0;"></td></tr>
					<cfoutput>
                    				
									<tr>
										<td>
										<b>TOTAL COS</b>
										</td>
										<td>
										#dollarFormat(totBegInv)#
										</td>
										<td>
										#dollarFormat(totPurchases)#
										</td>
										<td>
										#dollarFormat(totTransfersTo)#
										</td>
										<td>
										#dollarFormat(totTransfersFrom)#
										</td>
										<td>
										#dollarFormat(totEndInv)#
										</td>
										<td>
										#dollarFormat(totUsage)#
										</td>
										<td>
										<cfif IsNumeric(totUsage) AND IsNumeric(getSales.NetSales)>
											<b>#DecimalFormat(totPercent)# %</b>
										<cfelse>
											-
										</cfif>
										</td>
									</tr>	
                    </cfoutput>
					</table>
			
			</cfif>

	</div>

<cfinclude template="footer.cfm">