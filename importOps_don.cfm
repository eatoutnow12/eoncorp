<cffile action = "read" file = "#ExpandPath('./')#\mas90\OpsDon.CSV" variable = "Report" charset="ISO-8859-1">

<cfset myObj = createObject("component", "cfc.utils")>
<cfset daoObj = application.daoObj>

<!--- Convert the CSV to an array of arrays.--->
<cfset arrCSV = myObj.CSVToArray(
	CSVData = Report,
	Delimiter = ",",
	Qualifier = """"
	) />

<cftry>

	<cftransaction>

		<cfloop from="2" to="#arrayLen(arrCSV)#" index="i">
			<cfset dataArr = arrCSV[i]>
			
			<cfset transDate = Trim(dataArr[1])>
			<cfset transDate = dateFormat(transDate,application.eonDateFormat)>
			<cfset amount = 0>					
			<cfset entryType = Trim(dataArr[5])>
			
<!---Transfers From Store Accounts--->			
			<cfif entryType EQ "CR" AND FindNoCase("Trsf from Little King",dataArr[3]) GT 0>				

				<cfset storeId = Trim(dataArr[3])>
				<cfset storeId = Trim(ListGetAt(storeId,5," " ))>
			
				<cfset amount = Trim(dataArr[4])>
				
				<cfif NOT IsNumeric(storeId) OR Len(storeId) NEQ 4>
					<cfthrow message="Store Id invalid on row number:  #i#. Delete that line to continue.">
				</cfif>
				
				<cfif amount gt 0>
					<cfset amount = DecimalFormat(amount)>
					<cfset amount = LSParseNumber(amount)>
				</cfif>
				
				<!---Make Debit Side Entry--->
				<cfset code = "1100-0000">
				<cfset note = "ACH TRANSFER TO OPS">
				<cfset transType = 1>
				
				<cfif amount gt 0>
					<cfset application.daoObj.Insertmas90Entry(code,note,transDate,amount,transType,'JE','misc')>
				</cfif>
				
				<!---Make Credit Side Entry--->
				<cfset code = "1101-#storeId#">
				<cfset transType = 2>
				
				<cfif amount gt 0>
					<cfset application.daoObj.Insertmas90Entry(code,note,transDate,amount,transType,'JE','misc')>
				</cfif>

			</cfif>

		</cfloop>
		
	</cftransaction>
	
	<cfcatch type="any">
		Import Failed	
	<cfdump var="#cfcatch#. Row number: #i#">
	</cfcatch>
</cftry>
		
