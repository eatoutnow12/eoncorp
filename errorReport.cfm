﻿		<cfparam name="attributes.report" default="">
		<cfparam name="attributes.parsedData" default="">
		<cfparam name="attributes.ErrorSubject" default="DSR Failure Report">
		<cfparam name="attributes.priority" default="normal">
		
		<cfif IsSimpleValue(attributes.report)>
			<cffile action="write" file="#ExpandPath('./')#\logs\dsr.log" output="#attributes.report#">
		</cfif>
		
		<cfset mailAttributes = {
			from="Status Reports <reports@eatoutnow.net>",
			to="kenny@eatoutnow.net",
			subject="#attributes.ErrorSubject#",
			priority="#attributes.priority#"
		}
		/>
		
		<cfmail attributeCollection="#mailAttributes#" type="html">
			<cfif NOT IsSimpleValue(attributes.report)>
				<cfdump var="#attributes.report#">
			<cfelse>
				<cfoutput>#attributes.report#</cfoutput>
			</cfif>
			<br/>
			<cfif NOT IsSimpleValue(attributes.parsedData)>
				<cfdump var="#attributes.parsedData#">
			<cfelse>
				<cfoutput>#attributes.parsedData#</cfoutput>
			</cfif>
		</cfmail>