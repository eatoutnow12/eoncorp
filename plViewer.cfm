﻿<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="9">
	
<script type="text/javascript">
	function ValidateSubmit()
	{
		var periodStart = parseInt(document.getElementById('periodStart').value);
		var periodEnd = parseInt(document.getElementById('periodEnd').value);
		
		var storeId = document.getElementById('bUnit');

		if (periodStart > periodEnd)
		{
			alert('"From period" should be smaller than or equal to "To period". \n Please select again.')
		}
		else if (storeId.selectedIndex < 0)
		{
			alert('Please select at least one store')
		}
		else
		{
			document.forms[0].submit();
		}
	}	

</script>
	
		
<form action="plViewer.cfm" method="post">
<div style="margin:10px"><h3>P&L Viewer</h3><br>

	<label for="bUnit">Select Store:</label>
	<select id="bUnit" name="bUnit" multiple="multiple" style="height:200px;width:80px">
		<cfloop query="application.getAllUnits">
			<cfoutput>
            	<option id="#Trim(unitName)#" value="#Trim(unitName)#" <cfif IsDefined("form.bUnit") AND ListFind(form.bUnit,unitName) GT 0>selected</cfif>>
				#Trim(unitName)#
				</option>
            </cfoutput>
		</cfloop>
	</select>
	
	
	<label for="plYear">Year:</label>
	<select id="plYear" name="plYear">
		<cfloop index="i" from="#year(now())#" to="2011" step="-1">
			<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.plYear") AND form.plYear EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
		</cfloop>
	</select>	
	
	<label for="periodStart">From Period:</label>
	<select id="periodStart" name="periodStart">
		<cfloop index="i" from="1" to="13">
			<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.periodStart") AND form.periodStart EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
		</cfloop>
	</select>	
	
	<label for="periodEnd">To Period:</label>
	<select id="periodEnd" name="periodEnd">
		<cfloop index="i" from="1" to="13">
			<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.periodEnd") AND form.periodEnd EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
		</cfloop>
	</select>			
<p></p>
<input type="button" onclick="ValidateSubmit()" value="Generate P&L">

</form>


<cfif IsDefined("form.bUnit")>
<p></p>	
	<cfset variables.storeList = form.bUnit>
	<cfset variables.plYear = form.plYear>

	
<cfoutput>

	<div style="width:100%" align="center">
	<a href="exportPlExcel.cfm" style="text-align:center;"><img src="/img/icons/logo_excel.gif" width="20" height="20" border="0"> Export</a>
	</div>
<br><br>	

<cfsavecontent variable="session.plExport">
	
	<table>
		<tr>
			<td>
			
			</td>
			
			<td>
				<b>Sales - Food</b>
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				Cost of Sales
			</td>
			
			<td>
				Syrup
			</td>
			
			<td>
				Supplies - Paper
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				<b>TOTAL Cost of Sales</b>
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				Wages - Management
			</td>
			
			<td>
				Wages - Hourly
			</td>
			
			<td>
				Payroll Taxes
			</td>
			
			<td>
				Insurance - Grp Health
			</td>
			
			<td>
				Training
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				<b>TOTAL Cost of Labor</b>
			</td>
			
			<td>
				&nbsp;
			</td>

			<td>
				Cash Over/Short
			</td>
			
			<td>
				Uniforms/Laundry
			</td>
			
			<td>
				Supplies - Operating
			</td>
			
			<td>
				Janitorial
			</td>
			
			<td>
				Repairs/Maintenance
			</td>
			
			<td>
				Equipment Lease
			</td>
			
			<td>
				Auto Expense
			</td>
			
			<td>
				Smallwares
			</td>
			
			<td>
				Bank Service Charges
			</td>
			
			<td>
				Visa/MC/Amex
			</td>
			
			<td>
				Royalties
			</td>
			
			<td>
				Advertising
			</td>
			
			<td>
				Travel
			</td>
			
			<td>
				NSF Checks
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				<b>TOTAL Variable Expenses</b>
			</td>
			
			<td>
				&nbsp;
			</td>
				
			<td>
				Licenses/Permits
			</td>
			
			<td>
				Elec/Gas/Water/Tele
			</td>
			
			<td>
				Trash Removal
			</td>
			
			<td>
				Rent
			</td>
			
			<td>
				CAM
			</td>
			
			<td>
				Taxes - PPTX/RE
			</td>
			
			<td>
				Insurance - Business
			</td>
			
			<td>
				INS - WORK. COMP.
			</td>
			
			<td>
				INSURANCE - AUTO
			</td>
			
			<td>
				Auto - Admin
			</td>
			
			<td>
				Legal/Prof Fees
			</td>
			
			<td>
				Income Taxes
			</td>
			
			<td>
				Admin - Hlth/Life Ins
			</td>
			
			<td>
				Admin Expenses
			</td>
			
			<td>
				Admin Payroll
			</td>
			
			<td>
				Depreciation			
			</td>
			
			<td>
				Amortization
			</td>
			
			<td>
				Penalties
			</td>
			
			<td>
				Interest Expense
			</td>
			
			<td>
				Misc Expense
			</td>
			
			<td>
				Bad Debt
			</td>
			
			<td>
				Gain/Loss on Sale of Asset
			</td>
			
			<td>
				Interest Income
			</td>
			
			<td>
				Other Income
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				<b>TOTAL Fixed Expenses</b>
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				<b>NET INCOME (LOSS)</b>
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				G&A (Above Rest. Exp.)
			</td>
			
			<td>
				&nbsp;
			</td>
			
			<td>
				Company Net Income*
			</td>
			
		</tr>
		
		<cfquery name="getCodes" datasource="#application.eonFinDsn#">
			SELECT [code]
			      ,[codeLabel]
			      ,[codeType] 
				  ,[codeVarName]
			FROM dbo.plCodeLabels
			WHERE codeType > 0
			ORDER BY codeType
		</cfquery>	
		
		<cfloop list="#variables.storeList#" index="storeId">
			
			<cfset plResultStruct = structNew()>
			
			<cfloop query="getCodes">
				<cfset plResultStruct[getCodes.codeVarName] = 0>				
			</cfloop>	
		
			<cfloop index="periodIdx" from="#form.periodStart#" to="#form.periodEnd#">
			
				<cfset plPeriod = periodIdx>
	
				<cfif Len(plPeriod) EQ 1>
					<cfset plPeriod = '0' & plPeriod>
				</cfif>
				
				<cfloop query="getCodes">
					<cfset variables.metricLabel = getCodes.codeLabel>
					
					<cfquery name="getPL" datasource="#application.eonFinDsn#">			
						SELECT GF2_NetChange as Metric FROM dbo.[#plYear#_#plPeriod#] WHERE GH2_LeftAccount = '#getCodes.code#-#storeId#'					
					</cfquery>
					
					<cfif getPL.RecordCount>
						<cfset plResultStruct[getCodes.codeVarName] = plResultStruct[getCodes.codeVarName] + getPL.Metric>
					</cfif>
					
				</cfloop>
			</cfloop>
			<tr>
				<td>
					<b><u>#storeId#</u></b>
				</td>
				
				<td>
					<cfset Sales_Food = (plResultStruct.Sales_Food*-1)-plResultStruct.Promo_Food>
					#Sales_Food#
				</td>
				
				<td>
					&nbsp;
				</td>
				
				<td>
					#plResultStruct.CostofSales#
				</td>
				
				<td>
					#plResultStruct.Syrup#
				</td>
				
				<td>
					#plResultStruct.Supplies_Paper#
				</td>
				
				<td>
					&nbsp;
				</td>
				
				<td>
					<cfset costOfSalesTotal = plResultStruct.CostofSales+plResultStruct.Syrup+plResultStruct.Supplies_Paper>
					#costOfSalesTotal#
				</td>
				
				<td>
					&nbsp;
				</td>
				
				<td>
					#plResultStruct.Wages_Management#
				</td>
				
				<td>
					#plResultStruct.Wages_Hourly#
				</td>
				
				<td>
					#plResultStruct.PayrollTaxes#
				</td>
				
				<td>
					#plResultStruct.Insurance_GrpHealth#
				</td>
				
				<td>
					#plResultStruct.Training#
				</td>
				
				<td>
					&nbsp;
				</td>
				
				<td>
					<cfset costOfLaborTotal = plResultStruct.Wages_Management+plResultStruct.Wages_Hourly+plResultStruct.PayrollTaxes+plResultStruct.Insurance_GrpHealth+plResultStruct.Training>
					#costOfLaborTotal#
				</td>
				
				<td>
					&nbsp;
				</td>

				
				<td>
					#plResultStruct.CashOverShort#
				</td>
				
				<td>
					#plResultStruct.UniformsLaundry#
				</td>
			
				<td>
					#plResultStruct.Supplies_Operating#
				</td>
				
				<td>
					#plResultStruct.Janitorial#
				</td>
				
				<td>
					#plResultStruct.RepairsMaintenance#
				</td>
				
				<td>
					#plResultStruct.EquipmentLease#
				</td>
				
				<td>
					#plResultStruct.AutoExpense#
				</td>
				
				<td>
					#plResultStruct.Smallwares#
				</td>
				
				<td>
					#plResultStruct.BankServiceCharges#
				</td>
				
				<td>
					#plResultStruct.VisaMCAmex#
				</td>
				
				<td>
					#plResultStruct.Royalties#
				</td>
				
				<td>
					<cfif storeId EQ "5514">
						<cfset plResultStruct.Advertising = (Sales_Food*0.04)>
					<cfelse>
						<cfset plResultStruct.Advertising = (Sales_Food*0.03)>
					</cfif>
					
					
					#plResultStruct.Advertising#
				</td>
				
				<td>
					#plResultStruct.Travel#
				</td>
				
				<td>
					#plResultStruct.NSFchecks#
				</td>
				
				<td>
					&nbsp;
				</td>
				
				<td>
					<cfset variableTotal = plResultStruct.Travel+plResultStruct.CashOverShort+plResultStruct.NSFchecks+plResultStruct.UniformsLaundry+plResultStruct.Supplies_Operating+plResultStruct.Janitorial+plResultStruct.RepairsMaintenance+plResultStruct.EquipmentLease+plResultStruct.AutoExpense+plResultStruct.Smallwares+plResultStruct.BankServiceCharges+plResultStruct.VisaMCAmex+plResultStruct.Royalties+plResultStruct.Advertising>
					#variableTotal#
				</td>				
				
				<td>
					&nbsp;
				</td>
					
				<td>
					#plResultStruct.LicensesPermits#
				</td>
				
				<td>
					#plResultStruct.ElecGasWaterTele#
				</td>
				
				<td>
					#plResultStruct.TrashRemoval#
				</td>
				
				<td>
					#plResultStruct.Rent#
				</td>
				
				<td>
					#plResultStruct.CAM#
				</td>
				
				<td>
					#plResultStruct.Taxes_PPTXRE#
				</td>
				
				<td>
					#plResultStruct.Insurance_Business#
				</td>
				
				<td>
					#plResultStruct.INS_WORKCOMP#
				</td>
				
				<td>
					#plResultStruct.INSURANCE_AUTO#
				</td>
				
				<td>
					#plResultStruct.Auto_Admin#
				</td>
				
				<td>
					#plResultStruct.LegalProfFees#
				</td>
				
				<td>
					#plResultStruct.IncomeTaxes#
				</td>
				
				<td>
					#plResultStruct.Admin_HlthLifeIns#
				</td>
				
				<td>
					#plResultStruct.AdminExpenses#
				</td>
				
				<td>
					#plResultStruct.AdminPayroll#
				</td>
				
				<td>
					#plResultStruct.Depreciation#			
				</td>
				
				<td>
					#plResultStruct.Amortization#
				</td>
				
				<td>
					#plResultStruct.Penalties#
				</td>
				
				<td>
					#plResultStruct.InterestExpense#
				</td>
				
				<td>
					#plResultStruct.MiscExpense#
				</td>
				
				<td>
					#plResultStruct.BadDebt#
				</td>
				
				<td>
					#plResultStruct.GainLossonSaleofAsset#
				</td>
				
				<td>
					#plResultStruct.InterestIncome#
				</td>
				
				<td>
					#plResultStruct.OtherIncome#
				</td>
				
				<td>
					&nbsp;
				</td>
				
				<td>
					<cfset fixedTotal = plResultStruct.LicensesPermits+plResultStruct.ElecGasWaterTele+plResultStruct.TrashRemoval+plResultStruct.Rent+plResultStruct.CAM+plResultStruct.Taxes_PPTXRE+plResultStruct.Insurance_Business+plResultStruct.INS_WORKCOMP+plResultStruct.INSURANCE_AUTO+plResultStruct.Auto_Admin+plResultStruct.LegalProfFees+plResultStruct.IncomeTaxes+plResultStruct.Admin_HlthLifeIns+plResultStruct.AdminExpenses+plResultStruct.AdminPayroll+plResultStruct.Depreciation+plResultStruct.Amortization+plResultStruct.Penalties+plResultStruct.InterestExpense+plResultStruct.MiscExpense+plResultStruct.BadDebt+plResultStruct.GainLossonSaleofAsset+plResultStruct.InterestIncome+plResultStruct.OtherIncome>
					#fixedTotal#
				</td>
				
				<td>
					&nbsp;
				</td>
				
				<td>
					<cfset netIncome = Sales_Food - (costOfSalesTotal+costOfLaborTotal+variableTotal+fixedTotal)>
					#netIncome#
				</td>
			
				<td>
					&nbsp;
				</td>
				
				<td>
					<cfset plResultStruct.GnA = (Sales_Food*0.04)> 
					#plResultStruct.GnA#
				</td>
				
				<td>
					&nbsp;
				</td>
				
				<td>
					<cfset compNetIncome = (netIncome - plResultStruct.GnA)>
					#compNetIncome#
				</td>
				
			</tr>		
		</cfloop>
			<tr>
				<td colspan="66">
				*Before loan payment, capital expenditure, franchisor expenses, and owner compensation
				</td>
			</tr>
	</table>
	</cfsavecontent>
	
	#session.plExport#

</cfoutput>



</cfif>
<cfinclude template="footer.cfm">