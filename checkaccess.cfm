﻿<cfparam name="variables.error" default="">
<cfparam name="attributes.reportId" default="0">

<cfquery name="getUserPermissions" datasource="#application.eonDSn#">
	SELECT [userAccess]
	FROM [userPermissions]
	WHERE userId = '#session.userId#' AND reportId = #attributes.reportId#
</cfquery>

<cfif NOT getUserPermissions.RecordCount>
	<cfset variables.error = "You do not have permissions to access this report.">
	<cfset session.permission1 = "">
<cfelse>
	<cfset session.permission1 = getUserPermissions.userAccess>
</cfif>

<cfif Len(Trim(variables.error))>
	<cflocation url="#application.baseurl#login.cfm?err=#urlEncodedFormat(variables.error)#" addtoken="false">
</cfif>