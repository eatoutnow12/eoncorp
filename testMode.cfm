﻿<cfparam name="url.test" default="0">

<cfset application.eonMonitorTestMode = url.test>

<cfif application.eonMonitorTestMode EQ 0>		
		<!---Update Store Access Record with current date and time and set status to 0.--->
		<cfquery name="updateStatus" datasource="#application.eonProcData#">
			UPDATE [storeSystemMonitor]
		    SET [lastConnection] = GETDATE()
		        ,[notified] = 0
		</cfquery>
</cfif>