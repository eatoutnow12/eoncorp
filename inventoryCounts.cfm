<cfinclude template="header.cfm">

		<script type="text/javascript">
			function ValidateSubmit()
			{
				document.forms[0].submit();
			}	
		</script>
		<cfset ctr = 0>
		
		<style>			
			
			.headerRow {
		 		color: #FFFFFF;
				background-color:#000;
				padding-left:10px;
		 	}
						
			td {
				width:inherit;
				padding-left:10px;
			}
			
		</style>
		
<form action="inventoryCounts.cfm" method="post">
<div style="margin:10px"><h3>Inventory Counts Viewer</h3><br>

		<cfif NOT IsDefined("cookie.storeId")>
			<label for="bUnit">Select Store:</label>	
			<select id="bUnit" name="bUnit">
				<cfloop query="application.getAllUnits">
					<cfoutput>
		            	<option id="#Trim(unitName)#" value="#Trim(id)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ id>selected</cfif>>
						#Trim(unitName)#
						</option>
		            </cfoutput>
				</cfloop>
				<option id="0" value="0" <cfif IsDefined("form.bUnit") AND form.bUnit EQ 0>selected</cfif>>All</option>
			</select>&nbsp;&nbsp;&nbsp;&nbsp;
		<cfelse>
			<cfoutput><input type="hidden" name="bUnit" value="#cookie.storeId#"></cfoutput>
		</cfif>
	
		<label for="fcYear">Year:</label>
		<select id="fcYear" name="fcYear">
			<cfloop index="i" from="#year(now())#" to="2011" step="-1">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcYear") AND form.fcYear EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>
		</select>	
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="fcPeriod">Period:</label>
		<select id="fcPeriod" name="fcPeriod">
			<cfloop index="i" from="1" to="13">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcPeriod") AND form.fcPeriod EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>
		</select>
		
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="fcWeek">Week:</label>
		<select id="fcWeek" name="fcWeek">					
			<cfloop index="i" from="1" to="4">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcWeek") AND form.fcWeek EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>			
		</select>

		<input type="button" onclick="ValidateSubmit()" value="View Inventory Counts">

</form>

	<cfif IsDefined("form.bUnit")>
		
		<cfquery name="getUnitName" dbtype="query">
			SELECT unitName FROM application.getAllUnits
			WHERE id = '#form.bUnit#'
		</cfquery>
		
		<cfquery name="getCal" datasource="#application.eonDsn#">
			SELECT startDate 
			FROM businessCalendar
			WHERE YEAR = #form.fcYear#
		</cfquery>				
		
		<cfif form.fcWeek EQ 0>
			<cfset currentDateEnd = application.utilObj.getDateByPeriodWeekDay(getCal.startDate,form.fcPeriod,4,7)>					
		<cfelse>
			<cfset currentDateEnd = application.utilObj.getDateByPeriodWeekDay(getCal.startDate,form.fcPeriod,form.fcWeek,7)>
		</cfif>
		
		<cfset currentDateEnd = dateformat(currentDateEnd, application.eonDateFormat)>
		
		<cfquery name="getInventory" datasource="#application.eonDsn#">
			SELECT storeInventory.itemNumber, purchaseItems.itemDescription, quantity, price, dbo.purchaseCategories.categoryDescr, itemCategoryId,itemUnitLabel
			FROM dbo.storeInventory
			INNER JOIN dbo.purchaseItems
			ON dbo.storeInventory.itemNumber = dbo.purchaseItems.itemNumber
			INNER JOIN dbo.purchaseCategories
			ON dbo.purchaseItems.itemCategoryId = dbo.purchaseCategories.categoryId
			INNER JOIN storeItemUnits 
			ON purchaseItems.itemUnitId = storeItemUnits.itemUnitId 
			WHERE 
			businessunitId = '#form.bUnit#'
			AND isLatest = 1
			AND dateweekEnding = '#currentDateEnd#'
			AND inProcess = 0
UNION  		
			SELECT storeInventory.itemNumber
			,itemDescription= (SELECT itemDescription FROM dbo.storeInProcessItems WHERE itemNumber=storeInventory.itemNumber AND storeInProcessItems.id = storeInventory.inProcess), quantity, price
			,categoryDescr= (SELECT categoryDescr FROM dbo.purchaseCategories WHERE categoryId = (SELECT itemCategoryId FROM purchaseItems WHERE itemNumber = storeInventory.itemNumber))
			,itemCategoryId= (SELECT itemCategoryId FROM purchaseItems WHERE itemNumber = storeInventory.itemNumber)
			,itemUnitLabel= (SELECT storeItemUnits.itemUnitLabel FROM purchaseItems INNER JOIN storeItemUnits ON purchaseItems.itemUnitId = storeItemUnits.itemUnitId WHERE itemNumber = storeInventory.itemNumber)
			FROM dbo.storeInventory
			WHERE 
			storeInventory.businessunitId = '#form.bUnit#'
			AND isLatest = 1
			AND dateweekEnding = '#currentDateEnd#'
			AND inProcess > 0
			ORDER BY itemCategoryId, storeInventory.itemNumber	
		</cfquery>
		<br><br>
		<table width="800" style="border:green double 2px;">
			<tr>
				<td>
				<b>Item Number</b>
				</td>
				<td nowrap="nowrap">
				<b>Item Description</b>
				</td>
				<td>
				<b>Unit</b>
				</td>
				<td>
				<b>Quantity</b>
				</td>
				<td>
				<b>Unit Price</b>
				</td>
				<td nowrap="nowrap">
				<b>Item Category</b>
				</td>
			</tr>
		
		<cfloop query="getInventory">
			<cfoutput>
            	<tr bgcolor="###iif(ctr MOD 2,DE('efefef'),DE('ffffff'))#">
					<td>
					#ItemNumber#
					</td>
					<td nowrap="nowrap">
					#ItemDescription#
					</td>
					<td>
					#itemUnitLabel#
					</td>
					<td>
					#Quantity#
					</td>
					<td>
					#Price#
					</td>
					<td nowrap="nowrap">
					#categoryDescr#
					</td>
				</tr>
            </cfoutput>
			<cfset ctr++>
		</cfloop>
		</table>

	</cfif>
<cfinclude template="footer.cfm">