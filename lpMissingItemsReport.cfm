﻿<cfquery name="getItems" datasource="#application.eonDsn#">
	SELECT invoiceNumber, itemNumber, invoiceDate, invoiceLineNumber FROM purchaseInvoices
	WHERE invoiceDate BETWEEN CONVERT(VARCHAR(16),DATEADD(DAY,-9,GETDATE()),101) AND CONVERT(VARCHAR(16),DATEADD(DAY,-3,GETDATE()),101)
	AND itemExtendedPrice > 0
	AND purchaseInvoices.itemNumber NOT IN (SELECT itemNumber FROM purchaseItems)
	AND purchaseInvoices.itemNumber <> '999999'
	ORDER BY invoiceNumber, itemNumber
</cfquery>

<cfset mailAttributes = {
	from="EatOutNow Status Reports <do-not-reply@eatoutnow.net>",
	to="invoicesreport@eatoutnow.net",
	subject="Missing Items Found"
}>

<cfset mailAttributes2 = {
	from="EatOutNow Status Reports <do-not-reply@eatoutnow.net>",
	to="kenny@eatoutnow.net",
	subject="No Missing Items Found"
}>

<cfif getItems.RecordCount GT 0>
	<cfmail attributeCollection="#mailAttributes#" type="html">
		<cfdump var="#getItems#" label="Items Not Found in EON database" metainfo="No">
	</cfmail>
<cfelse>
	<cfmail attributeCollection="#mailAttributes2#" type="text">
		Ran report. No missing items.
	</cfmail>
</cfif>