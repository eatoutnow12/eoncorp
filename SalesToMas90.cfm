﻿
<cfsetting enablecfoutputonly="Yes">
<cfset ReportDate = '06/13/2010'>


<cfquery name="getDSR" datasource="#application.eonDsn#">
	SELECT [glAccountCodes].[code],[dailySales].[note], [dailySales].[dateOfSale],
	CASE
	    WHEN [dailySales].[debit] > 0 THEN [dailySales].[debit]
	    WHEN [dailySales].[credit] > 0 THEN (-1 * [dailySales].[credit])
	    END AS "Amount"
	,[businessUnit].[unitName], dbo.businessUnit.id AS bUnitId
	FROM [dailySales]
	INNER JOIN [glAccountCodes] ON [glAccountCodes].[id] = [dailySales].[glAccountCodeId]
	INNER JOIN [businessUnit] ON [dbo].[glAccountCodes].[businessUnitId] = [dbo].[businessUnit].[id]
	WHERE ([dailySales].[debit] > 0 OR [dailySales].[credit] > 0)
	
	<!---AND [dailySales].[businessUnitId] = '78379866-5ac7-4b8c-bb7f-01686c2c2742'--->
	<!---AND [dailySales].[dateOfSale] >= '04/27/2010'--->
	AND [dailySales].[dateOfSale] = '#ReportDate#'
	
	ORDER BY [dailySales].[dateOfSale], [glAccountCodes].[codeDescription]
</cfquery>

<cfquery name="getNetSales" dbtype="query">
	SELECT unitName, Amount, dateOfSale, bUnitId from getDSR where note = 'Net Food Sales'
</cfquery>

<cfset advtCredit = 0>

<cfloop query="getNetSales">
	
	<cfset store = getNetSales.unitName>
	
	<cfset advtCode = "6200-" & store>
	
	<cfif store NEQ "5514">
		<cfset advtRate = 0.02>
	<cfelse>
		<cfset advtRate = 0.01>
	</cfif>
	
	<cfset advtFee = (Abs(getNetSales.Amount) * advtRate)>
	<cfset advtFee = LSParseNumber(advtFee)>
	<cfset advtFee = DecimalFormat(advtFee)>
	<cfset advtFee = LSParseNumber(advtFee)>
	<cfset advtCredit = Evaluate(advtCredit+advtFee)>
	<cfset advtCredit = DecimalFormat(advtCredit)>
	<cfset advtCredit = LSParseNumber(advtCredit)>
	
	
	<cfset temp = queryAddRow(getDSR, 1)>
	<cfset temp = querySetCell(getDSR, "code", advtCode)>
	<cfset temp = querySetCell(getDSR, "dateOfSale", getNetSales.dateOfSale)>
	<cfset temp = querySetCell(getDSR, "Amount", advtFee)>
	<cfset temp = querySetCell(getDSR, "note", "Advertising Fee")>
	
	<cfset creditDate = getNetSales.dateOfSale>
	
	<!---Add Rows for Calculated Cash Over/Short--->
	
	<cfset cashOverShortCode = "5700-" & store>
	
	<cfstoredproc procedure="REP_CashOverShortByStoreAndDate" datasource="#application.eonDsn#">
		<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#getNetSales.bUnitId#">
		<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#getNetSales.dateOfSale#">
		
		<cfprocresult name="Cos">
	</cfstoredproc>
	
	<cfset cashOverShortDollar = Cos.cashOverShort>
	<cfset cashOverShortDollar = DecimalFormat(cashOverShortDollar)>
	<cfset cashOverShortDollar = LSParseNumber(cashOverShortDollar)>
	
	
	<cfset temp = queryAddRow(getDSR, 1)>
	<cfset temp = querySetCell(getDSR, "code", cashOverShortCode)>
	<cfset temp = querySetCell(getDSR, "dateOfSale", getNetSales.dateOfSale)>
	<cfset temp = querySetCell(getDSR, "Amount", cashOverShortDollar)>
	<cfset temp = querySetCell(getDSR, "note", "Cash over/short")>
	
</cfloop>

	<cfset temp = queryAddRow(getDSR, 1)>
	<cfset temp = querySetCell(getDSR, "code", "1120-0000")>
	<cfset temp = querySetCell(getDSR, "dateOfSale", creditDate)>
	<cfset advtCredit = (-1 * advtCredit)>
	<cfset temp = querySetCell(getDSR, "Amount", LSParseNumber(advtCredit))>
	<cfset temp = querySetCell(getDSR, "note", "Cumulative Advertising Fee")>

<!--- set vars for special chars --->
<cfset TabChar = Chr(9)>
<cfset NewLine = Chr(13) & Chr(10)>
<!--- set content type to invoke Excel --->
<cfcontent type="application/msexcel">

<!--- suggest default name for XLS file --->
<!--- use "Content-Disposition" in cfheader for 
Internet Explorer  --->
<cfheader name="Content-Disposition" value="filename=#DateFormat(ReportDate,"mmddyyyy-")#DsrToMas90.xls">
 <!--- output data using cfloop & cfoutput --->
<cfloop query="getDSR">
  <cfoutput>#code##TabChar##note##TabChar##DateFormat(dateOfSale, "mm/dd/yyyy")##TabChar#JE#TabChar##DateFormat(dateOfSale, "yymmdd")##TabChar#1#TabChar#GL#TabChar##Amount##TabChar##note##NewLine#</cfoutput>
</cfloop>