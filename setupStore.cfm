<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="3">

<cfif NOT IsDefined("cookie.storeId") OR NOT Len(cookie.storeId)>
	<cfif NOT IsDefined("form.submit")>
	
		<form action="setupStore.cfm" method="post">
			Select Store: 
			<select id="bUnit" name="bUnit">
					<option id="0000" value="0000">
					0000
					</option>				
					<cfloop query="application.getAllUnits">
					<cfoutput>

					<option id="#id#" value="#id#">
					#unitName#
					</option>
					</cfoutput>
				</cfloop>
			</select>	<br><br>
			<input type="submit" name="submit" value="submit">
		</form>
	
	<cfelse>
		
		<cfset variables.store = Trim(form.bUnit)>
		
		<cfquery dbtype="query" name="getStore">
			SELECT unitName FROM application.getAllUnits
			WHERE id = '#variables.store#'
		</cfquery>
		
		<cfif Len(variables.store)>
			
			<cfcookie name="storeId" value="#variables.store#" expires="never">
			<cfcookie name="storeName" value="#getStore.unitName#" expires="never">

			<cfdump var="#cookie.storeId#">
			<cflog type="information" file="eonCorp" text="Store Setup app used at IP: #cgi.REMOTE_ADDR# for store: #cookie.storeName#">
		</cfif>
	
	</cfif>
<cfelse>
	<cfdump var="#cookie.storeId#">
</cfif>

<cfinclude template="footer.cfm">