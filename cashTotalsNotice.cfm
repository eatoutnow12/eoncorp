	<cfset yesterday = dateAdd("d",-1,Now())>
	<cfset yesterday = dateFormat(yesterday,application.eonDateFormat)>
	
<cfloop index="zoneId" from="1" to="2">
	
	<cfif zoneId EQ 1>
		<cfset mailAttributes = {
			from="Eon Reports <reports@eatoutnow.net>",
			to="brad.lau@eatoutnow.net",
			cc="office@eatoutnow.net",
			subject="Abnormal cash levels report - Omaha"
		}
		/>
	<cfelseif zoneId EQ 2>
		<cfset mailAttributes = {
			from="Eon Reports <reports@eatoutnow.net>",
			to="sami.hynek@eatoutnow.net",
			cc="office@eatoutnow.net",
			subject="Abnormal cash levels report - Lincoln"
		}
		/>
	</cfif>

	<cfquery datasource="#application.eonDsn#" name="getCashNums">
		SELECT dbo.businessUnit.unitName,
		dbo.storeNightlyData.gcTotal,
		dbo.storeNightlyData.safeTotal
		FROM dbo.storeNightlyData
		INNER JOIN dbo.businessUnit
		ON dbo.storeNightlyData.businessUnitId = dbo.businessUnit.id
		WHERE dbo.storeNightlyData.businessDate = '#yesterday#'
		AND (dbo.storeNightlyData.gcTotal+dbo.storeNightlyData.safeTotal) <> #application.cashLevel#
		AND (dbo.storeNightlyData.gcTotal+dbo.storeNightlyData.safeTotal) > 0
		AND businessUnit.unitZoneId = #zoneId#
		ORDER BY unitName
	</cfquery>
	
	<cfif getCashNums.RecordCount>
		<cfmail attributeCollection="#mailAttributes#" type="html">
			<b>Stores with abnormal cash levels for #yesterday#:</b>
			<cftable query="getCashNums" border="1" HTMLTable colheaders>
				<cfcol text="#unitName#" header="<b>Store</b>">
				<cfcol text="#dollarFormat(safeTotal)#" header="<b>Safe</b>">
				<cfcol text="#dollarFormat(gcTotal)#" header="<b>Gift Certificates</b>">
			</cftable>			
		</cfmail>	
	</cfif>
</cfloop>