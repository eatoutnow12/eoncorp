﻿
<cfparam name="variables.businessUnitId" default="">
<cfparam name="variables.appDateFormat" default="mm/dd/yyyy">
<cfset daoObj = createObject("component","cfc.dsrDAO")>
<cfset parsedData = ArrayNew(1)>
<cfset folderPrefix = DateFormat(Now(),"yyyymmdd")>
<cfset save_path = ExpandPath('./') & "\DSR\" & folderPrefix & "\">
<cfset debugMode = false>
<cfset application.dsrNoError = 1>
<cfset storesImported = ArrayNew(1)>

<cfif directoryExists(#save_path#)>
	<cfdirectory action="delete" directory="#save_path#" recurse="true">
<cfelse>
	<cfdirectory action="create" directory="#save_path#">
</cfif>

<cftry>
	<cfset javaSystem = createObject("java", "java.lang.System") />
	<cfset jProps = javaSystem.getProperties() />
	<cfset jProps.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory") />
	<cfset jProps.setproperty("mail.pop3.port",995) />
	<cfset jProps.setProperty("mail.pop3.socketFactory.port", 995) />
	<cfset dsrFileCount = 0>


	<cfpop 
	action="GETALL" 
	name="dsrQry" 
	server="pop.gmail.com" 
	username="dsr@eatoutnow.net" 
	password="eonsystem" 
	port="995" 
	attachmentpath="#save_path#"
	generateuniquefilenames="true"
	timeout="360">
	
	<cfif FileExists("#save_path#DSR.TXT")>	
		<cffile action="rename" source="#save_path#DSR.TXT" destination="#save_path#DSR0.TXT">
	</cfif>

	<cfif FileExists("#save_path#MMIX.TXT")>	
		<cffile action="rename" source="#save_path#MMIX.TXT" destination="#save_path#MMIX0.TXT">
	</cfif>

	
	<cfdirectory action="list" directory="#save_path#" filter="DSR*.txt" name="dsrTextFiles">
	<cfdirectory action="list" directory="#save_path#" filter="MMIX*.txt" name="mmixTextFiles">
	
	<cfset dsrFileCount = dsrTextFiles.RecordCount-1>

<cfcatch type="any">
	<cfif NOT debugMode>
		<cfmodule template="errorReport.cfm" report="#cfcatch#">
		<cfset application.dsrNoError = 0>
	</cfif>
</cfcatch>
</cftry>
	
<cfif dsrTextFiles.RecordCount>
	<cfloop index="fileIdx" from="0" to="#dsrFileCount#">
		
		<cftry>
		
			<cffile action = "read" file = "#save_path#DSR#fileIdx#.TXT" variable = "Report" charset="ISO-8859-1">
			
			<cfset lineDelim = Chr(13) & Chr(10)>
			
			<cfset columnDelim = chr(32) & chr(32)>
			
			<cfscript>
				parsedData = ListToArray(Report,lineDelim, false);
			</cfscript>
			
			<cfset variables.beginDate = DateFormat(ListToArray(parsedData[3], ' ', false, true)[4], variables.appDateFormat)>
			<cfset variables.endDate = DateFormat(DateAdd("d", variables.beginDate, 6), variables.appDateFormat)>
			
			<cfset storeId = ListToArray(parsedData[2], '  ', false, true)>
			<cfset storeId = Trim(ListGetAt(storeId[1],2,'-'))>
			
			<cfif NOT isNumeric(storeId)>
				<cfthrow message="storeId not numeric. storeId: #storeId#">
			<cfelse>
				<cfquery name="getBusinessUnit" datasource="#application.eonDsn#" cachedwithin="#CreateTimeSpan(10,0,0,0)#">
					SELECT id FROM businessUnit 
					WHERE unitName = '#storeId#' 
				</cfquery>
				<cfif getBusinessUnit.RecordCount>
					<cfset variables.businessUnitId = getBusinessUnit.id>			
				</cfif>				
			</cfif>
			
		<cfcatch type="any">
			<cfif NOT debugMode>
				<cfmodule template="errorReport.cfm" report="#cfcatch#" parsedData="#parsedData#">
			</cfif>
		</cfcatch>	
		
		</cftry>
		
		<cfset netSalesPos = 3>
		<cfset salesTaxPos = 3>
		<cfset paidOutsPos = 3>
		<cfset gcRedeemedPos = 3>
		<cfset ArPos = 2>
		<cfset VisaMcPos = 3>
		<cfset AmexPos = 2>
		<cfset AmDepPos = 3>
		<cfset PmDepPos = 3>
		<cfset couponPos = 3>
		<cfset promosPos = 3>
		<cfset cashOverPos = 2>
		<cfset couponNumPos = 4>
		<cfset promoNumPos = 4>
		<cfset deletesNumPos = 2>
		<cfset custCntPos = 3>
		<cfset laborPos = 3>		
		<cfset giftCertPos = 3>
		<cfset giftCertPaidOutPos = 3>
		
		<cfif Len(variables.businessUnitId)>
			<cfset variables.netSalesCodeId = daoObj.getAccountCode(variables.businessUnitId, '4100-%')>
		<cfif variables.businessUnitId EQ "85dbf6e1-7550-4116-8dbd-6b0af85d1037">
			<cfset variables.salesTaxCodeId = daoObj.getAccountCode(variables.businessUnitId, '2361-0000')>	
		<cfelse>
			<cfset variables.salesTaxCodeId = daoObj.getAccountCode(variables.businessUnitId, '2360-0000')>
		</cfif>
			<cfset variables.paidOutsCodeId = daoObj.getAccountCode(variables.businessUnitId, '5715-%')>			
			<cfset variables.gcRedeemedCodeId = daoObj.getAccountCode(variables.businessUnitId, '1101-%')>
			<cfset variables.ArCodeId = daoObj.getAccountCode(variables.businessUnitId, '1200-0000')>
			<cfset variables.VisaCodeId = daoObj.getAccountCode(variables.businessUnitId, '1101-%')>
			<cfset variables.AmexCodeId = daoObj.getAccountCode(variables.businessUnitId, '1101-%')>
			<cfset variables.AmDepositCodeId = daoObj.getAccountCode(variables.businessUnitId, '1101-%')>
			<cfset variables.PmDepositCodeId = daoObj.getAccountCode(variables.businessUnitId, '1101-%')>
			<cfset variables.couponCreditCodeId = daoObj.getAccountCode(variables.businessUnitId, '4100-%')>	
			<cfset variables.promosCreditCodeId = daoObj.getAccountCode(variables.businessUnitId, '4100-%')>
			<cfset variables.couponDebitCodeId = daoObj.getAccountCode(variables.businessUnitId, '4110-%')>	
			<cfset variables.promosDebitCodeId = daoObj.getAccountCode(variables.businessUnitId, '4110-%')>
			
			<cfloop index="day" from="0" to="6">
				
				<cftry>
				
					<cftransaction>
									
						<!---Entry Date--->
						<cfset variables.entryDate = DateFormat(DateAdd("d", variables.beginDate, day), variables.appDateFormat)>
							
							<cfif DateDiff("d", Dateformat(variables.entryDate, "mm/dd/yyyy"), DateFormat(Now(),"mm/dd/yyyy")) GT 0>	
								<!---Net Sales $--->						
								<cfset variables.netSalesDollar = ListToArray(parsedData[7], ' ', false, true)[netSalesPos]>
								<cfset variables.netSalesDollar = LSParseNumber(variables.netSalesDollar)>
								
								<!---Sales Tax $--->						
								<cfset variables.salesTaxDollar = ListToArray(parsedData[9], ' ', false, true)[salesTaxPos]>
								<cfset variables.salesTaxDollar = LSParseNumber(variables.salesTaxDollar)>
								
								<!---Paid outs $--->						
								<cfset variables.paidOutsDollar = ListToArray(parsedData[13], ' ', false, true)[paidOutsPos]>
								<cfset variables.paidOutsDollar = LSParseNumber(variables.paidOutsDollar)>
							
								<!---GC Redeemed $--->						
								<cfset variables.gcRedeemedDollar = ListToArray(parsedData[14], ' ', false, true)[gcRedeemedPos]>
								<cfset variables.gcRedeemedDollar = LSParseNumber(variables.gcRedeemedDollar)>
								
								<!---A/R $--->						
								<cfset variables.ArDollar = ListToArray(parsedData[15], ' ', false, true)[ArPos]>
								<cfset variables.ArDollar = LSParseNumber(variables.ArDollar)>
								
								<!---Visa/MC $--->						
								<cfset variables.VisaMcDollar = ListToArray(parsedData[18], ' ', false, true)[VisaMcPos]>
								<cfset variables.VisaMcDollar = LSParseNumber(variables.VisaMcDollar)>
								
								<!---Amex $--->						
								<cfset variables.AmexDollar = ListToArray(parsedData[19], ' ', false, true)[AmexPos]>
								<cfset variables.AmexDollar = LSParseNumber(variables.AmexDollar)>
								
								<!---AM Deposit $--->						
								<cfset variables.AMdepositsDollar = ListToArray(parsedData[20], ' ', false, true)[AmDepPos]>
								<cfset variables.AMdepositsDollar = LSParseNumber(variables.AMdepositsDollar)>
								
								<!---PM Deposit $--->						
								<cfset variables.PMdepositsDollar = ListToArray(parsedData[21], ' ', false, true)[PmDepPos]>
								<cfset variables.PMdepositsDollar = LSParseNumber(variables.PMdepositsDollar)>
									
								<!---Coupon $--->						
								<cfset variables.couponDollar = ListToArray(parsedData[23], ' ', false, true)[couponPos]>
								<cfset variables.couponDollar = LSParseNumber(variables.couponDollar)>
								<cfset variables.couponDollar = Abs(variables.couponDollar)>															
								
								<!---Promos $--->						
								<cfset variables.promosDollar = ListToArray(parsedData[25], ' ', false, true)[promosPos]>
								<cfset variables.promosDollar = LSParseNumber(variables.promosDollar)>
								<cfset variables.promosDollar = Abs(variables.promosDollar)>
								
								<!---Cash Over Short --->						
								<cfset variables.cashOverShortDollar = ListToArray(parsedData[22], ' ', false, true)[cashOverPos]>								
								<cfset variables.cashOverShortDollar = LSParseNumber(variables.cashOverShortDollar)>								

								<!---Gift Certs Sold--->						
								<cfset variables.GcSold = ListToArray(parsedData[10], ' ', false, true)[giftCertPos]>
								<cfset variables.GcSold = LSParseNumber(variables.GcSold)>

								<!---Gift Certs Paid out--->						
								<cfset variables.GcPaidOut = ListToArray(parsedData[12], ' ', false, true)[giftCertPaidOutPos]>
								<cfset variables.GcPaidOut = LSParseNumber(variables.GcPaidOut)>
								
								<cfset Calc1 = Evaluate(variables.AmexDollar + variables.VisaMcDollar + variables.AMdepositsDollar + variables.PMdepositsDollar + variables.gcRedeemedDollar + variables.ArDollar + variables.paidOutsDollar + variables.GcPaidOut)>
								<cfset Calc2 = Evaluate(variables.netSalesDollar + variables.salesTaxDollar + variables.GcSold)>
								<cfset Calc3 = Evaluate(Calc1 - Calc2)>
								<cfset Calc3 =  DecimalFormat(Calc3)>
								<cfset Calc3 = lsParseNumber("#Calc3#")>

							
								<cfif CompareNoCase("#variables.cashOverShortDollar#", "#Calc3#") EQ 0>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.netSalesCodeId, '0', variables.netSalesDollar, 'Net Food Sales')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.salesTaxCodeId, '0', variables.salesTaxDollar, 'Sales Tax Payable')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.gcRedeemedCodeId, variables.gcRedeemedDollar, '0', 'G/C Redeemed')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.ArCodeId, variables.ArDollar, '0', 'A/R')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.VisaCodeId, variables.VisaMcDollar, '0', 'Visa/MC/Discover')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.AmexCodeId, variables.AmexDollar, '0', 'AMEX')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.AmDepositCodeId, variables.AMdepositsDollar, '0', 'AM Deposit')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.PmDepositCodeId, variables.PMdepositsDollar, '0', 'PM Deposit')>								
										<!---Insert Data--->
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.couponDebitCodeId, variables.couponDollar, '0', 'Coupon $')>
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.couponCreditCodeId, '0', variables.couponDollar, 'Coupon $')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.paidOutsCodeId, variables.paidOutsDollar, '0', 'Paid outs')>
										<!---Insert Data--->
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.promosDebitCodeId, variables.promosDollar, '0', 'Promos $')>
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.promosCreditCodeId, '0', variables.promosDollar, 'Promos $')>	
											
										
										<!---General Data START--->
										
										<!---Coupons Num--->						
										<cfset variables.CouponsNum = ListToArray(parsedData[24], ' ', false, true)[couponNumPos]>
										<cfset variables.CouponsNum = LSParseNumber(variables.CouponsNum)>
										
										<!---Promos Num--->						
										<cfset variables.PromosNum = ListToArray(parsedData[26], ' ', false, true)[promoNumPos]>
										<cfset variables.PromosNum = LSParseNumber(variables.PromosNum)>
										
										<!---Deletes Num--->						
										<cfset variables.DeletesNum = ListToArray(parsedData[27], ' ', false, true)[deletesNumPos]>
										<cfset variables.DeletesNum = ReplaceNoCase(variables.DeletesNum, '(', '')>
										<cfset variables.DeletesNum = ReplaceNoCase(variables.DeletesNum, ')', '')>
										<cfset variables.DeletesNum = LSParseNumber(variables.DeletesNum)>
										
										<!---Customer Count --->						
										<cfset variables.CustomerCount = ListToArray(parsedData[29], ' ', false, true)[custCntPos]>
										<cfset variables.CustomerCount = LSParseNumber(variables.CustomerCount)>
		
										<!--- Labor --->						
										<cfset variables.laborNum = ListToArray(parsedData[30], ' ', false, true)[laborPos]>
										<cfset variables.laborNum = LSParseNumber(variables.laborNum)>
										
										<!---Insert General Data--->
										<cfset daoObj.InsertDailyGeneralData(variables.businessUnitId, variables.entryDate, variables.CouponsNum, variables.PromosNum, 
										variables.DeletesNum, variables.CustomerCount, variables.GcSold, variables.laborNum, variables.GcPaidOut)>
								<cfelse>
									<cfset failMessage = 'Validation Failed - ' & 'Date: ' & variables.entryDate & ' Store: ' & storeId & ' Calc: ' & Calc3 & ' DSR: ' & variables.cashOverShortDollar>
									<cfmodule template="errorReport.cfm" report="#failMessage#" parsedData="#parsedData#">
									
										<!---Insert Zero Dollar Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.netSalesCodeId, '0', '0', 'Net Food Sales')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.salesTaxCodeId, '0', '0', 'Sales Tax Payable')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.gcRedeemedCodeId, '0', '0', 'G/C Redeemed')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.ArCodeId, '0', '0', 'A/R')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.VisaCodeId, '0', '0', 'Visa/MC/Discover')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.AmexCodeId, '0', '0', 'AMEX')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.AmDepositCodeId, '0', '0', 'AM Deposit')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.PmDepositCodeId, '0', '0', 'PM Deposit')>								
										<!---Insert Data--->
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.couponDebitCodeId, '0', '0', 'Coupon $')>
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.couponCreditCodeId, '0', '0', 'Coupon $')>
										<!---Insert Data--->								
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.paidOutsCodeId, '0', '0', 'Paid outs')>
										<!---Insert Data--->
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.promosDebitCodeId, '0', '0', 'Promos $')>
										<cfset daoObj.InsertSalesData(variables.businessUnitId, variables.entryDate, variables.promosCreditCodeId, '0', '0', 'Promos $')>	
										
										<!---Insert General Data--->
										<cfset daoObj.InsertDailyGeneralData(variables.businessUnitId, variables.entryDate, 0, 0, 0, 0, 0, 0, 0)>
									
									
								</cfif>							
									<!---Increment Data Cursors --->
									<cfset netSalesPos++>							
									<cfset salesTaxPos++>
									<cfset paidOutsPos++>
									<cfset gcRedeemedPos++>
									<cfset ArPos++>
									<cfset VisaMcPos++>
									<cfset AmexPos++>							
									<cfset AmDepPos++>
									<cfset PmDepPos++>
									<cfset couponPos++>
									<cfset promosPos++>
									<cfset cashOverPos++>
									<cfset giftCertPos++>
									<cfset giftCertPaidOutPos++>
									
									<cfset couponNumPos++>
									<cfset promoNumPos++>
									<cfset deletesNumPos++>
									<cfset custCntPos++>
									<cfset laborPos++>	

																
						</cfif>								
								

					</cftransaction>
							<cfcatch type="any">
								<cfif NOT debugMode>
									<cfmodule template="errorReport.cfm" report="#cfcatch#" parsedData="#parsedData#">
									<cfset application.dsrNoError = 0>
								</cfif>
							</cfcatch>	
						</cftry>
			</cfloop>
			
			<cfset variables.businessUnitId = "">
		</cfif>
			<!--- Add to list of stores done --->
			<cfset arrayAppend(storesImported, storeId)>
	</cfloop>	

</cfif>

<cfif arraylen(storesImported) gt 0>
	<cfset ArraySort(storesImported, "numeric", "asc")> 
</cfif>

<cfif fileIdx EQ 11 AND application.dsrNoError EQ 1>
	<cfmodule template="errorReport.cfm" report="#storesImported#" ErrorSubject="Success - DSR Import" parsedData="#application#">
	<cfmodule template="CondensedDSR.cfm">
	<cfmodule template="getDailyMenuMix.cfm" mmixFileList="#mmixTextFiles#">
<cfelse>
	<cfmodule template="errorReport.cfm" report="#storesImported#" ErrorSubject="FAILED - DSR Import" parsedData="#application#" priority="urgent">
</cfif>
