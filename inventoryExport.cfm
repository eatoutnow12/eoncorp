﻿<cfinclude template="header.cfm">
<cfmodule template="checkaccess.cfm" reportId="11">
		<script type="text/javascript">
			function ValidateSubmit()
			{
				document.forms[0].submit();
			}	
		</script>
<form action="inventoryExport.cfm" method="post">
<div style="margin:10px"><h3>Inventory Data Viewer</h3><br>

	<label for="bUnit">Select Store:</label>
	<select id="bUnit" name="bUnit">
		<cfloop query="application.getAllUnits">
			<cfoutput>
            	<option id="#Trim(unitName)#" value="#Trim(id)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ id>selected</cfif>>
				#Trim(unitName)#
				</option>
            </cfoutput>
		</cfloop>
	</select>&nbsp;&nbsp;&nbsp;&nbsp;
	
		<label for="fcYear">Year:</label>
		<select id="fcYear" name="fcYear">
			<cfloop index="i" from="#year(now())#" to="2011" step="-1">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcYear") AND form.fcYear EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>
		</select>	
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="fcPeriod">Period:</label>
		<select id="fcPeriod" name="fcPeriod">
			<cfloop index="i" from="1" to="13">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcPeriod") AND form.fcPeriod EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>
		</select>
		
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="fcWeek">Week:</label>
		<select id="fcWeek" name="fcWeek">					
			<cfloop index="i" from="1" to="4">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcWeek") AND form.fcWeek EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>			
		</select>

		<input type="button" onclick="ValidateSubmit()" value="Export Inventory Data">

</form>

	<cfif IsDefined("form.bUnit")>
		
		<cfquery name="getUnitName" dbtype="query">
			SELECT unitName FROM application.getAllUnits
			WHERE id = '#form.bUnit#'
		</cfquery>
		
		<cfquery name="getCal" datasource="#application.eonDsn#">
			SELECT startDate 
			FROM businessCalendar
			WHERE YEAR = #form.fcYear#
		</cfquery>				
		
		<cfif form.fcWeek EQ 0>
			<cfset currentDateEnd = application.utilObj.getDateByPeriodWeekDay(getCal.startDate,form.fcPeriod,4,7)>					
		<cfelse>
			<cfset currentDateEnd = application.utilObj.getDateByPeriodWeekDay(getCal.startDate,form.fcPeriod,form.fcWeek,7)>
		</cfif>
		
		<cfset currentDateEnd = dateformat(currentDateEnd, application.eonDateFormat)>
		
		<cfquery name="getInventory" datasource="#application.eonDsn#">
			SELECT storeInventory.itemNumber, purchaseItems.itemDescription, quantity, price, dbo.purchaseCategories.categoryDescr, itemCategoryId
			FROM dbo.storeInventory
			INNER JOIN dbo.purchaseItems
			ON dbo.storeInventory.itemNumber = dbo.purchaseItems.itemNumber
			INNER JOIN dbo.purchaseCategories
			ON dbo.purchaseItems.itemCategoryId = dbo.purchaseCategories.categoryId
			WHERE 
			businessunitId = '#form.bUnit#'
			AND isLatest = 1
			AND dateweekEnding = '#currentDateEnd#'
			AND inProcess = 0
UNION  		
			SELECT storeInventory.itemNumber
			,itemDescription= (SELECT itemDescription FROM dbo.storeInProcessItems WHERE itemNumber=storeInventory.itemNumber AND storeInProcessItems.id = storeInventory.inProcess), quantity, price
			,categoryDescr= (SELECT categoryDescr FROM dbo.purchaseCategories WHERE categoryId = (SELECT itemCategoryId FROM purchaseItems WHERE itemNumber = storeInventory.itemNumber))
			,itemCategoryId= (SELECT itemCategoryId FROM purchaseItems WHERE itemNumber = storeInventory.itemNumber)

			FROM dbo.storeInventory
			WHERE 
			storeInventory.businessunitId = '#form.bUnit#'
			AND isLatest = 1
			AND dateweekEnding = '#currentDateEnd#'
			AND inProcess > 0
			ORDER BY itemCategoryId, storeInventory.itemNumber	
		</cfquery>
	
		<cfsavecontent variable="session.invExport">
	
		<table>
			<tr>
				<td>
				<b>Item Number</b>
				</td>
				<td nowrap="nowrap">
				<b>Item Description</b>
				</td>
				<td>
				<b>Quantity</b>
				</td>
				<td>
				<b>Unit Price</b>
				</td>
				<td nowrap="nowrap">
				<b>Item Category</b>
				</td>
			</tr>
		
		<cfloop query="getInventory">
			<cfoutput>
            	<tr>
					<td>
					#ItemNumber#
					</td>
					<td nowrap="nowrap">
					#ItemDescription#
					</td>
					<td>
					#Quantity#
					</td>
					<td>
					#Price#
					</td>
					<td nowrap="nowrap">
					#categoryDescr#
					</td>
				</tr>
            </cfoutput>
		</cfloop>
		</table>
		</cfsavecontent>
		
		
		<cfheader
			name="Content-Disposition"
			value="attachment; filename=invExport-Store#getUnitName.unitName#-#form.fcYear#-P#form.fcPeriod#-W#form.fcWeek#.xls"
			/>
			
			<!---
				We are going to stream the excel data to the browser
				using a file stream from the server. To do this, we
				will have to save a temp file to the server.
			--->
		 
			<!--- Get the temp file for streaming. --->
			<cfset strFilePath = GetTempFile(
				GetTempDirectory(),
				"excel_"
				) />
		 
			<!--- Write the excel data to the file. --->
			<cffile
				action="WRITE"
				file="#strFilePath#"
				output="#session.invExport.Trim()#"
				/>
		 
			<!---
				Stream the file to the browser. By doing this, the
				content buffer is automatically cleared and the file
				is streamed. We don't have to worry about anything
				after the file as no page content is taken into
				account any more.
		 
				Additionally, we are requesting that the file be
				deleted after it is done streaming (deletefile). Now,
				we don't have to worry about cluttering up the server.
			--->
			<cfcontent
				type="application/msexcel"
				file="#strFilePath#"
				deletefile="true"
				/>	
	</cfif>
<cfinclude template="footer.cfm">