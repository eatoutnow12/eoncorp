﻿<cfset specialYears = "2007, 2012, 2017, 2022, 2027">
<cfset specialPeriodDays = 35>
<cfset regularPeriodDays = 28>

<cfset inDate = "4/19/2010">
<cfset P1Start2010 = "12/28/2009">

<cfset dayofYear = DayOfYear(inDate)>
<cfset currentYear = Year(inDate)>

<cfset Period = -1>
<cfset Week = -1>

<cfloop index="i" from="1" to="13">
	<cfset addDays = (regularPeriodDays*i)>
	<cfset temp = DateAdd("d", P1Start2010, addDays)>
	<cfset diff = DateDiff("d", temp, inDate)>
	
	<cfif diff lt 0>
		<cfset Period = i>
		<cfbreak>
	</cfif>
</cfloop>

<cfloop index="j" from="1" to="52">
	<cfset addDays = (7*j)>
	<cfset temp = DateAdd("d", P1Start2010, addDays)>
	<cfset diff = DateDiff("d", temp, inDate)>

	<cfif diff lt 0>
		<cfset Week = j%4>
		<cfif Week eq 0>
			<cfset Week = 4>
		</cfif>
		<cfbreak>
	</cfif>	
</cfloop>

<cfdump var = "#Period#">
<br>
<cfdump var="#Week#">