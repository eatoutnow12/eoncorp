<cfinclude template="header.cfm">
<style type="text/css">
	tr.lightrow td {
	background-color: #FFFFFF;
	}
	tr.darkrow td {
	background-color: #EFEFEF;
	}
</style>

		<script type="text/javascript">
			function ValidateSubmit()
			{
				document.forms[0].submit();
			}	
		</script>
<form action="<cfoutput>#getFileFromPath( cgi.script_name )#</cfoutput>" method="post">
<div style="margin:10px"><h3>Inventory Transfers Viewer</h3><br>

	<cfif NOT IsDefined("cookie.storeId")>
				<label for="bUnit">Select Store:</label>	
				<select id="bUnit" name="bUnit">
					<cfloop query="application.getAllUnits">
						<cfoutput>
			            	<option id="#Trim(unitName)#" value="#Trim(id)#" <cfif IsDefined("form.bUnit") AND form.bUnit EQ id>selected</cfif>>
							#Trim(unitName)#
							</option>
			            </cfoutput>
					</cfloop>
				</select>&nbsp;&nbsp;&nbsp;&nbsp;
			<cfelse>
				<cfoutput><input type="hidden" name="bUnit" value="#cookie.storeId#"></cfoutput>
			</cfif>
	
		<label for="fcYear">Year:</label>
		<select id="fcYear" name="fcYear">
			<cfloop index="i" from="#year(now())#" to="2011" step="-1">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcYear") AND form.fcYear EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>
		</select>	
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="fcPeriod">Period:</label>
		<select id="fcPeriod" name="fcPeriod">
			<cfloop index="i" from="1" to="13">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcPeriod") AND form.fcPeriod EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>
		</select>
		
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="fcWeek">Week:</label>
		<select id="fcWeek" name="fcWeek">					
			<cfloop index="i" from="1" to="4">
				<cfoutput><option id="#i#" value="#i#" <cfif IsDefined("form.fcWeek") AND form.fcWeek EQ '#i#'>selected</cfif>>#i#</option></cfoutput>
			</cfloop>			
		</select>

		<input type="button" onclick="ValidateSubmit()" value="View Transfers">

</form>

	<cfif IsDefined("form.bUnit")>
		
		<cfquery name="getCal" datasource="#application.eonDsn#">
			SELECT startDate 
			FROM businessCalendar
			WHERE YEAR = #form.fcYear#
		</cfquery>		

		<cfquery name="getUnitName" dbtype="query">
			SELECT unitName FROM application.getAllUnits
			WHERE id = '#form.bUnit#'
		</cfquery>			
		

		<cfset currentDateEnd = application.utilObj.getDateByPeriodWeekDay(getCal.startDate,form.fcPeriod,form.fcWeek,7)>

		<cfset currentDateStart = dateformat(dateadd("d",-6,currentDateEnd), application.eonDateFormat)>
		<cfset currentDateEnd = dateformat(currentDateEnd, application.eonDateFormat)>
		
		<cfquery name="Transfers" datasource="#application.eonDsn#">
			SELECT [dateOfTransfer]
					,[fromStoreId]
					,fromStoreName = (SELECT unitName FROM dbo.businessUnit WHERE id = storeInventoryTransfers.fromStoreId)
					,[toStoreId]
					,toStoreName = (SELECT unitName FROM dbo.businessUnit WHERE id = storeInventoryTransfers.toStoreId)
					,[itemNumber]
					,itemName = (SELECT itemDescription FROM purchaseItems WHERE itemNumber = storeInventoryTransfers.itemNumber)
					,[unitCost]
					,[quantity]
					,(unitCost*quantity) AS total
			  FROM [storeInventoryTransfers]
			  WHERE (fromStoreId = '#form.bUnit#' OR toStoreId = '#form.bUnit#')
			  AND dateOfTransfer BETWEEN '#currentDateStart#' AND '#currentDateEnd#'
			  AND dateDeleted IS NULL
		</cfquery>
		
		<cfquery name="getTransfersFrom" dbtype="query">
			SELECT * FROM Transfers
			WHERE fromStoreId = '#form.bUnit#'
		</cfquery>
		
		<cfquery name="getTransfersTo" dbtype="query">
			SELECT * FROM Transfers
			WHERE toStoreId = '#form.bUnit#'
		</cfquery>

	<h4>Transfers From Store <cfoutput>#getUnitName.unitName#</cfoutput>:</h4>
	<table width="600" style="border:green double 2px;margin:5px">
		<tr>
			<td><b>To Store</b></td>
			<td><b>Item #</b></td>
			<td><b>Item Name</b></td>
			<td><b>Quantity</b></td>
			<td><b>Cost</b></td>
			<td><b>Amount</b></td>
		</tr>

		<cfset idx = 1>
		<cfloop query="getTransfersFrom">	
			<cfset idx++>
			<cfset rowclass = "lightrow"> 
			<cfif idx mod 2>
				<cfset rowclass = "darkrow">
			</cfif>

				<cfoutput>
		        	<tr class="#rowclass#">
						<td>
							#toStoreName#
						</td>
						
						<td>
							#itemNumber#
						</td>
						
						<td>
							#itemName#
						</td>
						
						<td>
							#Quantity#
						</td>
						<td>
							#dollarFormat(unitCost)#
						</td>
						<td>
							#dollarFormat(total)#
						</td>
					</tr>
		        </cfoutput>

		</cfloop>
		
	</table>


	<br>
	<hr noshade="noshade" style="width:620px;margin:0">	

	
	<h4>Transfers To Store <cfoutput>#getUnitName.unitName#</cfoutput>:</h4>
	<table width="600" style="border:green double 2px;margin:5px">
		<tr>
			<td><b>From Store</b></td>
			<td><b>Item #</b></td>
			<td><b>Item Name</b></td>
			<td><b>Quantity</b></td>
			<td><b>Cost</b></td>
			<td><b>Amount</b></td>
		</tr>

		<cfset idx = 1>
		<cfloop query="getTransfersTo">	
			<cfset idx++>
			<cfset rowclass = "lightrow"> 
			<cfif idx mod 2>
				<cfset rowclass = "darkrow">
			</cfif>

				<cfoutput>
		        	<tr class="#rowclass#">
						<td>
							#fromStoreName#
						</td>
						
						<td>
							#itemNumber#
						</td>
						
						<td>
							#itemName#
						</td>
						
						<td>
							#Quantity#
						</td>
						<td>
							#dollarFormat(unitCost)#
						</td>
						<td>
							#dollarFormat(total)#
						</td>
					</tr>
		        </cfoutput>

		</cfloop>
		
	</table>
	
	</cfif>

<cfinclude template="footer.cfm">