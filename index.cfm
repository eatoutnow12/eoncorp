﻿<cfif isdefined("session.userId")>
	<cfquery name="getReports" datasource="#application.eonDsn#">
		SELECT corpReports.reportId, reportTemplate, reportTitle FROM corpReports
		INNER JOIN dbo.userPermissions
		ON corpReports.reportId = userPermissions.reportId
		WHERE dbo.userPermissions.userId = '#session.userId#'
		UNION 
		SELECT corpReports.reportId, reportTemplate, reportTitle FROM corpReports
		WHERE corpReports.reportForAll <> 0
		ORDER BY reportTitle
	</cfquery>
</cfif>
<cfinclude template="header.cfm">
<!---	<cfif Len(variables.error)>
			<div style="width:100%;text-align:center;">
	 		<div style="color:#DD4B39;font-weight:bold;display:inline-block;">
				<cfoutput>#variables.error#</cfoutput>
			</div>
			</div>
	</cfif>--->
	
	<div>
		
	<div>
		<h3 style="text-decoration:underline;">EON Corp Portal</h3>
			Please choose an app below:
		</div>		
		<ul>
			<cfoutput>
				<cfloop query="getReports">			
					<li>
						<a href="#reportTemplate#.cfm">#reportTitle#</a>
					</li>
				</cfloop>
			</cfoutput>		
		</ul>
	</div>
<cfinclude template="footer.cfm" />